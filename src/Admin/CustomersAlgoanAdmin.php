<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class CustomersAlgoanAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('customerId')
            ->add('algoanExtnalId')
            ->add('name')
            ->add('surname')
            ->add('civility')
            ->add('postalCode')
            ->add('phone')
            ->add('email')
            ->add('dateBankaccountOpened')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('customerId')
            ->add('algoanExtnalId')
            ->add('name')
            ->add('surname')
            ->add('analysis',null, array(
                'template' => 'Admin/CustomersAlgoan/analysis_list_field.html.twig'
            ))
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => []
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('customerId')
            ->add('algoanExtnalId')
            ->add('name')
            ->add('surname')
            ->add('civility')
            ->add('postalCode')
            ->add('phone')
            ->add('email')
            ->add('analysis',null, array(
                'template' => 'Admin/CustomersAlgoan/analysis_show_field.html.twig'
            ))
            ->add('dateBankaccountOpened')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
}
