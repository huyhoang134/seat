<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class DistributeursAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        'page' => 1,
        'isReservationAvailable' => ['type' => 1, 'value' => 1],
    ];

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('kvps')
            ->add('kvpsDe')
            ->add('codeCe')
            ->add('district')
            ->add('occasion')
            ->add('distribType')
            ->add('serviceCenter')
            ->add('raisonSociale')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telephone')
            ->add('fax')
            ->add('telephoneAbbr')
            ->add('faxAbbr')
            ->add('latitude')
            ->add('longitude')
            ->add('email')
            ->add('website')
            ->add('websiteSeat')
            ->add('seatAccess')
            ->add('cupraAccess')
            ->add('created')
            ->add('modified')
            ->add('escooter')
            ->add('isReservationAvailable')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id')
            ->add('kvps')
            ->add('kvpsDe')
            ->add('codeCe')
            ->add('district')
            ->add('occasion')
            ->add('distribType')
            ->add('serviceCenter')
            ->add('raisonSociale')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telephone')
            ->add('email')
            ->add('escooter', null, ['editable' => true,'header_style' => 'width: 50px'])
            ->add('isReservationAvailable', null, ['editable' => true,'header_style' => 'width: 50px'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('General', ['class' => 'col-md-6'])
            ->add('kvps')
            ->add('kvpsDe')
            ->add('codeCe')
            ->add('district')
            ->add('occasion')
            ->add('distribType')
            ->add('serviceCenter')
            ->add('raisonSociale')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telephone')
            ->add('fax')
            ->add('telephoneAbbr')
            ->add('faxAbbr')
            ->add('latitude')
            ->add('longitude')
            ->add('email')
            ->add('website')
            ->add('websiteSeat')
            ->add('seatAccess')
            ->add('cupraAccess')
            ->end()
            ->with('Dealer Info', ['class' => 'col-md-6'])
            ->add('dealerName')
            ->add('dealerCode')
            ->add('dealerEmail')
            ->add('dealerPhone')
            ->add('dealerResp')
            ->end()
            ->with('Options', ['class' => 'col-md-6'])
            ->add('created')
            ->add('modified')
            ->add('escooter')
            ->add('isReservationAvailable')
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('kvps')
            ->add('kvpsDe')
            ->add('codeCe')
            ->add('district')
            ->add('occasion')
            ->add('distribType')
            ->add('serviceCenter')
            ->add('raisonSociale')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telephone')
            ->add('fax')
            ->add('telephoneAbbr')
            ->add('faxAbbr')
            ->add('latitude')
            ->add('longitude')
            ->add('email')
            ->add('website')
            ->add('websiteSeat')
            ->add('seatAccess')
            ->add('cupraAccess')
            ->add('dealerName')
            ->add('dealerCode')
            ->add('dealerEmail')
            ->add('dealerPhone')
            ->add('dealerResp')
            ->add('created')
            ->add('modified')
            ->add('escooter')
            ->add('isReservationAvailable')
            ;
    }
}
