<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\WebhookLogs;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class WebHookLogsAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('eventType')
            ->add('customerApplication')
            ->add('timeStamp')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('eventType')
            ->add('customerApplication')
            ->add('eventData')
            ->add('caseId')
            ->add('timeStamp')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('id', null, [
                'attr' => [
                    'readonly' => true,
                ]
            ])
            ->add('eventType')
            ->add('customerApplication')
            ->add('eventData', TextareaType::class, [
                'required' => false,
                'attr'     => ['rows' => 5],
            ])
            ->add('timeStamp')
            ->add('createdAt')
            ->add('updatedAt');
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('eventType')
            ->add('customerApplication')
            ->add('eventData', TextType::class)
            ->add('caseId')
            ->add('timeStamp')
            ->add('createdAt')
            ->add('updatedAt');
    }

    /*public function prePersist($webHookLogs)
    {
        if ($webHookLogs instanceof WebhookLogs) {
            $jsonArray = json_decode($webHookLogs->getEventData(), true);

            $webHookLogs->setEventData($jsonArray);
        }
    }*/
}
