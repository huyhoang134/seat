<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

final class ImportLogsAdmin extends AbstractAdmin
{

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'id',
        '_sort_order' => 'DESC',
        '_per_page' => '256'
    );
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('type')
            ->add('autheur')
            ->add('date')
            ->add('fichier')
            //->add('logs')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('type')
            ->add('autheur')
            ->add('date')
            ->add('fichier')
            //->add('logs')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    //'edit' => [],
                    //'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            //->add('id')
            ->add('type')
            ->add('autheur')
            ->add('date')
            ->add('fichier')
            ->add('logs')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('type')
            ->add('autheur')
            ->add('date')
            ->add('fichier')
            ->add('logs', null, array(
                'template' => 'Admin/form/logs.html.twig'
            ))
        
                ;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
}
