<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

final class ImportAdmin extends AbstractAdmin
{

   protected $baseRouteName = 'import'; 
   protected $baseRoutePattern = 'import';
 
   
   protected function configureRoutes(RouteCollection $collection)
   {
       $collection->remove('create');
   }
}
