<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class CustomersApplicationAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('idApplication')
            ->add('civility')
            ->add('name')
            ->add('surname')
            ->add('maidenName')
            ->add('dateOfBirth')
            ->add('cityBirth')
            ->add('zipcodeBirth')
            ->add('adress')
            ->add('adress2')
            ->add('city')
            ->add('zipcode')
            ->add('nationality')
            ->add('phone')
            ->add('email')
            ->add('mainResidence')
            ->add('familySitutation')
            ->add('childs')
            ->add('occupation')
            ->add('hiringDate')
            ->add('contractType')
            ->add('licenceOptin')
            ->add('commercialOptin')
            ->add('applicationState')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('idApplication')
            ->add('civility')
            ->add('name')
            ->add('surname')
            ->add('maidenName')
            ->add('dateOfBirth')
            ->add('cityBirth')
            ->add('zipcodeBirth')
            ->add('adress')
            ->add('adress2')
            ->add('city')
            ->add('zipcode')
            ->add('nationality')
            ->add('phone')
            ->add('email')
            ->add('mainResidence')
            ->add('familySitutation')
            ->add('childs')
            ->add('occupation')
            ->add('hiringDate')
            ->add('contractType')
            ->add('licenceOptin')
            ->add('commercialOptin')
            ->add('applicationState')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('idApplication')
            ->add('civility')
            ->add('name')
            ->add('surname')
            ->add('maidenName')
            ->add('dateOfBirth')
            ->add('cityBirth')
            ->add('zipcodeBirth')
            ->add('adress')
            ->add('adress2')
            ->add('city')
            ->add('zipcode')
            ->add('nationality')
            ->add('phone')
            ->add('email')
            ->add('mainResidence')
            ->add('familySitutation')
            ->add('childs')
            ->add('occupation')
            ->add('hiringDate')
            ->add('contractType')
            ->add('licenceOptin')
            ->add('commercialOptin')
            ->add('applicationState')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('idApplication')
            ->add('civility')
            ->add('name')
            ->add('surname')
            ->add('maidenName')
            ->add('dateOfBirth')
            ->add('cityBirth')
            ->add('zipcodeBirth')
            ->add('adress')
            ->add('adress2')
            ->add('city')
            ->add('zipcode')
            ->add('nationality')
            ->add('phone')
            ->add('email')
            ->add('mainResidence')
            ->add('familySitutation')
            ->add('childs')
            ->add('occupation')
            ->add('hiringDate')
            ->add('contractType')
            ->add('licenceOptin')
            ->add('commercialOptin')
            ->add('applicationState')
            ;
    }
}
