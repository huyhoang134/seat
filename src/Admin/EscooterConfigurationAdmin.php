<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class EscooterConfigurationAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('vehicleBrand')
            ->add('vehicleEngine')
            ->add('vehicleModel')
            ->add('vehicleKm')
            ->add('vehicleKmAnnual')
            ->add('vehicleKmTotal')
            ->add('vehicleKmPrice')
            ->add('vehicleOptions')
            ->add('vehicleOptionsPrice')
            ->add('optinInsuranceGcm')
            ->add('optinInsuranceVdr')
            ->add('loaPrice')
            ->add('price')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('vehicleBrand')
            ->add('vehicleEngine')
            ->add('vehicleModel')
            ->add('vehicleKm')
            ->add('vehicleKmAnnual')
            ->add('vehicleKmTotal')
            ->add('vehicleKmPrice')
            ->add('vehicleOptions')
            ->add('vehicleOptionsPrice')
            ->add('optinInsuranceGcm')
            ->add('optinInsuranceVdr')
            ->add('loaPrice')
            ->add('price')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('id')
            ->add('vehicleBrand')
            ->add('vehicleEngine')
            ->add('vehicleModel')
            ->add('vehicleKm')
            ->add('vehicleKmAnnual')
            ->add('vehicleKmTotal')
            ->add('vehicleKmPrice')
            ->add('vehicleOptions')
            ->add('vehicleOptionsPrice')
            ->add('optinInsuranceGcm')
            ->add('optinInsuranceVdr')
            ->add('loaPrice')
            ->add('price')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('vehicleBrand')
            ->add('vehicleEngine')
            ->add('vehicleModel')
            ->add('vehicleKm')
            ->add('vehicleKmAnnual')
            ->add('vehicleKmTotal')
            ->add('vehicleKmPrice')
            ->add('vehicleOptions')
            ->add('vehicleOptionsPrice')
            ->add('optinInsuranceGcm')
            ->add('optinInsuranceVdr')
            ->add('loaPrice')
            ->add('price')
            ;
    }
}
