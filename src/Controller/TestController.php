<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\BankPushService;



class TestController extends AbstractController
{
    private $bankPushService;

    private $parameterBag;

    public function __construct(BankPushService $bankPushService, ParameterBagInterface $parameterBag)
    {
        $this->bankPushService = $bankPushService;
        $this->parameterBag    = $parameterBag;
    }


    /**
     * @Route("/test/kyc", name="kyc_step1")
     */
    public function kycStep1()
    {

        /*
         * Create request to get the access token
         *
         * */

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('quicksign-url') . '/applicant/api/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Pour ignorer la vérification SSL
        curl_setopt($ch, CURLOPT_VERBOSE, true); // Pour afficher des informations de débogage
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'grant_type' => 'client_credentials',
            'client_id' => $this->parameterBag->get('quicksign-client-id'),
            'client_secret' => $this->parameterBag->get('quicksign-client-secret'),
        ]));

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $ret = json_decode($response);
            $access_token=$ret->access_token;

        }

        /*
         * Create request to get iframe URL
         * we need to provide a default contract pdf.
         *
         * */

        $quick_sign_contract="quicksign/T&Cs.pdf";
        $url = $this->parameterBag->get('quicksign-url') . '/caseflow/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/cases/sync';


        $user_info=[
            'applicantClientId'=>'123456',//id of current user (maybe the userid in db ?)
            'telephone'=>'+33 645025544',
            'birthPlace'=>'Paris',
            'gender'=>'male',
            'honorificPrefix'=>'Mr',
            'birthDate'=>'1975-12-17',
            'givenName'=>'Mr',
            'givenName'=>'Thomas',
            'familyName'=>'Bourdin',
            'streetAddress'=>'27 rue mirabeau',
            'addressRegion'=>'Ile de france',
            'postalCode'=>'95870',
            'addressCountry'=>'FRA',
            'email'=>'tbourdin@partitech.com',
            'nationality'=>'FRA',
        ];

        $context = '{
  "workflow": "volkswagen-fs-fr-nebula",
  "caseClientId": "'.time().'",
  
  "applicants": [
    {
      "type": "applicant",
      "applicantClientId": "'.$user_info['applicantClientId'].'",
      "telephone": "'.$user_info['telephone'].'",
      "birthPlace": {
        "name": "'.$user_info['birthPlace'].'"
      },
       "gender": "'.$user_info['gender'].'",
      "honorificPrefix": "'.$user_info['honorificPrefix'].'",
      "birthDate": "'.$user_info['birthDate'].'",
      "givenName": "'.$user_info['givenName'].'",
      "familyName": "'.$user_info['familyName'].'",
        "address": {
        "streetAddress": "'.$user_info['streetAddress'].'",
        "addressCountry": {
          "identifier": "'.$user_info['addressCountry'].'"
        },
        "addressRegion": "'.$user_info['addressRegion'].'",
        "postalCode": "'.$user_info['postalCode'].'"
      },
      "email": "'.$user_info['email'].'",
      "nationality": {
        "identifier":"'.$user_info['nationality'].'"
      }
    }
  ]
}';

        //$context='{  }';
        $contract_path= realpath($quick_sign_contract);
        $pdfFile1 = new \CURLFile($contract_path, 'application/pdf', 'contrat.pdf');




        $ch = curl_init($url);


        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'context' => $context,
           /* 'contracts[0]' => $pdfFile1*/
        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);



        $response = curl_exec($ch);


        if (curl_errno($ch)) {
            echo 'Erreur cURL : ' . curl_error($ch);
        } else {
            dump($context);
            dd($response);
            echo 'Réponse : ' . $response;
            $ret=json_decode($response);

            $identification_id=$ret->caseId;
            $identification_url=$ret->reconnectionUrl;
            $identification_url_reconnect=$ret->smsReconnectionUrl;

        }

        curl_close($ch);

        return $this->render('test/kyc.html.twig', [
            'identification_id' => $identification_id,
            'identification_url' => $identification_url,
            'identification_url_reconnect' => $identification_url_reconnect,
            'quicksign_url' => $this->parameterBag->get('quicksign-url')
        ]);
    }

    /**
     * @Route("/test/algoan/", methods={"GET","POST"}, name="algoan_step1")
     */
    public function algoanStep1()
    {

        $this->algoan_api_url=$this->parameterBag->get('algoan-url');
        //https://api.algoan.com/v1/oauth/token
        $data=[
            'grant_type'=>'client_credentials',
            'client_id'=>'282ff2fbf539dcd0a4f486b8',
            'client_secret'=>'mgh7qKv0a42OHVXUGIgU8XqoiopEyYKo',

        ];


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_URL => $this->algoan_api_url.'/v1/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS=>http_build_query($data)
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);


        if ($err) {
            echo "cURL Error #:" . $err;
        } else {

            $ret = json_decode($response);

            return $this->render('test/algoan.html.twig', [
                'api_key' => "282ff2fbf539dcd0a4f486b8",
                'user_id' => 'random_user_id_for_test-321321321321321',

            ]);

        }
    }

    /**
     * @Route("/test/assets/{sessionid}", methods={"GET","POST"}, name="algoan_step1")
     */
    public function test_assets($sessionid): Response
    {
        $workspaceId = $this->bankPushService->getKycWorkspaceId($sessionid);
        $documentIds = $this->bankPushService->getKycDocumentId($sessionid, $workspaceId);
        $documentsFile = $this->bankPushService->getKycDocumentFile($documentIds);

        dd($documentsFile);


    }
}
?>
