<?php

namespace App\Controller;

use App\Entity\CustomersApplication;
use App\Entity\EscooterConfiguration;
use App\Entity\ZipcodeFr;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use App\Entity\Distributeurs;

/*
 * https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md
 * */

class ApiController extends AbstractController
{
    var $CURLOPT_TIMEOUT = 150000;

    private $parameterBag;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $entityManager)
    {
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
    }

    /**
     * @Get(
     *     path = "/api/get-distributors/{zipcode}",
     *     name = "api_get_distriburor_by_zipcode",
     *     requirements = {"id"="[a-z0-9]+"}
     * )
     *
     * @SWG\Get(
     *     summary="Get the closest distributor for a zipcode",
     *     operationId="api_get_distriburor_by_zipcode",
     *     tags={"Distributor"},
     *
     *      @SWG\Parameter(
     *          name="zipcode",
     *          in="path",
     *          type="string",
     *          description="zipcode"
     *      ),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Response ok",
     *          @SWG\Schema(
     *              type="object",
     *              example={
     *                  {
     *                      "name": "SEAT PARIS",
     *                      "label_info": "Essais disponible",
     *                      "address": "41 boulevard Gouvion Saint Cyr, 75017, PARIS",
     *                      "telephone": "0170601717",
     *                      "distrib_brand_name": "SEAT",
     *                      "distrib_brand_color": "#d96632",
     *                      "distance": "1.6km",
     *
     *                  }
     *              }
     *              )
     *     ),
     *
     *     @SWG\Response(
     *          response=401,
     *          description="Access Denied"
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Not Found"
     *     )
     *
     *     )
     */
    public function getDistributor(Request $request, $zipcode)
    {


        $ZipcodeFrRepository = $this->entityManager->getRepository(ZipcodeFr::class);
        $cp                  = $ZipcodeFrRepository->findOneByCodePostal($zipcode);
        if ( ! empty($cp)) {
            $DistributeursRepository = $this->entityManager->getRepository(Distributeurs::class);
            $distribs                = $DistributeursRepository->findDidtribByZipcodeFr($cp);


//            $serializer = $this->get('serializer');
//            $response   = new Response(
//                json_encode($distribs),
//                Response::HTTP_OK
//            );
//            $response->headers->set('Content-Type', 'application/json');

            $distributeursArray = [];
            foreach ($distribs as $item) {
                $distributeursArray[] = [
                    'lat'        => $item['lat'],
                    'lng'        => $item['lng'],
                    'kvps'       => $item['kvps'],
                    'name'       => $item['raison_sociale'],
                    'address'    => $item['address'],
                    'postalCode' => $item['code_postal'],
                    'ville'      => $item['ville'],
                ];
            }

            return $this->json([
                'success'  => true,
                'items'    => $distributeursArray,
                'template' => $this->render('default/parts/distributeur-choice.html.twig',[
                    'items' => $distributeursArray,
                ])
            ]);

        } else {
            return new Response(json_encode(['error' => 'Zipcode code not found.']), Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @Get(
     *     path = "/api/check-zipcode-eligible/{zipcode}",
     *     name = "api_check_zipcode_eligible",
     *     requirements = {"id"="[a-z0-9]+"}
     * )
     */
    public function getCheckZipcodeEligible(Request $request, $zipcode)
    {
        $zipcodeFrRepository = $this->entityManager->getRepository(ZipcodeFr::class);
        /** @var ZipcodeFr $cp */
        $cp = $zipcodeFrRepository->findOneByCodePostal($zipcode);

        if ( ! $cp) {
            return new Response(json_encode(['error' => 'Zipcode code not found.']), Response::HTTP_NOT_FOUND);
        }

        $codePostal = (int)$cp->getCodePostal();

        if ($codePostal >= 75000 && $codePostal <= 95999) {
            $response = new Response(
                json_encode(['status' => true]),
                Response::HTTP_OK
            );
        } else {
            $response = new Response(
                json_encode(['error' => 'Zipcode code not eligible.']),
                Response::HTTP_NOT_FOUND
            );
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Get(
     *     path = "/api/get-distributors-by-adress/{zipcode}",
     *     name = "api_get_distriburor_by_zipcode_bis",
     *     requirements = {"id"="[a-z0-9]+"}
     * )
     *
     * @SWG\Get(
     *     summary="Get the closest distributor for a given address",
     *     operationId="api_get_distriburor_by_address",
     *     tags={"Distributor"},
     *
     *      @SWG\Parameter(
     *          name="address",
     *          in="path",
     *          type="string",
     *          description="address"
     *      ),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Response ok",
     *          @SWG\Schema(
     *              type="object",
     *              example={
     *                  {
     *                      "id": "NR004",
     *                  }
     *              }
     *              )
     *     ),
     *
     *     @SWG\Response(
     *          response=401,
     *          description="Access Denied"
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Not Found"
     *     )
     *
     *     )
     */
    public function getDistributorForAddress(Request $request, $zipcode)
    {
        $ZipcodeFrRepository = $this->entityManager->getRepository(ZipcodeFr::class);
        $cp                  = $ZipcodeFrRepository->findOneByCodePostal($zipcode);
        
        if ( ! empty($cp)) {
            $DistributeursRepository = $this->entityManager->getRepository(Distributeurs::class);
            $distribs                = $DistributeursRepository->findDidtribByZipcodeFr($cp);

            if ( ! empty($distribs)) {
                if (intval(str_replace('km', '', $distribs[0]['distance'])) > 50) {
                    $response = new Response(json_encode(['id' => 'NR004']), Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                } else {
                    $response = new Response(json_encode(['id' => $distribs[0]['id']]), Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            } else {
                $response = new Response(json_encode(['id' => 'NR004']), Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }

            $serializer = $this->get('serializer');
            $response   = new Response(
                json_encode($distribs),
                Response::HTTP_OK
            );
            $response->headers->set('Content-Type', 'application/json');

            return $response;


        } else {
            return new Response(json_encode(['error' => 'Zipcode code not found.']), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Get(
     *     path = "/api/get-config",
     *     name = "api_get_config",
     *     requirements = {"id"="[a-z0-9]+"}
     * )
     *
     * @SWG\Get(
     *     summary="Get page configuration",
     *     operationId="api_get_config",
     *     tags={"Configuration"},
     *
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Response ok",
     *          @SWG\Schema(
     *              type="object",
     *              example={
     *                  {
     *
     *                  }
     *              }
     *              )
     *     ),
     *
     *     @SWG\Response(
     *          response=401,
     *          description="Access Denied"
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Not Found"
     *     )
     *
     *     )
     */
    public function getConfig(Request $request)
    {
        $conf = [];

        $conf['dataAccessory'][] = [
            'id'                => '1L0050320A',
            'label'             => "Casque Intégral G361 HEBO",
            'description'       => "Offert avec l'offre de lancement",
            'desktopImage'      => 'images/casque_integral.png',
            'mobileImage'       => 'images/casque_integral.png',
            'configuratorImage' => 'images/casque_intégral.png',
            'price'             => '140',
            'isFree'            => true,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0051435',
            'label'             => "Support Smartphone",
            'description'       => "Offert avec l'offre de lancement",
            'desktopImage'      => 'images/support_pour_smartphone.png',
            'mobileImage'       => 'images/support_pour_smartphone_small.png',
            'configuratorImage' => 'images/support_pour_smartphone.png',
            'price'             => '60',
            'isFree'            => true,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0061121',
            'label'             => "Malle 39 litres",
            'description'       => "",
            'desktopImage'      => 'images/coffre_arriere.png',
            'mobileImage'       => 'images/coffre_arriere.png',
            'configuratorImage' => 'images/coffre_arriere.png',
            'price'             => '150',
            'isFree'            => false,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0084342E',
            'label'             => "Gants HEBO City Eté",
            'description'       => "Offert avec l'offre de lancement",
            'desktopImage'      => 'images/gants_d-ete.png',
            'mobileImage'       => 'images/gants_d-ete_small.png',
            'configuratorImage' => 'images/gants_d’été.png',
            'price'             => '35',
            'isFree'            => true,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0071267A',
            'label'             => "Antivol pour scooter",
            'description'       => "Offert avec l'offre de lancement",
            'desktopImage'      => 'images/antivol.png',
            'mobileImage'       => 'images/antivol.png',
            'configuratorImage' => 'images/antivol.png',
            'price'             => '30',
            'isFree'            => true,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0052100',
            'label'             => "Smart light",
            'description'       => "",
            'desktopImage'      => 'images/lumiere_arriere.png',
            'mobileImage'       => 'images/lumiere_arriere.png',
            'configuratorImage' => 'images/lumiere_arriere.png',
            'price'             => '90',
            'isFree'            => false,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0084503',
            'label'             => "Tablier",
            'description'       => "",
            'desktopImage'      => 'images/blanket.jpg',
            'mobileImage'       => 'images/blanket_mobile.jpg',
            'configuratorImage' => 'blanket_mobile.jpg',
            'price'             => '130',
            'isFree'            => false,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0064160',
            'label'             => "Pare-brise haute protection",
            'description'       => '',
            'desktopImage'      => 'images/bulle.jpg',
            'mobileImage'       => 'images/bulle_mobile.jpg',
            'configuratorImage' => '',
            'price'             => '210',
            'isFree'            => false,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0071101',
            'label'             => "Support de malle",
            'description'       => '',
            'desktopImage'      => 'images/trunksupport.jpg',
            'mobileImage'       => 'images/trunksupport_mobile.jpg',
            'configuratorImage' => 'trunksupport_mobile.jpg',
            'price'             => '140',
            'isFree'            => false,
        ];

        $conf['dataAccessory'][] = [
            'id'                => '1L0050320E',
            'label'             => "Casque JET G262 HEBO",
            'description'       => '',
            'desktopImage'      => 'images/jet helmet.jpg',
            'mobileImage'       => 'images/jet helmet_mobile.jpg',
            'configuratorImage' => 'jet helmet_mobile.jpg',
            'price'             => '110',
            'isFree'            => false,
        ];


        /*$conf['dataAccessory'][]=[
           'id'=>'1L0084342A',
           'label'=>"Gants HEBO City Hiver",
           'description'=>'',
           'desktopImage'=>'',
           'mobileImage'=>'',
           'configuratorImage'=>'',
           'price'=>'50',
           'isFree'=>false,
       ];
       
       

       
       $conf['dataAccessory'][]=[
           'id'=>'1L0915115',
           'label'=>"Batterie eScooter en accessoires",
           'description'=>'',
           'desktopImage'=>'',
           'mobileImage'=>'',
           'configuratorImage'=>'',
           'price'=>'3400',
           'isFree'=>false,
       ];
      




1L0052100	Smart light	90 €
1L0050320A	Casque Intégral G361 HEBO Taille S	140 €
1L0050320B	Casque Intégral G361 HEBO Taille M	140 €
1L0050320C	Casque Intégral G361 HEBO Taille L	140 €
1L0050320D	Casque Intégral G361 HEBO Taille XL	140 €
1L0050320E	Casque JET G262 HEBO Taille S	110 €
1L0050320F	Casque JET G262 HEBO Taille M	110 €
1L0050320G	Casque JET G262 HEBO Taille L	110 €
1L0050320H	Casque JET G262 HEBO Taille XL	110 €
1L0084342A	Gants HEBO City Hiver Taille S	50 €
1L0084342B	Gants HEBO City Hiver Taille M	50 €
1L0084342C	Gants HEBO City Hiver Taille L	50 €
1L0084342D	Gants HEBO City Hiver Taille XL	50 €
1L0084342E	Gants HEBO City Eté Taille S	35 €
1L0084342F	Gants HEBO City Eté Taille M	35 €
1L0084342G	Gants HEBO City Eté Taille L	35 €
1L0084342H	Gants HEBO City Eté Taille XL	35 €
1L0915115	Batterie eScooter en accessoires	3.400 €

        * */
        $serializer = $this->get('serializer');
        $response   = new Response(
            json_encode($conf),
            Response::HTTP_OK
        );
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function requestToApi($type, $data) {
        $ch   = curl_init();
        curl_setopt($ch, CURLOPT_URL,
        $this->parameterBag->get('leadconnect-url') . '?' . urlencode(json_encode($data)));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, false);
        $response  = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code == 200) {
            if ($type == 'step') {
                return $this->redirectToRoute('financement-pre-commande-leadconnect');
            } else {
                return $this->redirectToRoute('contact-leadconnect');
            }
        }
    }

    /**
     * @Get(
     *     path = "/api/leadconnect",
     *     name = "leadconnect",
     * )
     */
    public function leadConnect(Request $request)
    {
        $campaignId = $this->parameterBag->get('send-lead-campaign-id');

        if (!$request->get('dealer')) {
            $checkZipcode = $this->getDistributorForAddress($request, $request->get('zipcode'));
            if ($checkZipcode->getStatusCode() == 200) {
                $dealer = json_decode($checkZipcode->getContent())->id;
                $data = [
                    'dealer'        => $dealer,
                    'action'        => 'lead',
                    'campaignid'    => $campaignId,
                    'model'         => $request->get('model'),
                    'civility'      => $request->get('civilite'),
                    'last_name'     => $request->get('nom'),
                    'first_name'    => $request->get('prenom'),
                    'email'         => $request->get('email'),
                    'zip_code'      => $request->get('zipcode'),
                    'phone'         => str_replace(' ', '', $request->get('phone')),
                    'privacy_email' => $request->get('privacy_email'),
                    'comment'       => $request->get('comment'),
                    'created_at'    => date('Y/m/d H:m:s')
                ];
                return $this->requestToApi($request->get('type'), $data);
            }
        } else {
            $data = [
                'dealer'        => $request->get('dealer'),
                'action'        => 'lead',
                'campaignid'    => $campaignId,
                'model'         => $request->get('model'),
                'civility'      => $request->get('civilite'),
                'last_name'     => $request->get('nom'),
                'first_name'    => $request->get('prenom'),
                'email'         => $request->get('email'),
                'zip_code'      => $request->get('zipcode'),
                'phone'         => str_replace(' ', '', $request->get('phone')),
                'privacy_email' => $request->get('privacy_email'),
                'comment'       => $request->get('comment'),
                'created_at'    => date('Y/m/d H:m:s')
            ];
            return $this->requestToApi($request->get('type'), $data);
        }
    }

    /**
     * @Post(
     *     path = "/api/process-configurator-contact",
     *     name = "process_configurator_contact",
     * )
     */
    public function processConfiguratorContactData(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $customerApplication = new CustomersApplication();
        $escooterConfiguration = new EscooterConfiguration();

        $cp = $doctrine->getRepository(ZipcodeFr::class)->findOneByCodePostal($request->get('zipcode', 75001));

        $distributeursRepository = $doctrine->getRepository(Distributeurs::class);
        $distribs                = $distributeursRepository->findDidtribByZipcodeFr($cp);

        if ($request->get('dealer')) {
            // Case option dealer checked
            $distributeur = $distributeursRepository->findOneBy(['kvps' => $request->get('dealer')]);
            $customerApplication->setDistributeurs($distributeur);
        } else {
            // Case option dealer not checked
            foreach ($distribs as $item) {
                $distributeur = $distributeursRepository->findOneBy(['kvps' => $item['kvps']]);
                $customerApplication->setDistributeurs($distributeur);
            }
        }

        $customerApplication->setApplicationState(0);
        $customerApplication->setZipcode($request->get('zipcode'));

        $escooterConfiguration->setIdApplication($customerApplication);
        $escooterConfiguration->setVehicleBrand('seat');
        $escooterConfiguration->setVehicleEngine($request->get('gamme', '125mo'));
        $escooterConfiguration->setVehicleModel($request->get('model', 'S0AAA3'));
        $escooterConfiguration->setVehicleKm(100); // need replace
        $escooterConfiguration->setVehicleKmAnnual(intval($request->get('kilometer', '15000'))); // need replace
        $escooterConfiguration->setVehicleKmTotal(100); // need replace
        $escooterConfiguration->setVehicleKmPrice(100); // need replace
        $escooterConfiguration->setColorName($request->get('colorName', 'Rouge Pamplona'));

        $dataAccessory = $request->get('dataAccessory', '');
        $escooterConfiguration->setVehicleOptions($dataAccessory);
        $escooterConfiguration->setVehicleOptionsPrice($request->get('totalPriceOption', 0));
        $escooterConfiguration->setLoaPrice(100); // need replace
        $escooterConfiguration->setPrice(100); // need replace
        $escooterConfiguration->setOptinInsuranceGcm($request->get('gcm', false));
        $escooterConfiguration->setOptinInsuranceVdr($request->get('vdr', false));
        $escooterConfiguration->setAssurance($request->get('assurance') == 'oui' ? 1 : 0);

        $escooterConfiguration->setLoaDuree($request->get('loaDuree', ''));
        $escooterConfiguration->setLoaLoyers($request->get('loaLoyers', ''));
        $escooterConfiguration->setPrixAPartirDe($request->get('prixAPartirDe', ''));
        $escooterConfiguration->setMontantTotalDu($request->get('montantTotalDu', ''));
        $escooterConfiguration->setOptionAchat($request->get('optionAchat', ''));
        $escooterConfiguration->setLoyerMajore($request->get('loyerMajore', ''));
        $escooterConfiguration->setPrixCatalogue($request->get('prixCatalogue', ''));

        $entityManager = $doctrine->getManager();
        $entityManager->persist($customerApplication);
        $entityManager->persist($escooterConfiguration);
        $entityManager->flush();
        $customerApplicationId = $customerApplication->getIdApplication();

        $data = [
            'model' => $request->get('model'),
            'zip_code' => $request->get('zipcode'),
            'permit' => $request->get('permit', 'F'),
            'privacy_email' => $request->get('privacy_email', 'F'),
            'dataAccessory' => $request->get('dataAccessory'),
            'totalPriceOption' => $request->get('totalPriceOption'),
            'gcm' => $request->get('gcm'),
            'vdr' => $request->get('vdr'),
            'dealer' => $request->get('dealer'),
            'personne-majeure' => $request->get('personne-majeure', 'F')
        ];

        return $this->redirect($this->generateUrl('financement-donnees-personnelles', ['formData' => json_encode($data), 'customerApplication' => $customerApplicationId]));
    }

    /**
     * @Get(
     *     path = "/api/get-distributeur-promotion",
     *     name = "get_distributeur_promotion",
     * )
     */
    public function getDistributeursPromotion(Request $request)
    {
        $kvps = ['FR967'];
        $distributeursRepository = $this->entityManager->getRepository(Distributeurs::class);
        $distributeurs = $distributeursRepository->findDidtribByKvps($kvps);

        $distributeursArray = [];
        foreach ($distributeurs as $distributeur) {
            $distributeursArray[] = [
                'lat'        => $distributeur->getLatitude(),
                'lng'        => $distributeur->getLongitude(),
                'kvps'       => $distributeur->getKvps(),
                'name'       => $distributeur->getRaisonSociale(),
                'address'    => $distributeur->getAdresse(),
                'postalCode' => $distributeur->getCodePostal(),
                'ville'      => $distributeur->getVille(),
            ];
        }

        return $this->json([
            'success'  => true,
            'items'    => $distributeursArray,
            'template' => $this->render('default/parts/distributeur-choice.html.twig',[
                'items' => $distributeursArray,
            ])
        ]);
    }

    /**
     * @Get(
     *     path = "/api/get-ville-by-postal-code/{cp}",
     *     name = "get_ville_by_postal_code",
     * )
     */
    public function getVilleByPostalCode($cp): Response
    {
        $url = "https://datanova.laposte.fr/data-fair/api/v1/datasets/laposte-hexasmal/lines?q=" . urlencode($cp) . "&";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->CURLOPT_TIMEOUT);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0');
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);

        $response = json_decode($response);
        $ret = [];
        if ($response->results) {
            $ville = $response->results[0]->libelle_d_acheminement;

            $ret = [
                'ville'   => $ville,
                'success' => true
            ];
        } else {
            $ret = [
                'ville'   => '',
                'message' => 'Aucune ville ne correspond au code postal que vous avez saisi',
                'success' => false
            ];
        }

        return $this->json($ret);
    }
}
