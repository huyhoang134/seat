<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sonata\AdminBundle\Controller\CRUDController;


use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use App\Entity\ImportLogs;
use App\Entity\Distributeurs;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;

class ImportController extends CRUDController
{
    
    
    public function listAction()
    {
        
        
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }
        
        $request = $this->getRequest();
        $locale = $this->getRequest()->getLocale();
        

       
        
        $formBuilder = $this->createFormBuilder(null, [
            /*'constraints' => [new Callback([$this, 'formValidate'])]*/
        ]);
        
        
        $formBuilder
        ->add('distributeurs', CheckboxType::class, [
            'label' => 'Distributeurs',
            'mapped' => false,
            'required' => false,
        ])
        
        
        
        
      
        ->add('submit', SubmitType::class, [
            'label' => $this->get('translator')->trans('Valider')
        ]);
        ;
      
        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            
            
            
            if($form->isValid()){
                $formData = $this->getRequest()->request->get('form');
                $DistributeursRepositoty = $this->container->get('doctrine.orm.entity_manager')->getRepository(Distributeurs::class);
                $data=$form->all();
                
                //$DistributeursRepositoty->import_zipcode();
                
                $f=$data['distributeurs']->getData();
                if(!empty($f)){
                   
                    $api_url = "https://www.seat-leadconnect.fr/webservices/dealers/json?mode=eScooter";
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_URL => $api_url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 5,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        /*CURLOPT_POSTFIELDS => json_encode($data),
                        CURLOPT_HTTPHEADER => array(
                            "Accept: application/json",
                            "Authorization: Bearer " . $access_token,
                            "Cache-Control: no-cache",
                            "Content-Type: application/json",
                        )*/
                    ));
                    
                    
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $return_object=['error'=>false, 'object'=>false];
                    if ($err) {
                        $this->addFlash('error', @$err);
                        $return_object=['error'=>true, 'object'=>@$err];
                    } else {
                        $data = json_decode($response);
                        file_put_contents("./import/dealers.json",$response);
                        
                        $current_user = $this->container->get('security.token_storage')->getToken()->getUser()->__toString();
                        
                        $entityManager = $this->container->get('doctrine')->getManager();
                        $ImportLogs = new ImportLogs();
                        $ImportLogs->setType('Distributeurs');
                        $ImportLogs->setAutheur($current_user);
                        $ImportLogs->setDate(new \DateTime());
                        
                        $mediaManager = $this->container->get("sonata.media.manager.media");
                        $media = new \App\Application\Sonata\MediaBundle\Entity\Media();
                        $media->setBinaryContent("./import/dealers.json");
                        $media->setName("dealers.json");
                        $media->setContext('Distributeurs');
                        $media->setProviderName('sonata.media.provider.file');
                        $media->setProviderMetadata(['filename'=>"dealers.json", 'description'=>"https://www.seat-leadconnect.fr/webservices/dealers/json"]);
                        
                        $mediaManager->save($media);
                        
                        //fix pour setter le bon nom de fichier au download
                        /*$repo = $this->container->get('doctrine')->getRepository(\App\Application\Sonata\MediaBundle\Entity\Media::class);
                        $media = $repo->find($media->getId());
                        $media->setProviderMetadata(['filename'=>$file_name]);
                        $mediaManager->save($media);*/

                        $ImportLogs->setFichier($media);
                        $logs = $DistributeursRepositoty->importDistributeurs($data);
                        $txt="";
                        foreach($logs as $i=>$l){
                            $txt.=$l."<br>";
                        }
                        $ImportLogs->setLogs($txt);
                        $entityManager->persist($ImportLogs);
                        $entityManager->flush();
  
                    }  
                   
                    $this->addFlash('success', $this->get('translator')->trans('Procédure effectuée.'));
                }else{
                    $this->addFlash('success', $this->get('translator')->trans('Import des distributeurs ignoré.'));
                }
                
               
               
            }else{
                foreach($form->getErrors(true, false) as $er) {
                    $this->addFlash('error', $er->__toString());
                }
            }

        }
        
        
        return $this->render('Admin/import/listAction.html.twig', [
            'controller_name' => 'ParametresController',
            'form' => $form->createView()
        ]);
    }
    

    
    
    
 
}
