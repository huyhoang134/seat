<?php

namespace App\Controller;

use App\Entity\CustomersApplication;
use App\Entity\EscooterConfiguration;
use App\Services\BankPushService;
use App\Services\EmailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EmailController extends AbstractController
{
    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @var BankPushService
     */
    private $bankPushService;

    private $entityManager;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(EmailService $emailService, EntityManagerInterface $entityManager, BankPushService $bankPushService, ParameterBagInterface $parameterBag)
    {
        $this->emailService    = $emailService;
        $this->entityManager   = $entityManager;
        $this->bankPushService = $bankPushService;
        $this->parameterBag    = $parameterBag;
    }

    /**
     * @Route("/recorded-information.html", name="recorded-information")
     */
    public function sendMailRecordedInformation(Request $request): Response
    {
        $customerApplicationId = $request->get('application_id');

        if (!$customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        // Send Lead
        if ($request->get('is-send-lead')) {
            $customerApplication = $this->entityManager->getRepository(CustomersApplication::class)->find($customerApplicationId);
            $campaignId          = $this->parameterBag->get('send-lead-and-send-mail-campaign-id');

            $this->bankPushService->leadConnect($customerApplication, $campaignId);
        }
        //

        $data = $this->emailService->getEmailAndEncryptionId($customerApplicationId);

        $toEmail                 = $data['email'];
        $subject                 = 'Votre justificatif de domicile';
        $callBackUrl             = $this->generateUrl('financement-justificatif-de-domicile',
            ['encryption_application_id' => $data['encryptionId']],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $customerApplicationName = $this->emailService->getNameByCustomersApplicationId($customerApplicationId);
        $escooterConfiguration = $this->entityManager->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        $colorName = $escooterConfiguration->getColorName();

        $this->emailService->sendMail($toEmail, $customerApplicationName, $callBackUrl, $colorName, $subject);

        return $this->render('default/financement-recorded-information.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Informations enregistrées',
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/check-online-later.html", name="check-online-later")
     */
    public function sendMailCheckOnlineLater(Request $request): Response
    {
        $customerApplicationId = $request->get('application_id');

        if (!$customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        // Send Lead
        if ($request->get('is-send-lead')) {
            $customerApplication = $this->entityManager->getRepository(CustomersApplication::class)->find($customerApplicationId);
            $campaignId          = $this->parameterBag->get('send-lead-and-send-mail-campaign-id');

            $this->bankPushService->leadConnect($customerApplication, $campaignId);
        }
        //

        $data = $this->emailService->getEmailAndEncryptionId($customerApplicationId);

        $toEmail                 = $data['email'];
        $subject                 = 'Échéance & RIB';
        $callBackUrl             = $this->generateUrl('algoan-bank-interface',
            ['encryption_application_id' => $data['encryptionId']],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $customerApplicationName = $this->emailService->getNameByCustomersApplicationId($customerApplicationId);
        $escooterConfiguration = $this->entityManager->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        $colorName = $escooterConfiguration->getColorName();

        $this->emailService->sendMail($toEmail, $customerApplicationName, $callBackUrl, $colorName, $subject);

        return $this->render('default/financement-recorded-information.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Informations enregistrées',
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/verify-your-identity-later.html", name="verify-your-identity-later")
     */
    public function sendMailVerifyYourIdentityLater(Request $request): Response
    {
        $customerApplicationId = $request->get('application_id');

        if (!$customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        // Send Lead
        if ($request->get('is-send-lead')) {
            $customerApplication = $this->entityManager->getRepository(CustomersApplication::class)->find($customerApplicationId);
            $campaignId          = $this->parameterBag->get('send-lead-and-send-mail-campaign-id');

            $this->bankPushService->leadConnect($customerApplication, $campaignId);
        }
        //

        $data = $this->emailService->getEmailAndEncryptionId($customerApplicationId);

        $toEmail                 = $data['email'];
        $subject                 = 'Vérifions votre identité en 3 minutes';
        $callBackUrl             = $this->generateUrl('financement-munissez-telephone',
            ['encryption_application_id' => $data['encryptionId']],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $customerApplicationName = $this->emailService->getNameByCustomersApplicationId($customerApplicationId);
        $escooterConfiguration = $this->entityManager->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        $colorName = $escooterConfiguration->getColorName();

        $this->emailService->sendMail($toEmail, $customerApplicationName, $callBackUrl, $colorName, $subject);

        return $this->render('default/financement-recorded-information.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Informations enregistrées',
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }
}
