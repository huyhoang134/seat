<?php

namespace App\Controller;

use App\Entity\CustomersApplication;
use App\Entity\EscooterConfiguration;
use App\Entity\WebhookLogs;
use App\Services\BankPushService;
use App\Services\EmailService;
use App\Services\WebHookService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class WebhookController extends AbstractController
{
    /**
     * @var WebHookService 
     */
    private $webHookService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @var BankPushService
     */
    private $bankPushService;

    public function __construct(
        WebHookService $webHookService,
        EntityManagerInterface $entityManager,
        EmailService $emailService,
        BankPushService $bankPushService
    ) {
        $this->webHookService = $webHookService;
        $this->entityManager = $entityManager;
        $this->emailService = $emailService;
        $this->bankPushService = $bankPushService;
    }

    /**
     * @Route("/api/webhook", name="app_webhook", methods={"POST"})
     */
    public function index(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $webHook = $this->webHookService->saveWebHookLogs($request->getContent());

        if ($data && array_key_exists('caseId', $data)) {
            $webHook->setCaseId($data['caseId']);
            
            $this->entityManager->flush();
        }

        if (
            ($data && array_key_exists('caseId', $data) && array_key_exists('type', $data) && (strtolower($data['type']) == 'journeyended' || str_contains(strtolower($data['type']), 'feb')))
            || ($data && array_key_exists('caseId', $data) && array_key_exists('decision', $data) && (strtolower($data['decision']) == 'valid' || strtolower($data['decision']) == 'incomplete' || strtolower($data['decision']) == 'fraud'))
        ) {
            $this->saveCustomerApplicationIdToWebHookLogs($data, $webHook);
        }

        return $this->json(['message' => 'Webhook data received and processed']);
    }

    /**
     * @Route("/webhook", name="app_webhook1")
     */
    public function index1(): Response
    {
        return $this->render('webhook/index.html.twig', [
            'controller_name' => 'WebhookController',
        ]);
    }

    private function sendMailConnectAgain(int $customerApplicationId)
    {
        $data = $this->getDataSendMail($customerApplicationId);

        $this->emailService->sendMail($data['toEmail'], $data['customerApplicationName'], $data['callBackUrl'], $data['colorName']);
    }

    private function saveCustomerApplicationIdToWebHookLogs($data, $webHook)
    {
        $caseId      = $data['caseId'];
        $webHookLogs = $this->entityManager->getRepository(WebhookLogs::class)->findBy(['caseId' => $caseId]);

        /** @var WebhookLogs $webHookLog */
        foreach ($webHookLogs as $webHookLog) {
            $customerApplicationId = $webHookLog->getCustomerApplication();

            if ($customerApplicationId) {
                $customerApplication = $this->entityManager->getRepository(CustomersApplication::class)->find($customerApplicationId);

                if ($customerApplication) {
                    $webHook->setCustomerApplication($customerApplication->getIdApplication());
                    $this->entityManager->flush();
                    $eventData = json_decode($webHookLog->getEventData(), true);

                    if (
                        (array_key_exists('type', $eventData) && strtolower($eventData['type']) == 'journeyended' && strtolower($data['type']) == 'journeyended') ||
                        (array_key_exists('decision', $eventData) && strtolower($eventData['decision']) == 'valid' && strtolower($data['decision']) == 'valid')
                    ) {
                        //Send mail
                     //   $this->sendMailConnectAgain($customerApplicationId);

                        //Go to next step
                        return $this->redirectToRoute('successful-verification', ['customerApplication' => $customerApplicationId]);
                    }

                    // Send mail user FEBKO
                    if (
                        (array_key_exists('type', $eventData) && ((strtolower($eventData['type']) == 'febko' && strtolower($data['type']) == 'febko'))) ||
                        (array_key_exists('decision', $eventData) &&
                        ((strtolower($eventData['decision']) == 'incomplete' && strtolower($data['decision']) == 'incomplete' && array_key_exists('decisionReason', $eventData) && strtolower($data['decisionReason']) == 'blurriness') ||
                        (strtolower($eventData['decision']) == 'fraud' && strtolower($data['decision']) == 'fraud' && array_key_exists('decisionReason', $eventData) && strtolower($data['decisionReason']) == 'falseid'))))
                    {
                        $this->sendMailFEBKO($customerApplicationId);
                    }
                    //

                    // Send mail user journey
                    if (
                        (array_key_exists('type', $eventData) && ((strtolower($eventData['type']) == 'febok' && strtolower($data['type']) == 'febok') ||
                         ((strtolower($eventData['type']) == 'febko' && strtolower($data['type']) == 'febko') && array_key_exists('febAssessmentCode', $eventData) && (str_contains($data['febAssessmentCode'], 'ID_OK_FORM_MISMATCH')) && (str_contains($eventData['febAssessmentCode'], 'ID_OK_FORM_MISMATCH')))))
                        || (array_key_exists('decision', $eventData) &&
                            (((strtolower($eventData['decision']) == 'valid' && strtolower($data['decision']) == 'valid' && array_key_exists('decisionReason', $eventData) && strtolower($data['decisionReason']) == 'isoinput'))  ||
                            (strtolower($eventData['decision']) == 'incomplete' && strtolower($data['decision']) == 'incomplete' && array_key_exists('decisionReason', $eventData) && strtolower($data['decisionReason']) == 'blurriness')))
                    ) {
                        $sessionId = array_key_exists('sessionId', $eventData) ? $eventData['sessionId'] : null;

                        $customerApplication->setIsKycStatusFebOK(true);
                        $customerApplication->setKycSessionId($sessionId);
                        $this->entityManager->persist($customerApplication);
                        $this->entityManager->flush();

                        if ($customerApplication->getIsBankPushStatusAggregationSuccess()) {
                            $this->bankPushService->summarizeUserJourney($customerApplication, $sessionId);
                        }

                        break;
                    }
                }
            }
        }
    }

    private function sendMailFEBKO(int $customerApplicationId)
    {
        $data = $this->getDataSendMail($customerApplicationId);

        $this->emailService->sendMailFEBKO($data['toEmail'], $data['customerApplicationName'], $data['callBackUrl'], $data['colorName']);
    }

    private function getDataSendMail($customerApplicationId): array
    {
        $data = $this->emailService->getEmailAndEncryptionId($customerApplicationId);

        $toEmail                 = $data['email'];
        $callBackUrl             = $this->generateUrl('successful-verification',
            ['encryption_application_id' => $data['encryptionId']],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $customerApplicationName = $this->emailService->getNameByCustomersApplicationId($customerApplicationId);
        $escooterConfiguration = $this->entityManager->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        $colorName = $escooterConfiguration->getColorName();

        return [
            'toEmail'                 => $toEmail,
            'callBackUrl'             => $callBackUrl,
            'customerApplicationName' => $customerApplicationName,
            'colorName'               => $colorName
        ];
    }
}
