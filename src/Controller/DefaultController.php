<?php

namespace App\Controller;

use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\Distributeurs;
use App\Entity\WebhookLogs;
use App\Entity\CustomersAlgoan;
use App\Entity\CustomersApplication;
use App\Entity\EscooterConfiguration;
use App\Services\AlgoanService;
use App\Services\EncryptionService;
use App\Entity\CustomersMedia;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sonata\MediaBundle\Entity\MediaManager;
use App\Services\WebHookService;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\Writer\PngWriter;
use App\Services\BankPushService;



class DefaultController extends AbstractController
{
    /**
     * @var MediaManager 
     */
    private $mediaManager;

    /**
     * @var WebHookService
     */
    private $webHookService;
    
    /**
     * @var EncryptionService
     */
    private $encryptionService;

    /**
     * @var BankPushService 
     */
    private $bankPushService;

    /**
     * @var ParameterBagInterface 
     */
    private $parameterBag;

    /**
     * @var AlgoanService 
     */
    private $algoanService;

    public function __construct(
        MediaManager $mediaManager,
        EncryptionService $encryptionService,
        WebHookService $webHookService,
        BankPushService $bankPushService,
        ParameterBagInterface $parameterBag,
        AlgoanService $algoanService
    ) {
        $this->mediaManager      = $mediaManager;
        $this->encryptionService = $encryptionService;
        $this->webHookService    = $webHookService;
        $this->bankPushService   = $bankPushService;
        $this->parameterBag      = $parameterBag;
        $this->algoanService     = $algoanService;
    }

    /**
     * @Route("/", name="default")
     */
    public function indexHtml()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'index.html',
            'title' => 'homepage',
        ]);
    }
    
    /**
     * @Route("/contact.html", name="contact.html")
     */
    public function contact(Request $request)
    {
        $distributeursRepository = $this->container->get('doctrine')->getRepository(Distributeurs::class);

        $distributeur = $distributeursRepository->findOneBy(['kvps' => $request->get('dealer')]);
        $dealerName   = $distributeur ? $distributeur->getRaisonSociale() : '';

        return $this->render('default/contact.html.twig', [
            'controller_name' => 'default',
            'title'           => 'Contact',
            'dealerName'      => $dealerName,
            'dealerCode'      => $request->get('dealer'),
            'gamme'           => $request->get('gamme') ? $request->get('gamme') : '125mo',
        ]);
    }



    /**
     * @Route("/configurator.html", name="configurator.html")
     */
    public function configurator(Request $request): Response
    {
        return $this->render('default/configurator.html.twig', [
            'controller_name' => 'default',
            'title'           => 'Configurateur',
            'gamme'           => $request->get('gamme') ? $request->get('gamme') : '125mo'
        ]);
    }

    /**
     * @Route("/financement-pre-commande.html", name="financement-pre-commande.html")
     */
    public function financementPreCommande(Request $request): Response
    {
        return $this->render('default/financement-pre-commande.html.twig', [
            'controller_name' => 'default',
            'title'           => 'Financement pre-commande',
            'gamme'           => $request->get('gamme') ? $request->get('gamme') : '125mo'
        ]);
    }

    /**
     * @Route("/financement-pre-commande-leadconnect.html", name="financement-pre-commande-leadconnect")
     */
    public function financementPreCommandeLeadConnect(): Response
    {
        return $this->render('default/financement-pre-commande-leadconnect.html.twig', [
            'controller_name' => 'default',
            'title' => 'Financement pre-commande',
        ]);
    }

    /**
     * @Route("/contact-leadconnect.html", name="contact-leadconnect")
     */
    public function contactLeadConnect(): Response
    {
        return $this->render('default/contact-leadconnect.html.twig', [
            'controller_name' => 'default',
            'title' => 'Contact',
        ]);
    }
    
    /**
     * @Route("/financement-donnees-personnelles.html", name="financement-donnees-personnelles", methods={"GET"})
     */
    public function financementDonneesPersonnelles(Request $request): Response
    {
        $session = $request->getSession();
        $data=$session->get("form-step1-3");
        $customerApplicationId = $request->get('customerApplication');
        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        if(empty($data)){
            $data=[
                "customerApplication"=>$request->get("customerApplication"),
                "civilite"=>$request->get("civilite"),
                "nom"=>$request->get("nom"),
                "prenom"=>$request->get("prenom"),
                "maiden-name"=>$request->get("maiden-name"),
                "date-of-birth"=>$request->get("date-of-birth"),
                "city-of-birth"=>$request->get("city-of-birth"),
                "code-postal-naissance"=>$request->get("code-postal-naissance"),
                "address"=>$request->get("address"),
                "additional-address"=>$request->get("additional-address"),
                "code-postal"=>$request->get("code-postal"),
                "city"=>$request->get("city"),
                "country"=>$request->get("country"),
                "France"=>$request->get("France"),
                "nationality"=>$request->get("nationality"),
                "principal-residence"=>$request->get("principal-residence"),
                "family-status"=>$request->get("family-status"),
                "number-of-dependent-children"=>$request->get("number-of-dependent-children"),
                "occupation"=>$request->get("occupation"),
                "type-of-contract"=>$request->get("type-of-contract"),
                "hiring-date"=>$request->get("hiring-date"),
                "email"=>$request->get("email"),
                "phone"=>$request->get("phone"),
                "employeur"=>$request->get("employeur"),
                "revenu-net-mensuel"=>$request->get("revenu-net-mensuel"),
                "autres-revenus"=>$request->get("autres-revenus"),
                "charges-immobilieres"=>$request->get("charges-immobilieres"),
                "autres-remboursements-mensuels"=>$request->get("autres-remboursements-mensuels"),
            ];
        }
        return $this->render('default/financement-donnees-personnelles.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement donnees personnelles',
            'data'                  => $data,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/financement-donnees-personnelles.html", name="financement-donnees-personnellesPost", methods={"POST"})
     */
    public function financementDonneesPersonnellesPOST(Request $request): Response
    {

        $data=[
            "customerApplication"=>$request->get("customerApplication"),
            "civilite"=>$request->get("civilite"),
            "nom"=>$request->get("nom"),
            "prenom"=>$request->get("prenom"),
            "maiden-name"=>$request->get("maiden-name"),
            "date-of-birth"=>$request->get("date-of-birth"),
            "city-of-birth"=>$request->get("city-of-birth"),
            "code-postal-naissance"=>$request->get("code-postal-naissance"),
            "address"=>$request->get("address"),
            "additional-address"=>$request->get("additional-address"),
            "code-postal"=>$request->get("code-postal"),
            "city"=>$request->get("city"),
            "country"=>$request->get("country"),
            "France"=>$request->get("France"),
            "nationality"=>$request->get("nationality"),
            "principal-residence"=>$request->get("principal-residence"),
            "family-status"=>$request->get("family-status"),
            "number-of-dependent-children"=>$request->get("number-of-dependent-children"),
            "occupation"=>$request->get("occupation"),
            "type-of-contract"=>$request->get("type-of-contract"),
            "hiring-date"=>$request->get("hiring-date"),
            "email"=>$request->get("email"),
            "phone"=>$request->get("phone"),
            "employeur"=>$request->get("employeur"),
            "revenu-net-mensuel"=>$request->get("revenu-net-mensuel"),
            "autres-revenus"=>$request->get("autres-revenus"),
            "charges-immobilieres"=>$request->get("charges-immobilieres"),
            "autres-remboursements-mensuels"=>$request->get("autres-remboursements-mensuels"),
        ];


        /*
         * */
        $session = $request->getSession();
        $session->set("form-step1-3", $data);
        return $this->redirectToRoute('financement-validation');
        /*return $this->render('default/financement-donnees-personnelles.html.twig', [
            'controller_name' => 'default',
            'title' => 'Financement donnees personnelles',
        ]);*/
    }

    /**
     * @Route("/financement-validation.html", name="financement-validation")
     */
    public function financementValidation(Request $request): Response
    {
        $session = $request->getSession();
        $data=$session->get("form-step1-3");
        $customerApplicationId = $request->get('customerApplication');
        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        if(empty($data)){
            $data=[
                "civilite"=>$request->get("civilite"),
                "nom"=>$request->get("nom"),
                "prenom"=>$request->get("prenom"),
                "maiden-name"=>$request->get("maiden-name"),
                "date-of-birth"=>$request->get("date-of-birth"),
                "city-of-birth"=>$request->get("city-of-birth"),
                "code-postal-naissance"=>$request->get("code-postal-naissance"),
                "address"=>$request->get("address"),
                "additional-address"=>$request->get("additional-address"),
                "code-postal"=>$request->get("code-postal"),
                "city"=>$request->get("city"),
                "country"=>$request->get("country"),
                "France"=>$request->get("France"),
                "nationality"=>$request->get("nationality"),
                "principal-residence"=>$request->get("principal-residence"),
                "family-status"=>$request->get("family-status"),
                "number-of-dependent-children"=>$request->get("number-of-dependent-children"),
                "occupation"=>$request->get("occupation"),
                "type-of-contract"=>$request->get("type-of-contract"),
                "hiring-date"=>$request->get("hiring-date"),
                "email"=>$request->get("email"),
                "phone"=>$request->get("phone"),
                "employeur"=>$request->get("employeur"),
                "revenu-net-mensuel"=>$request->get("revenu-net-mensuel"),
                "autres-revenus"=>$request->get("autres-revenus"),
                "charges-immobilieres"=>$request->get("charges-immobilieres"),
                "autres-remboursements-mensuels"=>$request->get("autres-remboursements-mensuels"),
            ];
        }

        return $this->render('default/financement-validation.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement validation',
            'data'                  => $data,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }


    /**
     * @Route("/financement-prerequis.html", name="financement-prerequis")
     */
    public function financementPrerequis(Request $request): Response
    {
        $request->getSession()->remove("form-step1-3");

        $doctrine = $this->container->get('doctrine');
        if (!$request->get('id')) {
            return $this->redirectToRoute('default');
        }

        $customerApplicationId = $request->get('id');
        $customerApplication = $doctrine->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);

        if (!$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        $customerApplication->setCivility($request->get('civilite', 'mr'));
        $customerApplication->setName($request->get('prenom'));
        $customerApplication->setSurname($request->get('nom'));
        $customerApplication->setMaidenName($request->get('maiden-name'));
        $customerApplication->setDateOfBirth(new \DateTimeImmutable($request->get('date-of-birth')));
        $customerApplication->setCityBirth($request->get('city-of-birth'));
        $customerApplication->setZipcodeBirth($request->get('code-postal-naissance'));
        $customerApplication->setAdress($request->get('address'));
        $customerApplication->setAdress2($request->get('additional-address'));
        $customerApplication->setCity($request->get('city'));
        $customerApplication->setZipcode($request->get('code-postal'));
        $customerApplication->setNationality($request->get('nationality'));
        $customerApplication->setPhone(str_replace(' ', '', $request->get('phone')));
        $customerApplication->setEmail($request->get('email'));
        $customerApplication->setMainResidence($request->get('principal-residence'));
        $customerApplication->setFamilySitutation($request->get('family-status'));
        $customerApplication->setChilds($request->get('number-of-dependent-children'));
        $customerApplication->setOccupation($request->get('occupation'));
        $customerApplication->setHiringDate(new \DateTimeImmutable($request->get('hiring-date')));
        $customerApplication->setContractType($request->get('type-of-contract'));
        $customerApplication->setCountry($request->get('country'));
        $customerApplication->setEmployeur($request->get('employeur'));
        $customerApplication->setRevenuNetMensuel($request->get('revenu-net-mensuel'));
        $customerApplication->setAutresRevenus($request->get('autres-revenus'));
        $customerApplication->setAutresRevenus($request->get('autres-revenus'));
        $customerApplication->setChargesImmobilieres($request->get('charges-immobilieres'));
        $customerApplication->setAutresRemboursementsMensuels($request->get('autres-remboursements-mensuels'));

        $entityManager = $doctrine->getManager();
        $entityManager->persist($customerApplication);
        $entityManager->flush();

        return $this->render('default/financement-prerequis.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement pre-requis',
            'application_id'        => $customerApplicationId,
            'escooterConfiguration' => $escooterConfiguration,
            'customerApplication'   => $customerApplication
        ]);
    }

    /**
     * @Route("/financement-justificatif-de-domicile.html", name="financement-justificatif-de-domicile")
     */
    public function financementJustificatif(Request $request): Response
    {
        $doctrine = $this->container->get('doctrine');

        if (!$request->get('application_id') && !$request->get('encryption_application_id')) {
            return $this->redirectToRoute('default');
        }

        if ($request->get('encryption_application_id')) {
            // Set applicationId when link are clicked from gmail

            $applicationId = $this->encryptionService->decryption($request->get('encryption_application_id'));
        } else {
            // Set applicationId when link are clicked from within step 2.1

            $applicationId = $request->get('application_id');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        $customerMedia = $doctrine->getRepository(CustomersMedia::class)->findBy(['customersApplication' => $applicationId, 'type' => 'address']);
        $allFiles = [];
        if (!empty($customerMedia)) {
            foreach ($customerMedia as $item) {
                $files['id'] = $item->getMedia()->getId();
                $files['name'] = $item->getMedia()->getName();
                $files['path'] = $item->getMedia()->getProviderReference();
                $allFiles[] = $files;
            }
        }

        $customerApplication = $doctrine->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $applicationId]);

        if (!$customerApplication) {
            return $this->redirectToRoute('default');
        }

        return $this->render('default/financement-justificatif-de-domicile.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement Justificatif-de-domicile',
            'application_id'        => $applicationId,
            'files'                 => $allFiles,
            'isShowBackButton'      => $request->get('encryption_application_id') ? false : true,
            'escooterConfiguration' => $escooterConfiguration,
            'customerApplication'   => $customerApplication
        ]);
    }

    /**
     * @Route("/delete-file", name="delete-file")
     */
    public function deleteFile(Request $request) {
        $doctrine = $this->container->get('doctrine');
        $mediaId = $request->get('id');
        $type = $request->get('type');
        $media   = $doctrine->getRepository(Media::class)->findOneBy(['id' => $mediaId]);
        if ($media) {
            $customerMedia = $doctrine->getRepository(CustomersMedia::class)->findOneBy(['media' => $media, 'type' => $type]);
            if ($customerMedia) {
                $doctrine->getManager()->remove($media);
                $doctrine->getManager()->flush($media);
                $doctrine->getManager()->remove($customerMedia);
                $doctrine->getManager()->flush($customerMedia);
            }
        }
        return new JsonResponse(['message' => 'Delete successfully']);
    }

    /**
     * @Route("/financement-munissez-telephone-document.html", name="financement-munissez-telephone-document")
     */
    public function financementMunissezTelephoneDocument(Request $request): Response
    {
        if ( ! $request->get('application_id')) {
            return $this->redirectToRoute('default');
        }
        $images                = $request->files->get('files', []);
        $entityManager         = $this->container->get('doctrine')->getManager();
        $customerApplicationId = $request->get('application_id');
        $customerApplication   = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);
        if ( ! $customerApplication) {
            return $this->redirectToRoute('default');
        }

        if ( ! empty($images)) {
            // validate file size
            $fileSize = 0;
            foreach ($images as $image) {
                $fileSize += $image->getSize();
            }
            // max file size = 100 MB
            if ($fileSize > (100 * 1024 * 1024)) {
                return $this->redirectToRoute('financement-justificatif-de-domicile',
                    ['message' => 'La taille du fichier est trop grande. Veuillez vous assurer que la taille est inférieure à 100 Mo'],
                    307);
            } else {
                foreach ($images as $image) {
                    $media = new Media();
                    $media->setContext('default');
                    $media->setProviderName('sonata.media.provider.image');
                    $media->setBinaryContent($image);
                    $this->mediaManager->save($media);

                    $customerMedia = new CustomersMedia();
                    $customerMedia->setCustomersApplication($customerApplication);
                    $customerMedia->setMedia($media);
                    $customerMedia->setType('address');
                    $entityManager->persist($customerMedia);
                    $entityManager->flush($customerMedia);
                }
            }
        }

        return new JsonResponse(['application_id' => $request->get('application_id')]);
    }

    /**
     * @Route("/financement-munissez-telephone.html", name="financement-munissez-telephone")
     */
    public function financementMunissezTelephone(Request $request): Response
    {
        if ( ! $request->get('customerApplication') && ! $request->get('encryption_application_id')) {
            return $this->redirectToRoute('default');
        }

        if ($request->get('encryption_application_id')) {
            $applicationId = $this->encryptionService->decryption($request->get('encryption_application_id'));
        } else {
            $applicationId = $request->get('customerApplication');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        return $this->render('default/financement-munissez-telephone.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement Munissez-vous de votre téléphone',
            'application_id'        => $applicationId,
            'isShowBackButton'      => $request->get('encryption_application_id') ? false : true,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/financement-munissez-telephone-kyc-step-1.html", name="kyc-step-1")
     */
    public function kycStep1(Request $request): Response
    {
        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $request->get('application_id')]);
        if ( ! $customerApplication) {
            return $this->redirectToRoute('default');
        }
        
        //Bypass kyc for test next step
        /*if ($request->get('bypass') == 'kyc') {
            return $this->redirectToRoute('successful-verification', ['customerApplication' => $request->get('application_id')]);
        }*/

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $request->get('application_id')]);

        $reconection_url=$this->getKycUrls($request);

        return $this->render('default/kyc-step-1.html.twig', [
            'controller_name'              => 'default',
            'title'                        => 'Financement Munissez-vous de votre téléphone',
            'identification_id'            => $reconection_url['identification_id'],
            'identification_url'           => $reconection_url['identification_url'],
            'identification_url_reconnect' => $reconection_url['identification_url_reconnect'],
            'application_id'               => $request->get('application_id'),
            'escooterConfiguration'        => $escooterConfiguration
        ]);
    }

    public function getKycUrls($request){

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $request->get('application_id')]);



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('quicksign-url') . '/applicant/api/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'grant_type'    => 'client_credentials',
            'client_id'     => $this->parameterBag->get('quicksign-client-id'),
            'client_secret' => $this->parameterBag->get('quicksign-client-secret'),
        ]));

        $response = curl_exec($ch);
        $err      = curl_error($ch);

        curl_close($ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $ret          = json_decode($response);
            $access_token = $ret->access_token;

        }

        $quick_sign_contract = "quicksign/T&Cs.pdf";
        $url                 = $this->parameterBag->get('quicksign-url') . '/caseflow/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/cases/sync';

        $gender = 'male';
        $honorificPrefix = 'Mr';

        if ($customerApplication->getCivility() == 'mme') {
            $gender = 'female';
            $honorificPrefix = 'Ms';
        }

        $phoneNumber = $customerApplication->getPhone();

        $phoneNumber = preg_replace('/^0/', '', $phoneNumber);

        $user_info = [
            'applicantClientId' => $customerApplication->getIdApplication(),
            'telephone'         => '+33 ' . $phoneNumber,
            'birthPlace'        => $customerApplication->getCityBirth(),
            'gender'            => $gender,
            'honorificPrefix'   => $honorificPrefix,
            'birthDate'         => $customerApplication->getDateOfBirth()->format('Y-m-d'),
            'givenName'         => $customerApplication->getName(),
            'familyName'        => $customerApplication->getSurname(),
            'streetAddress'     => $customerApplication->getAdress(),
            'addressRegion'     => $customerApplication->getAdress2(),
            'postalCode'        => $customerApplication->getZipcode(),
            'addressCountry'    => 'FRA',
            'email'             => $customerApplication->getEmail(),
            'nationality'       => 'FRA',
        ];

        $context = '{
          "workflow": "volkswagen-fs-fr-nebula",
          "caseClientId": "' . $this->parameterBag->get('quicksign-client-id') . '",
          "contractTags": [
            {
              "tags": [
                "consent"
              ]
            }
          ],
          "applicants": [
            {
              "type": "applicant",
              "applicantClientId": "' . $user_info['applicantClientId'] . '",
              "telephone": "' . $user_info['telephone'] . '",
              "birthPlace": {
                "name": "' . $user_info['birthPlace'] . '"
              },
               "gender": "' . $user_info['gender'] . '",
              "honorificPrefix": "' . $user_info['honorificPrefix'] . '",
              "birthDate": "' . $user_info['birthDate'] . '",
              "givenName": "' . $user_info['givenName'] . '",
              "familyName": "' . $user_info['familyName'] . '",
                "address": {
                "streetAddress": "' . $user_info['streetAddress'] . '",
                "addressCountry": {
                  "identifier": "' . $user_info['addressCountry'] . '"
                },
                "addressRegion": "' . $user_info['addressRegion'] . '",
                "postalCode": "' . $user_info['postalCode'] . '"
              },
              "email": "' . $user_info['email'] . '",
              "nationality": {
                "identifier":"' . $user_info['nationality'] . '"
              }
            }
          ]
        }';

        $contract_path = realpath($quick_sign_contract);
        $pdfFile1      = new \CURLFile($contract_path, 'application/pdf', 'contrat.pdf');


        $ch = curl_init($url);


        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'context'      => $context,
            'contracts[0]' => $pdfFile1
        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);


        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Erreur cURL : ' . curl_error($ch);
        } else {
            $ret = json_decode($response);

            if (!$ret) {
                $message = 'Something went wrong. Please try again';

                return $this->render('default/kyc-error.html.twig', ['message' => $message, 'title' => 'KYC error']);
            } else if ( ! property_exists($ret, 'caseId') &&
                ! property_exists($ret, 'reconnectionUrl') &&
                ! property_exists($ret, 'smsReconnectionUrl') &&
                ! property_exists($ret, 'status') &&
                ! property_exists($ret, 'detail')) {
                $message = 'Something went wrong. Please try again';

                return $this->render('default/kyc-error.html.twig', ['message' => $message, 'title' => 'KYC error']);
            } else if (property_exists($ret, 'status') && property_exists($ret, 'detail')) {
                if ($ret->status == 400 && strpos($ret->detail, 'telephone')) {
                    $message = str_replace('applicants[0].telephone', '', $ret->detail);

                    return $this->render('default/kyc-error.html.twig', ['message' => $message, 'title' => 'KYC error']);
                } elseif ($ret->status == 400) {
                    $message = str_replace('applicants[0]', '', $ret->detail);

                    return $this->render('default/kyc-error.html.twig', ['message' => $message, 'title' => 'KYC error']);
                }
            }

            $this->webHookService->saveWebHookLogs($response, $request->get('application_id'), $ret->caseId);

            $identification_id            = $ret->caseId;
            $identification_url           = $ret->reconnectionUrl;
            $identification_url_reconnect = $ret->smsReconnectionUrl;

        }

        curl_close($ch);


        return  array(
            'identification_id'=> $identification_id,
            'identification_url'=>   $identification_url,
            'identification_url_reconnect'=>   $identification_url_reconnect
        );
    }



    /**
     * @Route("/financement-qr-code.html", name="financement-qr-code")
     */
    public function financementQrCodeAction(Request $request)
    {
        //Bypass kyc for test next step
        if ($request->get('bypass') == 'kyc') {
            return $this->redirectToRoute('successful-verification', ['customerApplication' => $request->get('application_id')]);
        }

        $reconection_url = $this->getKycUrls($request);

        $result = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data($reconection_url["identification_url_reconnect"])
            ->encoding(new Encoding('UTF-8'))
            ->size(200)
            ->margin(10)
            ->build();
        $qrcode_img = $result->getDataUri();

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $request->get('application_id')]);
        if (!$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $encrypted=$this->encryptionService->encryption($request->get('application_id'));
        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $request->get('application_id')]);

        return $this->render('default/financement-qr-code.html.twig', [
            'controller_name'              => 'default',
            'title'                        => 'Financement QR Code',
            'qrcode_img'                   => $qrcode_img,
            'application_id'               => $request->get('application_id'),
            'encryption_application_id'    => base64_encode($this->encryptionService->encryption($request->get('application_id'))),
            'escooterConfiguration'        => $escooterConfiguration,
            'customerApplication'          => $customerApplication
        ]);
    }

    /**
     * @Route("/get-kyc-status", name="financement-qr-code-status")
     */
    public function getKycStatusAction(Request $request) {

        $applicationId = $this->encryptionService->decryption(base64_decode($request->get('application_id')));
        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $applicationId]);

        if (!$customerApplication) {
            return new JsonResponse('ko', JsonResponse::HTTP_BAD_REQUEST);
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        $webhookLogsRepository = $this->container->get('doctrine')->getRepository(WebhookLogs::class);

        $queryBuilder = $webhookLogsRepository->createQueryBuilder('w');
        $query = $queryBuilder
            ->where('w.customerApplication = :customerApplication')
            ->andWhere('w.eventData LIKE :eventData')
            ->setParameter('customerApplication', $customerApplication->getIdApplication())
            ->setParameter('eventData', '%journeyEnded%')
            ->setMaxResults(1) // comme findOneBy
            ->getQuery();

        $webhookLog = $query->getOneOrNullResult();
        if ($webhookLog) {
            return new JsonResponse('ok');
        } else {
            return new JsonResponse('ko');
        }



        return $this->render('default/financement-qr-code.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Financement QR Code',
            'qrcode_img'            => $qrcode_img,
            'application_id'        => $request->get('application_id'),
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }


    /**
     * @Route("/etape-validee.html", name="successful-verification")
     */
    public function successfulVerification(Request $request): Response
    {
        if (!$request->get('customerApplication') && !$request->get('encryption_application_id')) {
            return $this->redirectToRoute('default');
        }

        if ($request->get('encryption_application_id')) {
            // Set applicationId when link are clicked from gmail

            $applicationId = $this->encryptionService->decryption(base64_decode($request->get('encryption_application_id')));
        } else {
            // Set applicationId when link are clicked from within step 2.1

            $applicationId = $request->get('customerApplication');
        }

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $applicationId]);
        if ( !$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        return $this->render('default/successful-verification.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Passez à l’étape suivante',
            'application_id'        => $applicationId,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/accord-de-principe.html", name="accord-de-principe")
     */
    public function accordDePrincipe(Request $request): Response
    {
        $customerApplicationId = $request->get('application_id');
        if ( ! $customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $customerApplication   = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);
        if ( !$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        return $this->render('default/financement-accord-de-principe.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Accord de principe',
            'application_id'        => $customerApplicationId,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/algoan-bank-interface.html", name="algoan-bank-interface")
     */
    public function algoanBankInterface(Request $request): Response
    {
        if (!$request->get('customerApplication') && !$request->get('encryption_application_id')) {
            return $this->redirectToRoute('default');
        }

        if ($request->get('encryption_application_id')) {
            $applicationId = $this->encryptionService->decryption($request->get('encryption_application_id'));
        } else {
            $applicationId = $request->get('customerApplication');
        }

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $applicationId]);

        if ( !$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        return $this->render('default/algoan-bank-interface.html.twig',[
            'controller_name'       => 'default',
            'title'                 => 'Bank Interface',
            'customerApplication'   => $customerApplication,
            'apiKey'                => $this->parameterBag->get('algoan-api-key-prod'),
            'isShowBackButton'      => $request->get('encryption_application_id') ? false : true,
            'escooterConfiguration' => $escooterConfiguration
        ]);

    }

    /**
     * @Route("/financement-echeance-rib.html", name="financement-echeance-rib")
     */
    public function maturityRIB(Request $request): Response
    {
        $doctrine = $this->container->get('doctrine');

        if ( ! $request->get('application_id') && ! $request->get('encryption_application_id')) {
            return $this->redirectToRoute('default');
        }

        if ($request->get('encryption_application_id')) {
            // Set applicationId when link are clicked from gmail

            $applicationId = $this->encryptionService->decryption($request->get('encryption_application_id'));
        } else {
            // Set applicationId when link are clicked from within step 3.1

            $applicationId = $request->get('application_id');
        }

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $applicationId]);
        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $applicationId]);

        $customerMedia       = $doctrine->getRepository(CustomersMedia::class)->findBy([
            'customersApplication' => $applicationId,
            'type'                 => 'bank'
        ]);

        $selectedDueDay = (int) $customerApplication->getDueDay();

        if ($selectedDueDay == 0) {
            $selectedDueDay = 1;
        }

        $allFiles = [];
        if ( ! empty($customerMedia)) {
            foreach ($customerMedia as $item) {
                $files['id']   = $item->getMedia()->getId();
                $files['name'] = $item->getMedia()->getName();
                $files['path'] = $item->getMedia()->getProviderReference();
                $allFiles[]    = $files;
            }
        }

        return $this->render('default/financement-echeance-rib.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Échéance & RIB*',
            'application_id'        => $applicationId,
            'files'                 => $allFiles,
            'selectedDueDay'        => $selectedDueDay,
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/votre-solvabilite-document.html", name="votre-solvabilite-document")
     */
    public function votreSolvabiliteDocument(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        if ( ! $request->get('application_id')) {
            return $this->redirectToRoute('default');
        }

        $bypass = false;
        $message = '';
        //Bypass bankpush for test next step
        if ($request->get('bypass') == 'bankpush') {
            $bypass = true;
            $message = 'success';
        }

        $images                = $request->files->get('files', []);
        $entityManager         = $this->container->get('doctrine')->getManager();
        $customerApplicationId = $request->get('application_id');
        $customerApplication   = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);
        if ( ! $customerApplication) {
            return $this->redirectToRoute('default');
        }
        $customerApplication->setDueDay($request->get('due-day') == 1 ? $request->get('due-day') . 'er du mois' : $request->get('due-day') . ' du mois');
        $doctrine->getManager()->persist($customerApplication);
        $doctrine->getManager()->flush($customerApplication);

        if ( ! empty($images)) {
            // validate file size
            $fileSize = 0;
            foreach ($images as $image) {
                $fileSize += $image->getSize();
            }
            // max file size = 100 MB
            if ($fileSize > (100 * 1024 * 1024)) {
                return $this->redirectToRoute('financement-echeance-rib',
                    ['message' => 'La taille du fichier est trop grande. Veuillez vous assurer que la taille est inférieure à 100 Mo'],
                    307);
            } else {
                foreach ($images as $image) {
                    $media = new Media();
                    $media->setContext('default');
                    $media->setProviderName('sonata.media.provider.image');
                    $media->setBinaryContent($image);
                    $this->mediaManager->save($media);

                    $customerMedia = new CustomersMedia();
                    $customerMedia->setCustomersApplication($customerApplication);
                    $customerMedia->setMedia($media);
                    $customerMedia->setType('bank');
                    $entityManager->persist($customerMedia);
                    $entityManager->flush($customerMedia);
                }
            }
        }

        return new JsonResponse(['application_id' => $request->get('application_id'), 'isBypassBankpush' => $bypass, 'message' => $message]);
    }

    /**
     * @Route("/votre-solvabilite.html", name="votre-solvabilite")
     */
    public function votreSolvabilite(Request $request)
    {

        $customerApplicationId = $request->get('customerApplication');
        $entityManager         = $this->container->get('doctrine')->getManager();

        if ( ! $customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);

        if ( !$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);
        $customersAlgoan = $this->container->get('doctrine')->getRepository(CustomersAlgoan::class)->findOneBy(['idApplication' => $customerApplicationId]);

        if (!$customersAlgoan) {
            $customerAlgoan = new CustomersAlgoan();
            $customerAlgoan->setIdApplication($customerApplication);
            $customerAlgoan->setCivility($customerApplication->getCivility());
            $customerAlgoan->setEmail($customerApplication->getEmail());
            $customerAlgoan->setName($customerApplication->getName());
            $customerAlgoan->setSurname($customerApplication->getSurname());
            $customerAlgoan->setPhone($customerApplication->getPhone());
            $customerAlgoan->setPostalCode($customerApplication->getZipcode());
            $customerAlgoan->setDateBankaccountOpened(new \DateTime());
            $customerAlgoan->setCreatedAt(new \DateTime());
            $customerAlgoan->setUpdatedAt(new \DateTime());

            if ($request->get('message') !== 'cancelled') {
                $customerAlgoan->setAlgoanExtnalId($request->get('analysisId'));
            }

            if ($request->get('message') === 'success') {
                //save data response to db
                $customerId = $request->get('customerId');
                $analysisId = $request->get('analysisId');

                $analysis = $this->algoanService->getAnalysis($customerId, $analysisId);

                if (empty($analysis)) {
                    $dataAnalysis = [
                        'activityScore' => '',
                        'creditScore' => '',
                        'error' => ''
                    ];

                    $customerAlgoan->setAnalysis($dataAnalysis);
                    $customerAlgoan->setCustomerId($request->get('customerId'));
                    $customerAlgoan->setAlgoanExtnalId($request->get('analysisId'));

                    $entityManager->persist($customerAlgoan);
                    $entityManager->flush();
                    
                    return $this->render('default/votre-solvabilite.html.twig', [
                        'controller_name' => 'default',
                        'title'           => 'Votre solvabilité',
                        'application_id'  => $request->get('customerApplication'),
                        'message'         => $request->get('message')
                    ]);
                }
                
                $activityScoreValue = $analysis['metadata']['activityScore']['value'] ?? '';
                $creditScoreValue = $analysis['scores']['creditScore']['value'] ?? '';
                $error = '';
                if (array_key_exists('code', $analysis) && $analysis['code'] == 'UNKNOWN_ENTITY' && $analysis['status'] == '404') {
                    $error = $analysis['message'];
                }

                $dataAnalysis = ['activityScore' => $activityScoreValue, 'creditScore' => $creditScoreValue, 'error' => $error];

                $customerAlgoan->setAnalysis($dataAnalysis);

                $customerAlgoan->setCustomerId($request->get('customerId'));
                $customerAlgoan->setAlgoanExtnalId($request->get('analysisId'));

                $customerApplication->setIsBankPushStatusAggregationSuccess(true);
                $entityManager->persist($customerApplication);
                $entityManager->persist($customerAlgoan);
                $entityManager->flush();

                if ($customerApplication->getIsKycStatusFebOK() && $customerApplication->getKycSessionId() && $customerAlgoan->getAnalysis() && $customerAlgoan->getAnalysis()['activityScore'] !== '') {
                    $this->bankPushService->summarizeUserJourney($customerApplication, $customerApplication->getKycSessionId());
                }
            }

            $entityManager->persist($customerAlgoan);
            $entityManager->flush();
        }

        return $this->render('default/votre-solvabilite.html.twig', [
            'controller_name'       => 'default',
            'title'                 => 'Votre solvabilité',
            'application_id'        => $request->get('customerApplication'),
            'message'               => $request->get('message'),
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/recapitulatif-de-votre-dossier.html", name="summary-of-your-file")
     */
    public function summaryOfYourFile(Request $request): Response
    {
        $customerApplicationId = $request->get('application_id');
        if ( ! $customerApplicationId) {
            return $this->redirectToRoute('default');
        }

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);
        if ( !$customerApplication) {
            return $this->redirectToRoute('default');
        }

        $escooterConfiguration = $this->container->get('doctrine')->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplicationId]);

        $vehicleModel        = $escooterConfiguration->getVehicleModel();
        $vehicleOptions      = $escooterConfiguration->getVehicleOptions();
        $vehicleOptionsPrice = $escooterConfiguration->getVehicleOptionsPrice();

        return $this->render('default/financement-recapitulatif-de-votre-dossier.html.twig', [
            'controller_name'     => 'default',
            'title'               => 'Récapitulatif de votre demande',
            'application_id'      => $customerApplicationId,
            'vehicleModel'        => $vehicleModel,
            'vehicleOptions'      => $vehicleOptions,
            'vehicleOptionsPrice' => $vehicleOptionsPrice,
            'distributeur'        => $escooterConfiguration->getIdApplication()->getDistributeurs(),
            'escooterConfiguration' => $escooterConfiguration
        ]);
    }

    /**
     * @Route("/download-cgu-pdf", name="download_cgu_pdf")
     */
    public function downloadCGUPdf(): Response
    {
        $pdfFilePath = $this->getParameter('kernel.project_dir') . '/public/cgu/CGU_SEAT_MO_OPEN_BANKING_V2.pdf';

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename="CGU_SEAT_MO_OPEN_BANKING_V2.pdf"');
        $response->setContent(file_get_contents($pdfFilePath));

        return $response;
    }

    /**
     * @Route("/test-email.html", name="test-email")
     */
    public function testEmail(Request $request): Response
    {

        $filePath=$this->parameterBag->get('kernel.project_dir') . '/public/bank-push/tarifeur.xlsx';
        $fileContent = base64_encode(file_get_contents($filePath));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.mailjet.com/v3.1/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, "5be6cff9787c8ac4496144aea181019d:775a11a8661c304410bb3f7082ded76d");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));


        $data = array(
            "Messages" => array(
                array(
                    "From" => array(
                        "Email" => "no-reply@seatfrance.com",
                        "Name" => "no-reply@seatfrance.com"
                    ),
                    "To" => array(
                        array(
                            "Email" => "tbourdin@partitech.com",
                            "Name" => "tbourdin"
                        )
                    ),
                    "TemplateID" => 5312495,
                    "TemplateLanguage" => true,
                    "Subject" => "SEAT FINANCEMENT",
                    "CustomCampaign" => "SEAT Financement - Transactionnel",
                    "Variables" => array(
                        "firstname" => "Bourdin",
                        "lastname" => "thomas",
                        /* "url" => "google.com"*/
                    ),
                    "Attachments" => array(
                        array(
                            "ContentType" => "text/plain",
                            "Filename" => basename($filePath),
                            "Base64Content" => $fileContent
                        )
                    )
                )
            )
        );

        /* $data = array(
            "Messages" => array(
                array(
                    "From" => array(
                        "Email" => "no-reply@seatfrance.com",
                        "Name" => "no-reply@seatfrance.com"
                    ),
                    "To" => array(
                        array(
                            "Email" => "tbourdin@partitech.com",
                            "Name" => "test thom"
                        )
                    ),
                    "TemplateID" => 5312495,
                    "TemplateLanguage" => true,
                    "Subject" => "SEAT FINANCEMENT",
                    "Variables" => array(
                        "firstname" => "Julien",
                        "lastname" => "D!",
                        "url" => "google.com"
                    ),
                    "Attachments" => array(
                        array(
                            "ContentType" => "text/plain",
                            "Filename" => basename($filePath),
                            "Base64Content" => $fileContent
                        )
                    )
                )
            )
        );*/
        $jsonData = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur cURL : ' . curl_error($ch);
        }

        curl_close($ch);
        dd($response);

        return false;

    }

    /**
     * @Route("/test-final-email.html", name="test-final-email")
     */
    public function testFinalEmail(Request $request): Response
    {
        //http://e-scooter.localhost/test-final-email.html?id_application=754&sessionid=416c7954-1965-4f17-898c-fc79c316d3ee

        $id_application = $request->get('id_application');
        $session_id = $request->get('sessionid');

        $customerApplication = $this->container->get('doctrine')->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $id_application]);
        $zipFilePath = $this->bankPushService->summarizeUserJourney($customerApplication, $session_id);
        if($zipFilePath){
            $x=explode('public', $zipFilePath);

            header('Location: '.$x[1]);
            exit();
        }

    }
}
