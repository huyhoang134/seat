<?php 
// src/Form/Type/ColorSelectorType.php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormView;

class NumberSuffixType extends AbstractType
{
    public function getParent()
    {
        return NumberType::class;
        //return TextType::class;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
    
        $resolver->setDefaults([
            'suffix' => 'suffix',
            'field_options' => [],
        ]);
    
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['field_name']=$form->getName();
        $view->vars['data']=$form->getData();
        $view->vars['suffix']=$options['suffix'];
        $view->vars['type'] = 'number';
        $view->vars['attr']['class'] = 'number_suffix';
    
    }
    
    public function getBlockPrefix()
    {
        return 'number_suffix';
    }
    
    
}

?>