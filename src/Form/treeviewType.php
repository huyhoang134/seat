<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
#use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class treeviewType extends AbstractType
{
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        
        $resolver->setDefaults([
            'field_type' => CheckboxType::class,
            'field_options' => [],
        ]);
        
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


       $builder->add('modeles', CheckboxType::class, ['required'=>false, 'label'=>'modeles']);
       $builder->add('modeles_finitions', CheckboxType::class, ['required'=>false, 'label'=>'_______finitions']);
       $builder->add('modeles_finitions_fsc', CheckboxType::class, ['required'=>false, 'label'=>'_______finitions___FSC']);
       $builder->add('modeles_accessoires', CheckboxType::class, ['required'=>false, 'label'=>'_______accessoires']);
       $builder->add('modeles_entretiens', CheckboxType::class, ['required'=>false, 'label'=>'_______Entretiens']);
       $builder->add('mentions_legales', CheckboxType::class, ['required'=>false, 'label'=>'_______Mentions legales']);
       $builder->add('accessoires', CheckboxType::class, ['required'=>false, 'label'=>'Référenciel accessoires']);
       $builder->add('baremes_hway', CheckboxType::class, ['required'=>false, 'label'=>'Hway']);

    }
    
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['field_name']=$form->getName();
        $view->vars['data']=$form->getData();

    }
    
    public function getBlockPrefix()
    {
        return 'treeview';
    }

}
