<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersKyc
 *
 * @ORM\Table(name="Customers_KYC", indexes={@ORM\Index(name="fk_Customers_KYC_Customers_application1_idx", columns={"id_application"})})
 * @ORM\Entity
 */
class CustomersKyc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ubble_identification_id", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $ubbleIdentificationId = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="identification_url", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $identificationUrl = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="kyc_state", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $kycState = 'NULL';

    /**
     * @var \CustomersApplication
     *
     * @ORM\ManyToOne(targetEntity="CustomersApplication")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_application", referencedColumnName="id_application")
     * })
     */
    private $idApplication;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUbbleIdentificationId(): ?int
    {
        return $this->ubbleIdentificationId;
    }

    public function setUbbleIdentificationId(?int $ubbleIdentificationId): self
    {
        $this->ubbleIdentificationId = $ubbleIdentificationId;

        return $this;
    }

    public function getIdentificationUrl(): ?string
    {
        return $this->identificationUrl;
    }

    public function setIdentificationUrl(?string $identificationUrl): self
    {
        $this->identificationUrl = $identificationUrl;

        return $this;
    }

    public function getKycState(): ?bool
    {
        return $this->kycState;
    }

    public function setKycState(?bool $kycState): self
    {
        $this->kycState = $kycState;

        return $this;
    }

    public function getIdApplication(): ?CustomersApplication
    {
        return $this->idApplication;
    }

    public function setIdApplication(?CustomersApplication $idApplication): self
    {
        $this->idApplication = $idApplication;

        return $this;
    }


}
