<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Distributeurs
 *
 * @ORM\Table(name="distributeurs")
 * @ORM\Entity(repositoryClass="App\Repository\DistributeursRepository")
 */
class Distributeurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kvps", type="string", length=255, nullable=true)
     */
    private $kvps;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kvps_de", type="string", length=255, nullable=true)
     */
    private $kvpsDe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_ce", type="string", length=255, nullable=true)
     */
    private $codeCe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="district", type="string", length=255, nullable=true)
     */
    private $district;

    /**
     * @var int|null
     *
     * @ORM\Column(name="occasion", type="integer", nullable=true)
     */
    private $occasion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="distrib_type", type="integer", nullable=true)
     */
    private $distribType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_center", type="string", length=255, nullable=true)
     */
    private $serviceCenter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale", type="string", length=255, nullable=true)
     */
    private $raisonSociale;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_postal", type="string", length=255, nullable=true)
     */
    private $codePostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone_abbr", type="string", length=255, nullable=true)
     */
    private $telephoneAbbr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fax_abbr", type="string", length=255, nullable=true)
     */
    private $faxAbbr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string|null
     *
     * @ORM\Column(name="website_seat", type="string", length=255, nullable=true)
     */
    private $websiteSeat;

    /**
     * @var int|null
     *
     * @ORM\Column(name="seat_access", type="integer", nullable=true)
     */
    private $seatAccess;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cupra_access", type="integer", nullable=true)
     */
    private $cupraAccess;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $escooter = '0';
    
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $testDrive = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean",options={"default":0})
     */
    private $isReservationAvailable = '0';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dealerName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dealerCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dealerEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dealerPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dealerResp;
    
    public function __toString()
    {
        return $this->getRaisonSociale();
    }
    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKvps(): ?string
    {
        return $this->kvps;
    }

    public function setKvps(?string $kvps): self
    {
        $this->kvps = $kvps;

        return $this;
    }

    public function getKvpsDe(): ?string
    {
        return $this->kvpsDe;
    }

    public function setKvpsDe(?string $kvpsDe): self
    {
        $this->kvpsDe = $kvpsDe;

        return $this;
    }

    public function getCodeCe(): ?string
    {
        return $this->codeCe;
    }

    public function setCodeCe(?string $codeCe): self
    {
        $this->codeCe = $codeCe;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getOccasion(): ?int
    {
        return $this->occasion;
    }

    public function setOccasion(?int $occasion): self
    {
        $this->occasion = $occasion;

        return $this;
    }

    public function getDistribType(): ?int
    {
        return $this->distribType;
    }

    public function setDistribType(?int $distribType): self
    {
        $this->distribType = $distribType;

        return $this;
    }

    public function getServiceCenter(): ?string
    {
        return $this->serviceCenter;
    }

    public function setServiceCenter(?string $serviceCenter): self
    {
        $this->serviceCenter = $serviceCenter;

        return $this;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raisonSociale;
    }

    public function setRaisonSociale(?string $raisonSociale): self
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getTelephoneAbbr(): ?string
    {
        return $this->telephoneAbbr;
    }

    public function setTelephoneAbbr(?string $telephoneAbbr): self
    {
        $this->telephoneAbbr = $telephoneAbbr;

        return $this;
    }

    public function getFaxAbbr(): ?string
    {
        return $this->faxAbbr;
    }

    public function setFaxAbbr(?string $faxAbbr): self
    {
        $this->faxAbbr = $faxAbbr;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getWebsiteSeat(): ?string
    {
        return $this->websiteSeat;
    }

    public function setWebsiteSeat(?string $websiteSeat): self
    {
        $this->websiteSeat = $websiteSeat;

        return $this;
    }

    public function getSeatAccess(): ?int
    {
        return $this->seatAccess;
    }

    public function setSeatAccess(?int $seatAccess): self
    {
        $this->seatAccess = $seatAccess;

        return $this;
    }

    public function getCupraAccess(): ?int
    {
        return $this->cupraAccess;
    }

    public function setCupraAccess(?int $cupraAccess): self
    {
        $this->cupraAccess = $cupraAccess;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }
    public function getEscooter(): ?bool
    {
        return $this->escooter;
    }
    
    public function setEscooter(bool $escooter): self
    {
        $this->escooter = $escooter;
    
        return $this;
    }
    
    public function getTestDrive(): ?bool
    {
        return $this->testDrive;
    }
    
    public function setTestDrive(bool $testDrive): self
    {
        $this->testDrive = $testDrive;
    
        return $this;
    }

    public function getIsReservationAvailable(): ?bool
    {
        return $this->isReservationAvailable;
    }
    
    public function setIsReservationAvailable(bool $isReservationAvailable): self
    {
        $this->isReservationAvailable = $isReservationAvailable;
    
        return $this;
    }

    public function getDealerName(): ?string
    {
        return $this->dealerName;
    }

    public function setDealerName(?string $dealerName): self
    {
        $this->dealerName = $dealerName;

        return $this;
    }

    public function getDealerCode(): ?string
    {
        return $this->dealerCode;
    }

    public function setDealerCode(?string $dealerCode): self
    {
        $this->dealerCode = $dealerCode;

        return $this;
    }

    public function getDealerEmail(): ?string
    {
        return $this->dealerEmail;
    }

    public function setDealerEmail(?string $dealerEmail): self
    {
        $this->dealerEmail = $dealerEmail;

        return $this;
    }

    public function getDealerPhone(): ?string
    {
        return $this->dealerPhone;
    }

    public function setDealerPhone(?string $dealerPhone): self
    {
        $this->dealerPhone = $dealerPhone;

        return $this;
    }

    public function getDealerResp(): ?string
    {
        return $this->dealerResp;
    }

    public function setDealerResp(?string $dealerResp): self
    {
        $this->dealerResp = $dealerResp;

        return $this;
    }
}
