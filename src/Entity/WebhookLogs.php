<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Json;

/**
 * @ORM\Table(name="Webhook_logs")
 * @ORM\Entity(repositoryClass="App\Repository\WebHookLogsRepository")
 */
class WebhookLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_application", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $customerApplication;

    /**
     * @var string|null
     *
     * @ORM\Column(name="event_type", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $eventType;

    /**
     * @var json|null
     *
     * @ORM\Column(name="event_data", type="text", nullable=true, options={"default"="NULL"})
     */
    private $eventData;

    /**
     * @var string|null
     *
     * @ORM\Column(name="time_stamp", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $timeStamp;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caseId;

    public function getEventType(): ?string
    {
        return $this->eventType;
    }

    public function setEventType(?string $eventType): self
    {
        $this->eventType = $eventType;

        return $this;
    }

    public function getEventData(): ?string
    {
        return $this->eventData;
    }

    public function setEventData($eventData): self
    {

        $this->eventData = $eventData;

        return $this;
    }

    public function getTimeStamp(): ?string
    {
        return $this->timeStamp;
    }

    public function setTimeStamp(?string $timeStamp): self
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerApplication(): ?string
    {
        return $this->customerApplication;
    }

    public function setCustomerApplication(?string $customerApplication): self
    {
        $this->customerApplication = $customerApplication;

        return $this;
    }

    public function getCaseId(): ?string
    {
        return $this->caseId;
    }

    public function setCaseId(?string $caseId): self
    {
        $this->caseId = $caseId;

        return $this;
    }
}