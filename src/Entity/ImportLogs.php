<?php

namespace App\Entity;
use Sonata\MediaBundle\Model\MediaInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * ImportLogs
 *
 * @ORM\Table(name="import_logs")
 * @ORM\Entity
 */
class ImportLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="autheur", type="string", length=255, nullable=true)
     */
    private $autheur;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fichier", referencedColumnName="id")
     * })
     */
    private $fichier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="logs", type="text", length=65535, nullable=true)
     */
    private $logs;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getType(): ?string
    {
        return $this->type;
    }
    
    public function setType(?string $type): self
    {
        $this->type = $type;
    
        return $this;
    }
    
    public function getAutheur(): ?string
    {
        return $this->autheur;
    }
    
    public function setAutheur(?string $autheur): self
    {
        $this->autheur = $autheur;
    
        return $this;
    }
    
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }
    
    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;
    
        return $this;
    }
    
    public function getFichier(): ?\App\Application\Sonata\MediaBundle\Entity\Media
    {
        return $this->fichier;
    }
    
    public function setFichier(?\App\Application\Sonata\MediaBundle\Entity\Media $fichier): self
    {
        $this->fichier = $fichier;
    
        return $this;
    }
    
    public function getLogs(): ?string
    {
        return $this->logs;
    }
    
    public function setLogs(?string $logs): self
    {
        $this->logs = $logs;
    
        return $this;
    }

}
