<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersApplication
 *
 * @ORM\Table(name="Customers_application", indexes={@ORM\Index(name="fk_Customers_application_distributeurs1_idx", columns={"distributeurs_id"})})
 * @ORM\Entity
 */
class CustomersApplication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_application", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idApplication;

    /**
     * @var string|null
     *
     * @ORM\Column(name="civility", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $civility = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $name = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $surname = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="maiden_name", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $maidenName = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_of_birth", type="date", nullable=true, options={"default"="NULL"})
     */
    private $dateOfBirth = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city_birth", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $cityBirth = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="zipcode_birth", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $zipcodeBirth;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adress", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $adress = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="adress_2", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $adress2 = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $city = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="zipcode", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $zipcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nationality", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $nationality = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $phone = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $email = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="main_residence", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $mainResidence = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="family_situtation", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $familySitutation = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="childs", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $childs = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="occupation", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $occupation = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="hiring_date", type="date", nullable=true, options={"default"="NULL"})
     */
    private $hiringDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contract_type", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $contractType = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="Licence_optin", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $licenceOptin;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="Commercial_optin", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $commercialOptin;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="application_state", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $applicationState;

    /**
     * @var Distributeurs
     *
     * @ORM\ManyToOne(targetEntity="Distributeurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distributeurs_id", referencedColumnName="id")
     * })
     */
    private $distributeurs;

    /**
     * @ORM\OneToOne(targetEntity=CustomersMedia::class, mappedBy="customersApplication", cascade={"persist", "remove"})
     */
    private $customersMedia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $country = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="due_day", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $dueDay = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="dealer", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $dealer = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="employeur", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $employeur = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_kyc_status_febok", type="boolean", nullable=true)
     */
    private $isKycStatusFebOK;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_bank_push_status_aggregation_success", type="boolean", nullable=true)
     */
    private $isBankPushStatusAggregationSuccess;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kyc_session_id", type="string", length=255, nullable=true)
     */
    private $kycSessionId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $revenuNetMensuel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $autresRevenus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $chargesImmobilieres;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $autresRemboursementsMensuels;

    public function getIdApplication(): ?int
    {
        return $this->idApplication;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getMaidenName(): ?string
    {
        return $this->maidenName;
    }

    public function setMaidenName(?string $maidenName): self
    {
        $this->maidenName = $maidenName;

        return $this;
    }
    
    public function getFullName(): string
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getCityBirth(): ?string
    {
        return $this->cityBirth;
    }

    public function setCityBirth(?string $cityBirth): self
    {
        $this->cityBirth = $cityBirth;

        return $this;
    }

    public function getZipcodeBirth(): ?int
    {
        return $this->zipcodeBirth;
    }

    public function setZipcodeBirth(?int $zipcodeBirth): self
    {
        $this->zipcodeBirth = $zipcodeBirth;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getAdress2(): ?string
    {
        return $this->adress2;
    }

    public function setAdress2(?string $adress2): self
    {
        $this->adress2 = $adress2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipcode(): ?int
    {
        return $this->zipcode;
    }

    public function setZipcode(?int $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMainResidence(): ?string
    {
        return $this->mainResidence;
    }

    public function setMainResidence(?string $mainResidence): self
    {
        $this->mainResidence = $mainResidence;

        return $this;
    }

    public function getFamilySitutation(): ?string
    {
        return $this->familySitutation;
    }

    public function setFamilySitutation(?string $familySitutation): self
    {
        $this->familySitutation = $familySitutation;

        return $this;
    }

    public function getChilds(): ?string
    {
        return $this->childs;
    }

    public function setChilds(?string $childs): self
    {
        $this->childs = $childs;

        return $this;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function setOccupation(?string $occupation): self
    {
        $this->occupation = $occupation;

        return $this;
    }

    public function getHiringDate(): ?\DateTimeInterface
    {
        return $this->hiringDate;
    }

    public function setHiringDate(?\DateTimeInterface $hiringDate): self
    {
        $this->hiringDate = $hiringDate;

        return $this;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function setContractType(?string $contractType): self
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getLicenceOptin(): ?bool
    {
        return $this->licenceOptin;
    }

    public function setLicenceOptin(?bool $licenceOptin): self
    {
        $this->licenceOptin = $licenceOptin;

        return $this;
    }

    public function getCommercialOptin(): ?bool
    {
        return $this->commercialOptin;
    }

    public function setCommercialOptin(?bool $commercialOptin): self
    {
        $this->commercialOptin = $commercialOptin;

        return $this;
    }

    public function getApplicationState(): ?bool
    {
        return $this->applicationState;
    }

    public function setApplicationState(?bool $applicationState): self
    {
        $this->applicationState = $applicationState;

        return $this;
    }

    public function getDistributeurs(): ?Distributeurs
    {
        return $this->distributeurs;
    }

    public function setDistributeurs(?Distributeurs $distributeurs): self
    {
        $this->distributeurs = $distributeurs;

        return $this;
    }

    public function getCustomersMedia(): ?CustomersMedia
    {
        return $this->customersMedia;
    }

    public function setCustomersMedia(?CustomersMedia $customersMedia): self
    {
        // unset the owning side of the relation if necessary
        if ($customersMedia === null && $this->customersMedia !== null) {
            $this->customersMedia->setCustomersApplication(null);
        }

        // set the owning side of the relation if necessary
        if ($customersMedia !== null && $customersMedia->getCustomersApplication() !== $this) {
            $customersMedia->setCustomersApplication($this);
        }

        $this->customersMedia = $customersMedia;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDueDay(): ?string
    {
        return $this->dueDay;
    }

    public function setDueDay(?string $dueDay): self
    {
        $this->dueDay = $dueDay;

        return $this;
    }

    public function getDealer(): ?string
    {
        return $this->dealer;
    }

    public function setDealer(?string $dealer): self
    {
        $this->dealer = $dealer;

        return $this;
    }

    public function getEmployeur(): ?string
    {
        return $this->employeur;
    }

    public function setEmployeur(?string $employeur): self
    {
        $this->employeur = $employeur;

        return $this;
    }

    public function getIsKycStatusFebOK(): ?bool
    {
        return $this->isKycStatusFebOK;
    }

    public function setIsKycStatusFebOK(?bool $isKycStatusFebOK): self
    {
        $this->isKycStatusFebOK = $isKycStatusFebOK;

        return $this;
    }

    public function getIsBankPushStatusAggregationSuccess(): ?bool
    {
        return $this->isBankPushStatusAggregationSuccess;
    }

    public function setIsBankPushStatusAggregationSuccess(?bool $isBankPushStatusAggregationSuccess): self
    {
        $this->isBankPushStatusAggregationSuccess = $isBankPushStatusAggregationSuccess;

        return $this;
    }

    public function getKycSessionId(): ?string
    {
        return $this->kycSessionId;
    }

    public function setKycSessionId(?string $kycSessionId): self
    {
        $this->kycSessionId = $kycSessionId;

        return $this;
    }

    public function getRevenuNetMensuel(): ?int
    {
        return $this->revenuNetMensuel;
    }

    public function setRevenuNetMensuel(?int $revenuNetMensuel): self
    {
        $this->revenuNetMensuel = $revenuNetMensuel;

        return $this;
    }

    public function getAutresRevenus(): ?int
    {
        return $this->autresRevenus;
    }

    public function setAutresRevenus(?int $autresRevenus): self
    {
        $this->autresRevenus = $autresRevenus;

        return $this;
    }

    public function getChargesImmobilieres(): ?int
    {
        return $this->chargesImmobilieres;
    }

    public function setChargesImmobilieres(?int $chargesImmobilieres): self
    {
        $this->chargesImmobilieres = $chargesImmobilieres;

        return $this;
    }

    public function getAutresRemboursementsMensuels(): ?int
    {
        return $this->autresRemboursementsMensuels;
    }

    public function setAutresRemboursementsMensuels(?int $autresRemboursementsMensuels): self
    {
        $this->autresRemboursementsMensuels = $autresRemboursementsMensuels;

        return $this;
    }
}
