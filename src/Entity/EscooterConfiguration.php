<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EscooterConfiguration
 *
 * @ORM\Table(name="Escooter_configuration", indexes={@ORM\Index(name="fk_Escooter_configuration_Customers_application_idx", columns={"id_application"})})
 * @ORM\Entity
 */
class EscooterConfiguration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_brand", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $vehicleBrand = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_engine", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $vehicleEngine = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_model", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $vehicleModel = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_km", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $vehicleKm;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_km_annual", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $vehicleKmAnnual;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_km_total", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $vehicleKmTotal ;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_km_price", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $vehicleKmPrice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vehicle_options", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $vehicleOptions = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="vehicle_options_price", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $vehicleOptionsPrice;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="optin_insurance_GCM", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $optinInsuranceGcm;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="optin_insurance_VDR", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $optinInsuranceVdr;

    /**
     * @var int|null
     *
     * @ORM\Column(name="loa_price", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $loaPrice;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $price;

    /**
     * @var CustomersApplication
     *
     * @ORM\ManyToOne(targetEntity="CustomersApplication")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_application", referencedColumnName="id_application")
     * })
     */
    private $idApplication;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="assurance", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $assurance;

    /**
     * @ORM\Column(name="loa_duree", type="string", length=255, nullable=true)
     */
    private $loaDuree;

    /**
     * @ORM\Column(name="loa_loyers", type="string", length=255, nullable=true)
     */
    private $loaLoyers;

    /**
     * @ORM\Column(name="prix_apartir_de", type="string", length=255, nullable=true)
     */
    private $prixAPartirDe;

    /**
     * @ORM\Column(name="montant_total_du", type="string", length=255, nullable=true)
     */
    private $montantTotalDu;

    /**
     * @ORM\Column(name="option_achat", type="string", length=255, nullable=true)
     */
    private $optionAchat;

    /**
     * @ORM\Column(name="loyer_majore", type="string", length=255, nullable=true)
     */
    private $loyerMajore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prixCatalogue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color_name", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $colorName = 'NULL';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVehicleBrand(): ?string
    {
        return $this->vehicleBrand;
    }

    public function setVehicleBrand(?string $vehicleBrand): self
    {
        $this->vehicleBrand = $vehicleBrand;

        return $this;
    }

    public function getVehicleEngine(): ?string
    {
        return $this->vehicleEngine;
    }

    public function setVehicleEngine(?string $vehicleEngine): self
    {
        $this->vehicleEngine = $vehicleEngine;

        return $this;
    }

    public function getVehicleModel(): ?string
    {
        return $this->vehicleModel;
    }

    public function setVehicleModel(?string $vehicleModel): self
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }

    public function getVehicleKm(): ?int
    {
        return $this->vehicleKm;
    }

    public function setVehicleKm(?int $vehicleKm): self
    {
        $this->vehicleKm = $vehicleKm;

        return $this;
    }

    public function getVehicleKmAnnual(): ?int
    {
        return $this->vehicleKmAnnual;
    }

    public function setVehicleKmAnnual(?int $vehicleKmAnnual): self
    {
        $this->vehicleKmAnnual = $vehicleKmAnnual;

        return $this;
    }

    public function getVehicleKmTotal(): ?int
    {
        return $this->vehicleKmTotal;
    }

    public function setVehicleKmTotal(?int $vehicleKmTotal): self
    {
        $this->vehicleKmTotal = $vehicleKmTotal;

        return $this;
    }

    public function getVehicleKmPrice(): ?int
    {
        return $this->vehicleKmPrice;
    }

    public function setVehicleKmPrice(?int $vehicleKmPrice): self
    {
        $this->vehicleKmPrice = $vehicleKmPrice;

        return $this;
    }

    public function getVehicleOptions(): ?string
    {
        return $this->vehicleOptions;
    }

    public function setVehicleOptions(?string $vehicleOptions): self
    {
        $this->vehicleOptions = $vehicleOptions;

        return $this;
    }

    public function getVehicleOptionsPrice(): ?int
    {
        return $this->vehicleOptionsPrice;
    }

    public function setVehicleOptionsPrice(?int $vehicleOptionsPrice): self
    {
        $this->vehicleOptionsPrice = $vehicleOptionsPrice;

        return $this;
    }

    public function getOptinInsuranceGcm(): ?bool
    {
        return $this->optinInsuranceGcm;
    }

    public function setOptinInsuranceGcm(?bool $optinInsuranceGcm): self
    {
        $this->optinInsuranceGcm = $optinInsuranceGcm;

        return $this;
    }

    public function getOptinInsuranceVdr(): ?bool
    {
        return $this->optinInsuranceVdr;
    }

    public function setOptinInsuranceVdr(?bool $optinInsuranceVdr): self
    {
        $this->optinInsuranceVdr = $optinInsuranceVdr;

        return $this;
    }

    public function getLoaPrice(): ?int
    {
        return $this->loaPrice;
    }

    public function setLoaPrice(?int $loaPrice): self
    {
        $this->loaPrice = $loaPrice;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIdApplication(): ?CustomersApplication
    {
        return $this->idApplication;
    }

    public function setIdApplication(?CustomersApplication $idApplication): self
    {
        $this->idApplication = $idApplication;

        return $this;
    }

    public function getCustomersApplication(): ?CustomersApplication
    {
        return $this->customersApplication;
    }

    public function setCustomersApplication(?CustomersApplication $customersApplication): self
    {
        $this->customersApplication = $customersApplication;

        return $this;
    }

    public function getAssurance(): ?bool
    {
        return $this->assurance;
    }

    public function setAssurance(?bool $assurance): self
    {
        $this->assurance = $assurance;

        return $this;
    }

    public function getLoaDuree(): ?string
    {
        return $this->loaDuree;
    }

    public function setLoaDuree(?string $loaDuree): self
    {
        $this->loaDuree = $loaDuree;

        return $this;
    }

    public function getLoaLoyers(): ?string
    {
        return $this->loaLoyers;
    }

    public function setLoaLoyers(?string $loaLoyers): self
    {
        $this->loaLoyers = $loaLoyers;

        return $this;
    }

    public function getPrixAPartirDe(): ?string
    {
        return $this->prixAPartirDe;
    }

    public function setPrixAPartirDe(?string $prixAPartirDe): self
    {
        $this->prixAPartirDe = $prixAPartirDe;

        return $this;
    }

    public function getMontantTotalDu(): ?string
    {
        return $this->montantTotalDu;
    }

    public function setMontantTotalDu(?string $montantTotalDu): self
    {
        $this->montantTotalDu = $montantTotalDu;

        return $this;
    }

    public function getOptionAchat(): ?string
    {
        return $this->optionAchat;
    }

    public function setOptionAchat(?string $optionAchat): self
    {
        $this->optionAchat = $optionAchat;

        return $this;
    }

    public function getLoyerMajore(): ?string
    {
        return $this->loyerMajore;
    }

    public function setLoyerMajore(?string $loyerMajore): self
    {
        $this->loyerMajore = $loyerMajore;

        return $this;
    }

    public function getPrixCatalogue(): ?string
    {
        return $this->prixCatalogue;
    }

    public function setPrixCatalogue(?string $prixCatalogue): self
    {
        $this->prixCatalogue = $prixCatalogue;

        return $this;
    }

    public function getColorName(): ?string
    {
        return $this->colorName;
    }

    public function setColorName(?string $colorName): self
    {
        $this->colorName = $colorName;

        return $this;
    }
}
