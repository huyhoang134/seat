<?php

namespace App\Entity;

use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Repository\CustomersMediaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Customers_media")
 * @ORM\Entity(repositoryClass=CustomersMediaRepository::class)
 */
class CustomersMedia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=CustomersApplication::class, inversedBy="customersMedia", cascade={"persist"})
     * @ORM\JoinColumn(name="customers_application_id", referencedColumnName="id_application")
     */
    private $customersApplication;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, inversedBy="customersMedia")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $media;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomersApplication(): ?CustomersApplication
    {
        return $this->customersApplication;
    }

    public function setCustomersApplication(?CustomersApplication $customersApplication): self
    {
        $this->customersApplication = $customersApplication;

        return $this;
    }

    public function getMedia(): ?Media
    {
        return $this->media;
    }

    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
