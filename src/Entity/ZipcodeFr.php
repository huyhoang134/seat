<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZipcodeFr
 *
 * @ORM\Table(name="zipcode_fr")
 * @ORM\Entity(repositoryClass="App\Repository\ZipcodeFrRepository")
 */
class ZipcodeFr
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_commune_insee", type="string", length=255, nullable=false)
     */
    private $codeCommuneInsee;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_de_la_commune", type="string", length=255, nullable=false)
     */
    private $nomDeLaCommune;

    /**
     * @var int
     *
     * @ORM\Column(name="code_postal", type="integer", nullable=false)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=11, scale=8, nullable=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=8, nullable=false)
     */
    private $latitude;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeCommuneInsee(): ?string
    {
        return $this->codeCommuneInsee;
    }

    public function setCodeCommuneInsee(string $codeCommuneInsee): self
    {
        $this->codeCommuneInsee = $codeCommuneInsee;

        return $this;
    }

    public function getNomDeLaCommune(): ?string
    {
        return $this->nomDeLaCommune;
    }

    public function setNomDeLaCommune(string $nomDeLaCommune): self
    {
        $this->nomDeLaCommune = $nomDeLaCommune;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }


}
