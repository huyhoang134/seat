<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersAlgoan
 *
 * @ORM\Table(name="Customers_Algoan", indexes={@ORM\Index(name="fk_Customers_Algoan_Customers_application1_idx", columns={"id_application"})})
 * @ORM\Entity(repositoryClass="App\Repository\CustomersAlgoanRepository")
 */
class CustomersAlgoan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="algoan_extnal_id", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $algoanExtnalId = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $name = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $surname = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="civility", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $civility = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="postal_code", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $postalCode = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="phone", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $phone = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $email = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_bankaccount_opened", type="date", nullable=true, options={"default"="NULL"})
     */
    private $dateBankaccountOpened = 'NULL';

    /**
     * @var CustomersApplication
     *
     * @ORM\ManyToOne(targetEntity="CustomersApplication")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_application", referencedColumnName="id_application")
     * })
     */
    private $idApplication;


    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $customerId = 'NULL';

    /**
     * @var array|null
     *
     * @ORM\Column(name="analysis", type="text", nullable=true, options={"default"="NULL"})
     */
    private $analysis;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt = null;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt = null;

    /**
     * @ORM\Column(name="is_checked_analysis", type="boolean", nullable=true)
     */
    private $isCheckedAnalysis;

    /**
     * @ORM\Column(name="count_checked_analysis", type="integer", nullable=true)
     */
    private $countCheckedAnalysis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlgoanExtnalId(): ?string
    {
        return $this->algoanExtnalId !== null ? (string)$this->algoanExtnalId : null;
    }

    public function setAlgoanExtnalId(?string $algoanExtnalId): self
    {
        $this->algoanExtnalId = $algoanExtnalId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getCivility(): ?bool
    {
        return $this->civility;
    }

    public function setCivility(?bool $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(?int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(?int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getDateBankaccountOpened(): ?\DateTimeInterface
    {
        return $this->dateBankaccountOpened;
    }

    public function setDateBankaccountOpened(?\DateTimeInterface $dateBankaccountOpened): self
    {
        $this->dateBankaccountOpened = $dateBankaccountOpened;

        return $this;
    }

    public function getIdApplication(): ?CustomersApplication
    {
        return $this->idApplication;
    }

    public function setIdApplication(?CustomersApplication $idApplication): self
    {
        $this->idApplication = $idApplication;

        return $this;
    }

    public function getAnalysis(): ?array
    {
        return $this->analysis ? unserialize($this->analysis) : NULL;
    }

    public function setAnalysis(?array $analysis): self
    {

        $this->analysis = $analysis ? serialize($analysis) : NULL;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsCheckedAnalysis(): ?bool
    {
        return $this->isCheckedAnalysis;
    }

    public function setIsCheckedAnalysis(?bool $isCheckedAnalysis): self
    {
        $this->isCheckedAnalysis = $isCheckedAnalysis;

        return $this;
    }

    public function getCountCheckedAnalysis(): ?int
    {
        return $this->countCheckedAnalysis;
    }

    public function setCountCheckedAnalysis(?int $countCheckedAnalysis): self
    {
        $this->countCheckedAnalysis = $countCheckedAnalysis;

        return $this;
    }
}
