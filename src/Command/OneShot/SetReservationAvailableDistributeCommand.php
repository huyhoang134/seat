<?php

namespace App\Command\OneShot;

use App\Entity\Distributeurs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetReservationAvailableDistributeCommand extends Command
{
    protected static $defaultName = 'app:set-reservation-available-distribute';
    protected static $defaultDescription = 'Set Reservation Available Distribute';
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription);
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $distributes = $this->entityManager->getRepository(Distributeurs::class)->findDidtribByKvpsDe($this->reservationAvailableDistribute());
        
        foreach ($distributes as $distribute) {
            $distribute->setIsReservationAvailable(true);
            $this->entityManager->persist($distribute);
            
            $io->writeln('Distribute ' . $distribute->getId() . ' set reservation available');
        }
        
        $this->entityManager->flush();

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }

    private function reservationAvailableDistribute(): array
    {
        return [
            'FRAS02488',
            'FRAS62840',
            'FRAS01543',
            'FRAS01420',
            'FRAS02676',
            'FRAS06080',
            'FRAS64390',
            'FRAS02636',
            'FRAS02665',
            'FRAS62680',
            'FRAS07430',
            'FRASFR597',
            'FRAS63770',
            'FRAS60215',
            'FRAS06181',
            'FRAS63930',
            'FRAS64550',
            'FRAS07211',
            'FRAS64900',
            'FRAS64460',
            'FRAS02524',
            'FRAS08790',
            'FRAS02169',
            'FRAS63730',
            'FRAS02608',
            'FRAS02654',
            'FRAS02538',
            'FRAS02628',
            'FRAS02379',
            'FRAS01603',
            'FRAS02606',
            'FRAS02422',
            'FRAS07790',
            'FRAS02641',
            'FRAS63630',
            'FRAS02639',
            'FRAS64190',
            'FRAS02638',
            'FRAS97333',
            'FRAS07746',
            'FRAS01740',
            'FRAS05720',
            'FRAS08380',
            'FRAS07980',
            'FRAS01723',
            'FRAS02612',
            'FRAS07060',
            'FRAS01773',
            'FRAS60171',
            'FRAS2430',
            'FRAS02687',
        ];
    }
}
