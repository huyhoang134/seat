<?php

namespace App\Command;

use App\Entity\CustomersAlgoan;
use App\Services\AlgoanService;
use App\Services\BankPushService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetAnalysisAlgoanCommand extends Command
{
    protected static $defaultName = 'app:get-analysis-algoan';
    protected static $defaultDescription = 'Get analysis data from customerId and AlgoanExtnalId';

    private $entityManager;

    private $algoanService;

    /**
     * @var BankPushService
     */
    private $bankPushService;

    const NUMBER_OF_ANALYSYS_CHECKED = 5;

    public function __construct(
        EntityManagerInterface $entityManager,
        AlgoanService $algoanService,
        BankPushService $bankPushService
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->algoanService = $algoanService;
        $this->bankPushService = $bankPushService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('limit-count-get-analysis', null, InputOption::VALUE_OPTIONAL, 'Limit Count Get Analysis')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startTime = microtime(true);
        
        $numberOfAnalysisChecks = $input->getOption('limit-count-get-analysis') ? intval($input->getOption('limit-count-get-analysis')) : self::NUMBER_OF_ANALYSYS_CHECKED;

        $customerAlgoanRepository = $this->entityManager->getRepository(CustomersAlgoan::class);

        $customersAlgoan = $customerAlgoanRepository->findCustomersAlgoanForCheckAnalysis();

        /* @var CustomersAlgoan $customerAlgoan */
        foreach ($customersAlgoan as $customerAlgoan) {
            if ($customerAlgoan->getAnalysis() &&
                ($customerAlgoan->getAnalysis()['activityScore'] !== 0 && $customerAlgoan->getAnalysis()['activityScore'] !== '')
            ) {
                $customerAlgoan->setIsCheckedAnalysis(true);
            }

            if (!$customerAlgoan->getAnalysis() || $customerAlgoan->getAnalysis()['activityScore'] === '' || $customerAlgoan->getAnalysis()['creditScore'] === '') {
                $result = $this->algoanService->getAnalysis($customerAlgoan->getCustomerId(), $customerAlgoan->getAlgoanExtnalId());

                $activityScoreValue = $result['metadata']['activityScore']['value'] ?? '';
                $creditScoreValue = $result['scores']['creditScore']['value'] ?? '';
                $error = '';

                if (array_key_exists('code', $result) && $result['code'] == 'UNKNOWN_ENTITY' && $result['status'] == '404') {
                    //
                    $customerAlgoan->setIsCheckedAnalysis(true);
                    $error = $result['message'];
                }

                if ($activityScoreValue && $creditScoreValue) {
                    $customerAlgoan->setIsCheckedAnalysis(true);
                } else {
                    $customerAlgoan->setIsCheckedAnalysis(false);
                    $customerAlgoan->setCountCheckedAnalysis($customerAlgoan->getCountCheckedAnalysis() + 1);

                    if ($customerAlgoan->getCountCheckedAnalysis() === $numberOfAnalysisChecks) {
                        $customerAlgoan->setIsCheckedAnalysis(true);
                    }
                }

                $dataAnalysis = ['activityScore' => $activityScoreValue, 'creditScore' => $creditScoreValue, 'error' => $error];
                $customerAlgoan->setAnalysis($dataAnalysis);

                $customerAlgoan->setUpdatedAt(new \DateTime());
                $this->entityManager->persist($customerAlgoan);

                $customerApplication = $customerAlgoan->getIdApplication();

                // Fix issue sending email with attached file without Algoan scoring when QS is received before Algoan
                if ($customerAlgoan->getIsCheckedAnalysis() && $customerApplication->getIsKycStatusFebOK() && $customerApplication->getKycSessionId()) {
                    $this->entityManager->flush();
                    
                    //summarizeUserJourney
                    $this->bankPushService->summarizeUserJourney($customerApplication, $customerApplication->getKycSessionId());
                }
            }
        }

        //check invalid records
        $customersAlgoanInvalidRecords  = $customerAlgoanRepository->findCustomersAlgoanInvalidRecords();

        /* @var CustomersAlgoan $customerAlgoan */
        foreach ($customersAlgoanInvalidRecords as $customerAlgoan) {
            $customerAlgoan->setIsCheckedAnalysis(true);
            $this->entityManager->persist($customerAlgoan);
        }

        $this->entityManager->flush();

        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;
        
        $output->writeln("Completed in: " . number_format($executionTime, 2) . " seconds");

        return 0;
    }
}
