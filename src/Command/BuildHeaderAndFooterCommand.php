<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Dotenv\Dotenv;

class BuildHeaderAndFooterCommand extends Command
{
    protected static $defaultName = 'build-header-and-footer';
    protected static $defaultDescription = 'Build header and footer';
    /**
     * @var ParameterBagInterface
     */
    private $params;

    public function __construct(ParameterBagInterface $params, $name = null)
    {
        parent::__construct($name);
        $this->params = $params;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->setHelp('Please change HEADER_FOOTER_URL in env file or use command option. HEADER_FOOTER_URL is url of template, APP_INCLUDE_CONTENT is string of content for replace')
            ->addOption('url', null, InputOption::VALUE_OPTIONAL, 'HEADER_FOOTER_URL')
            ->addOption('include-string', null, InputOption::VALUE_OPTIONAL, 'APP_INCLUDE_CONTENT')
            ->addOption('remove-string-placeholders', null, InputOption::VALUE_OPTIONAL, 'Strings that you want to remove from the template eg ###APP_INCLUDE_HEADER###, ###APP_INCLUDE_ANALYTICS_BODY###')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $url = $input->getOption('url') ?? $this->params->get('header-footer-url');
        $searchStr = $input->getOption('include-string') ?? $this->params->get('app-include-content');

        $removeStrArray = [];
        $removeStr = $input->getOption('remove-string-placeholders') ?? $this->params->get('remove-placeholders-string-content');
        if ($removeStr) {
            $removeStrArray = explode(',', $removeStr);
        }

        $io->writeln('Start download header and footer template');

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $io->writeln('Download header and footer template success');

        $relativePath = 'header-footer.html.twig';
        $templateDefaultPath = $this->params->get('kernel.project_dir') . '/templates/default/';
        $templatePath = $templateDefaultPath . $relativePath;

        file_put_contents($templatePath, $result);

        if (filesize($templatePath) > 0) {
            $io->writeln('Created header-footer file');

            $io->writeln('Start build header and footer template');
            $this->buildContentHeaderAndFooter($templatePath, $templateDefaultPath, $io, $searchStr, $removeStrArray);
        }

        return 0;
    }

    private function buildContentHeaderAndFooter($templatePath, $templateDefaultPath, $io, $searchStr, $removeStrArray)
    {
        //remove text
        $content = str_replace($removeStrArray, "", file_get_contents($templatePath));
        
        $start_pos = strpos($content, $searchStr);

        if ($start_pos !== false) {
            $htmlHeaderContent = substr($content, 0, $start_pos);
            $htmlFooterContent = substr($content, $start_pos + strlen($searchStr));
            
            file_put_contents($templateDefaultPath . 'escooter-header.html.twig', $htmlHeaderContent);
            $io->writeln('Build header complete');

            file_put_contents($templateDefaultPath . 'escooter-footer.html.twig', $htmlFooterContent);
            $io->writeln('Build footer complete');

            $io->success('Success!!!');
        } else {
            $io->warning('Can not find content string for replace');

            $io->error('Please replace url or string for replace!!!');
        }
    }
}
