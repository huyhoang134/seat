<?php

namespace App\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EnvironmentVariableExtension extends AbstractExtension
{

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getUrlEnv', [$this, 'getUrlEnv']),
        ];
    }

    public function getUrlEnv($name)
    {
        return $this->parameterBag->get($name);
    }
}