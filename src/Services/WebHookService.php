<?php

namespace App\Services;

use App\Entity\CustomersApplication;
use App\Entity\WebhookLogs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class WebHookService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function saveWebHookLogs($data, $idCustomerApplication = null, $caseId = null)
    {
        $webHook = new WebhookLogs();

        $webHook->setCaseId($caseId);
        $webHook->setEventType("webhook" ?? null);
        $webHook->setEventData($data);
        $webHook->setCustomerApplication($idCustomerApplication);
        $webHook->setTimeStamp(null);
        $webHook->setCreatedAt(new \DateTime());
        $webHook->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($webHook);
        $this->entityManager->flush();

        return $webHook;
    }

}
