<?php

namespace App\Services;

use App\Entity\CustomersAlgoan;
use App\Entity\CustomersApplication;
use App\Entity\CustomersMedia;
use App\Entity\EscooterConfiguration;
use App\Entity\WebhookLogs;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\MimeTypes;

class BankPushService
{
    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @var Pool
     */
    private $pool;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    private $token;

    private $encryptionService;

    private $entityManager;

    private $requestStack;

    public function __construct(EmailService $emailService, Pool $pool, ContainerInterface $container, ParameterBagInterface $parameterBag, EncryptionService $encryptionService, EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->emailService = $emailService;
        $this->pool         = $pool;
        $this->container    = $container;
        $this->parameterBag = $parameterBag;
        $this->encryptionService = $encryptionService;
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->generateKycToken();
    }

    public function summarizeUserJourney($customerApplication, $sessionId = null)
    {
        try {
            $this->bankPush($customerApplication, $sessionId);

            $excelFileContent = file_get_contents($this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplication->getIdApplication() . '/escooter-tarifeur.xlsx');
            $base64EncodedExcelFile = base64_encode($excelFileContent);

            $attachFiles = $this->attachFiles($customerApplication);

            //send mail
            //$toEmail = $customerApplication->getEmail();
            $toEmail = 'julien.darchy@tribal.paris';
            //$toEmail = 'tbourdin@partitech.com';
            $subject = 'Demande de Financement';

            $escooterConfiguration = $this->entityManager->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplication->getIdApplication()]);
            $modelName = $escooterConfiguration->getVehicleModel();

            $this->emailService->sendMailBankPush($toEmail, [
                'firstName' => $customerApplication->getName(),
                'lastname'  => $customerApplication->getSurname(),
                'fullName'  => $customerApplication->getName() . ' ' . $customerApplication->getSurname(),
                'civility'  => $customerApplication->getCivility(),
                'modelName' => $modelName
            ], $base64EncodedExcelFile, $attachFiles, $subject);

            $folderPath = $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplication->getIdApplication();
            $files = glob($folderPath . '/*');
            foreach ($files as $file) {
                // Check if the path is a file (not a directory)
                if (is_file($file)) {
                    // Delete the file
                    unlink($file);
                }
            }
            //send lead
            $campaignId = $this->parameterBag->get('send-lead-campaign-id');
            $this->leadConnect($customerApplication, $campaignId);
        } catch (\Exception $e) {
        }
    }

    private function attachFiles($customerApplication): array
    {
        $finder = new Finder();
        $files = $finder->files()->in($this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplication->getIdApplication())->notName('escooter-tarifeur.xlsx');
        $attachFiles = [];

        foreach ($files as $file) {
            $fileName = $file->getFilename();
            $fileContents = file_get_contents($file->getRealPath());
            $mimeTypes = new MimeTypes();
            $contentType = $mimeTypes->guessMimeType($file->getRealPath());

            $attachFiles[$file->getFilename()]['Filename'] = $fileName;
            $attachFiles[$file->getFilename()]['ContentType'] = $contentType;
            $attachFiles[$file->getFilename()]['Base64Content'] = base64_encode($fileContents);
        }

        return $attachFiles;
    }

    private function bankPush($customerApplication, $sessionId = null)
    {
        $customerApplicationId = $customerApplication->getIdApplication();

        $doctrine = $this->container->get('doctrine');
        $inputFileName = $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/tarifeur.xlsx';
        $spreadsheet = IOFactory::load($inputFileName);

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        /** @var CustomersApplication $customerApplication */
        $customerApplication = $doctrine->getRepository(CustomersApplication::class)->findOneBy(['idApplication' => $customerApplicationId]);

        $escooterConfiguration = $doctrine->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplication]);

        /** @var CustomersAlgoan $customerAlgoan */
        $customerAlgoan = $doctrine->getRepository(CustomersAlgoan::class)->findOneBy(['idApplication' => $customerApplication]);

        $scoreAnalysis = $customerAlgoan->getAnalysis() ?? [];
        
        $this->setDataToExcelFile($customerApplication, $spreadsheet, $escooterConfiguration, $scoreAnalysis);

        $insertedExcelFilePath = $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplicationId;
        if (!file_exists($insertedExcelFilePath)) {
            mkdir($insertedExcelFilePath, 0777, true);
        }
        $excelFile = $insertedExcelFilePath . '/escooter-tarifeur.xlsx';
        $writer->save($excelFile);
        
        // Get contracts file
        $certificateFile = false;
        $webhookLogs = $doctrine->getRepository(WebhookLogs::class)->findBy(['customerApplication' => $customerApplicationId]);
        
        /** @var WebhookLogs $webhookLog */
        foreach ($webhookLogs as $webhookLog) {
            $eventData = $webhookLog->getEventData();

            if ($eventData) {
                $formatEventData = json_decode($eventData, true);

                if (isset($formatEventData['contractId']) && isset($formatEventData['type']) && $formatEventData['type'] == 'journeyEnded') {
                    $certificateFile = $this->getCertificateFile($formatEventData['contractId']);
                    
                    break;
                }
            }
        }

        // Get media file
        $fileSystem = new Filesystem();
        $customerMedias = $doctrine->getRepository(CustomersMedia::class)->findBy(['customersApplication' => $customerApplicationId]);

        foreach ($customerMedias as $customerMedia) {
            $provider = $this->pool->getProvider($customerMedia->getMedia()->getProviderName());
            $mediaPublicUrl = $provider->generatePublicUrl($customerMedia->getMedia(), MediaProviderInterface::FORMAT_REFERENCE);
            $mediaFile = $this->parameterBag->get('kernel.project_dir').'/public' . $mediaPublicUrl;
            $fileSystem->copy($mediaFile, $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplicationId . '/' . basename($mediaFile));
        }

        if ($sessionId) {
            try {
                //Get documents file
                $workspaceId = $this->getKycWorkspaceId($sessionId);
                $documentIds = $this->getKycDocumentId($sessionId, $workspaceId);
                $documentsFile = $this->getKycDocumentFile($documentIds);

                // Add documents file to the zip archive
                if ($documentsFile) {
                    foreach ($documentsFile as $documentFile) {
                        $fileSystem->copy($documentFile, $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplicationId . '/' . basename($documentFile));
                    }
                }
            } catch (\Exception $e) {
            }
        }

        // Add the files to the zip archive
        if ($certificateFile) {
            $fileSystem->copy($certificateFile, $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/' . $customerApplicationId . '/' . basename($certificateFile));
        }
    }

    private function setDataToExcelFile(CustomersApplication $customerApplication, Spreadsheet $spreadsheet, EscooterConfiguration $escooterConfiguration, array $scoreAnalysis = [])
    {
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B7', $escooterConfiguration->getVehicleEngine());
        $sheet->getStyle('B7')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B8', $escooterConfiguration->getVehicleModel());
        $sheet->getStyle('B8')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B10', $escooterConfiguration->getPrixCatalogue());
        $sheet->getStyle('B10')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B14', $escooterConfiguration->getVehicleKmAnnual());
        $sheet->getStyle('B14')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B18', $customerApplication->getDistributeurs()->getRaisonSociale());
        $sheet->getStyle('B18')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B19', $customerApplication->getDistributeurs()->getKvps());
        $sheet->getStyle('B19')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B20', $customerApplication->getDistributeurs()->getCodeCe());
        $sheet->getStyle('B20')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B21', $customerApplication->getDistributeurs()->getAdresse());
        $sheet->getStyle('B21')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B22', $customerApplication->getDistributeurs()->getCodePostal());
        $sheet->getStyle('B22')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B23', $customerApplication->getDistributeurs()->getVille());
        $sheet->getStyle('B23')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B24', $customerApplication->getDistributeurs()->getTelephone());
        $sheet->getStyle('B24')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');

        $sheet->setCellValue('B25', $customerApplication->getDistributeurs()->getDealerName());
        $sheet->getStyle('B25')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B26', $customerApplication->getDistributeurs()->getDealerCode());
        $sheet->getStyle('B26')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B27', $customerApplication->getDistributeurs()->getDealerEmail());
        $sheet->getStyle('B27')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B28', $customerApplication->getDistributeurs()->getDealerPhone());
        $sheet->getStyle('B28')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B29', $customerApplication->getDistributeurs()->getDealerResp());
        $sheet->getStyle('B29')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        
        $sheet->setCellValue('B34', strtoupper($customerApplication->getCivility()));
        $sheet->getStyle('B34')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B35', $customerApplication->getName());
        $sheet->getStyle('B35')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B36', $customerApplication->getSurname());
        $sheet->getStyle('B36')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B37', date_format($customerApplication->getDateOfBirth(), 'd-m-Y'));
        $sheet->getStyle('B37')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B38', $customerApplication->getPhone());
        $sheet->getStyle('B38')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B39', $customerApplication->getEmail());
        $sheet->getStyle('B39')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B40', $customerApplication->getOccupation());
        $sheet->getStyle('B40')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B41', $customerApplication->getMaidenName());
        $sheet->getStyle('B41')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B42', $customerApplication->getZipcodeBirth());
        $sheet->getStyle('B42')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B43', $customerApplication->getCityBirth());
        $sheet->getStyle('B43')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B44', $customerApplication->getNationality());
        $sheet->getStyle('B44')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B45', $customerApplication->getAdress());
        $sheet->getStyle('B45')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B46', $customerApplication->getAdress2());
        $sheet->getStyle('B46')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B47', $customerApplication->getZipcode());
        $sheet->getStyle('B47')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B48', $customerApplication->getCity());
        $sheet->getStyle('B48')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B49', $customerApplication->getCountry());
        $sheet->getStyle('B49')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B50', $customerApplication->getMainResidence());
        $sheet->getStyle('B50')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B51', $customerApplication->getFamilySitutation());
        $sheet->getStyle('B51')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B52', $customerApplication->getChilds());
        $sheet->getStyle('B52')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B53', $customerApplication->getEmployeur());
        $sheet->getStyle('B53')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');

        $sheet->setCellValue('B54', $customerApplication->getContractType());
        $sheet->getStyle('B54')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B55', date_format($customerApplication->getHiringDate(), 'd-m-Y'));
        $sheet->getStyle('B55')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B56', $customerApplication->getDueDay());
        $sheet->getStyle('B56')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        
        $sheet->setCellValue('B58', $customerApplication->getRevenuNetMensuel());
        $sheet->getStyle('B58')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B59', $customerApplication->getAutresRevenus());
        $sheet->getStyle('B59')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B60', $customerApplication->getChargesImmobilieres());
        $sheet->getStyle('B60')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B61', $customerApplication->getAutresRemboursementsMensuels());
        $sheet->getStyle('B61')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        
        $sheet->setCellValue('B79', $escooterConfiguration->getOptinInsuranceGcm() == 1 ? 'OUI' : 'NON');
        $sheet->getStyle('B79')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B80', $escooterConfiguration->getOptinInsuranceVdr() == 1 ? 'OUI' : 'NON');
        $sheet->getStyle('B80')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B81', $escooterConfiguration->getAssurance() == 1 ? 'OUI' : 'NON');
        $sheet->getStyle('B81')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B84', $escooterConfiguration->getLoaDuree());
        $sheet->getStyle('B84')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B85', $escooterConfiguration->getLoyerMajore());
        $sheet->getStyle('B85')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        $sheet->setCellValue('B86', $escooterConfiguration->getPrixAPartirDe()  . ' €');
        $sheet->getStyle('B86')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        
        $sheet->setCellValue('B89', $scoreAnalysis ? $scoreAnalysis['activityScore'] : 0);
        if ($scoreAnalysis && $scoreAnalysis['activityScore'] == 0 ) {
            $sheet->getStyle('B89')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF');
        } elseif ($scoreAnalysis && $scoreAnalysis['activityScore'] > 500) {
            $sheet->getStyle('B89')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('149414');
        } else {
            $sheet->getStyle('B89')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        }
        $sheet->setCellValue('B90', $scoreAnalysis ? $scoreAnalysis['creditScore'] : 0);
        if ($scoreAnalysis && $scoreAnalysis['creditScore'] == 0 ) {
            $sheet->getStyle('B90')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF');
        } elseif ($scoreAnalysis && $scoreAnalysis['creditScore'] > 500) {
            $sheet->getStyle('B90')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('149414');
        } else {
            $sheet->getStyle('B90')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        }
        $sheet->setCellValue('B91', $scoreAnalysis ? $scoreAnalysis['error'] : 0);
        if ($scoreAnalysis && $scoreAnalysis['error'] == 0 ) {
            $sheet->getStyle('B91')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF');
        } elseif ($scoreAnalysis && $scoreAnalysis['error'] > 500) {
            $sheet->getStyle('B91')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('0077FF');
        } else {
            $sheet->getStyle('B91')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFA500');
        }
    }

    private function getCertificateFile($contractId)
    {
        try {
            // get token
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('quicksign-url') . '/applicant/api/oauth2/token');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
                'grant_type'    => 'client_credentials',
                'client_id'     => $this->parameterBag->get('quicksign-client-id'),
                'client_secret' => $this->parameterBag->get('quicksign-client-secret'),
            ]));

            $response = curl_exec($ch);
            $err      = curl_error($ch);

            curl_close($ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $ret          = json_decode($response);
                $access_token = $ret->access_token;

            }
            // end get token

            $url = $this->parameterBag->get('quicksign-url') . '/sign/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/contracts/' . $contractId . '/file';

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);

            $response = curl_exec($ch);

            if (curl_error($ch)) {
                echo 'cURL Error #: ' . curl_error($ch);
            } else {
                $savePath = $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/';

                $fileName = 'certificate.pdf';

                // remove certificate if exist
                $certificateFilePath = $savePath . 'certificate.pdf';

                if (file_exists($certificateFilePath)) {
                    unlink($certificateFilePath);
                }
                // end remove certificate if exist

                file_put_contents($savePath . $fileName, $response);

                return $savePath . 'certificate.pdf';
            }
        } catch (\Exception $e) {
            return false;
        }
        
        return false;
    }

    public function requestToApi($data)
    {
        $ch   = curl_init();
        curl_setopt($ch, CURLOPT_URL,
            $this->parameterBag->get('leadconnect-url') . '?' . urlencode(json_encode($data)));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, false);
        return curl_exec($ch);
    }

    public function leadConnect($customerApplication, $campaignId)
    {
        $doctrine = $this->container->get('doctrine');
        $escooterConfiguration = $doctrine->getRepository(EscooterConfiguration::class)->findOneBy(['idApplication' => $customerApplication->getIdApplication()]);

        $options = $escooterConfiguration->getVehicleOptions() ? $this->getDataAccessory($escooterConfiguration->getVehicleOptions()) : '';
        $comment = 'Option: '. $options . ', '. 'Mensualités (Monthly payments): '. $escooterConfiguration->getPrixAPartirDe().'€';

        $data = [
            'dealer'        => $customerApplication->getDistributeurs()->getKvps(),
            'action'        => 'lead',
            'campaignid'    => $campaignId,
            'model'         => $escooterConfiguration->getVehicleModel(),
            'civility'      => $customerApplication->getCivility(),
            'last_name'     => $customerApplication->getSurname(),
            'first_name'    => $customerApplication->getName(),
            'email'         => $customerApplication->getEmail(),
            'zip_code'      => $customerApplication->getZipcode(),
            'phone'         => $customerApplication->getPhone(),
            'privacy_email' => '',
            'comment'       => $comment,
            'created_at'    => date('Y/m/d H:m:s'),
        ];

        return $this->requestToApi($data);
    }

    private function generateKycToken()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('quicksign-url') . '/kyc/api/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'grant_type'    => 'client_credentials',
            'client_id'     => $this->parameterBag->get('quicksign-client-id'),
            'client_secret' => $this->parameterBag->get('quicksign-client-secret'),
        ]));

        $response = curl_exec($ch);
        $err      = curl_error($ch);

        curl_close($ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $ret          = json_decode($response);
            $this->token  = $ret->access_token;
        }
    }
    public function getKycWorkspaceId($sessionId)
    {
        try {
            $access_token = $this->token;

            $url = $this->parameterBag->get('quicksign-url') . '/kyc/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/sessions/' . $sessionId;

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);

            $response = curl_exec($ch);

            if (curl_error($ch)) {
                echo 'cURL Error #: ' . curl_error($ch);
            } else {
                $result = json_decode($response, true);
                return $result['workspaceId'];
            }
        } catch (\Exception $e) {}
    }

    public function getKycDocumentId($sessionId, $workspaceId)
    {
        try {
            $access_token = $this->token;

            $url = $this->parameterBag->get('quicksign-url') . '/kyc/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/workspaces/' . $workspaceId . '/results';

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);

            $response = curl_exec($ch);

            if (curl_error($ch)) {
                echo 'cURL Error #: ' . curl_error($ch);
            } else {
                $result = json_decode($response, true);
                $sessionsResults = $result['sessionsResults'];
                foreach ($sessionsResults as $session) {
                    if ($session['sessionId'] == $sessionId) {
                        $requiredElementResults = $session['requiredElementResults'];

                        foreach ($requiredElementResults as $requiredElementResult) {
                            return $requiredElementResult['documentIds'];
                        }
                    }
                }
            }
        } catch (\Exception $e) {}
    }

    public function getKycDocumentFile($documentIds)
    {
        $documentsFile = [];
        try {
            $access_token = $this->token;

            foreach ($documentIds as $documentId) {
                $url = $this->parameterBag->get('quicksign-url') . '/kyc/api/organizations/' . $this->parameterBag->get('quicksign-organization') . '/documents/' . $documentId . '/file';

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $access_token"]);

                $response = curl_exec($ch);

                list($headers, $body) = explode("\r\n\r\n", $response, 2);

                if (preg_match('/filename="(.*?)"/', $headers, $matches)) {
                    $fileName = $matches[1];
                } else {
                    $fileName = 'unknown';
                }

                $savePath = $this->parameterBag->get('kernel.project_dir') . '/public/bank-push/';
                file_put_contents($savePath . $fileName, $body);

                $documentsFile[] = $savePath . $fileName;
            }

            return $documentsFile;
        } catch (\Exception $e) {}
    }

    private function getDataAccessory(string $ids): string
    {
        $dataAccessory = [
            [
                "id" => "1L0050320E",
                "label" => "Casque JET G262 HEBO"
            ],
            [
                "id" => "1L0052100",
                "label" => "Smart light"
            ],
            [
                "id" => "1L0050320A",
                "label" => "Casque Intégral G361 HEBO"
            ],
            [
                "id" => "1L0071101",
                "label" => "Support de malle"
            ],
            [
                "id" => "1L0061121",
                "label" => "Malle 39 litres"
            ],
            [
                "id" => "1L0084342E",
                "label" => "Gants HEBO City Eté"
            ],
            [
                "id" => "1L0071267A",
                "label" => "Antivol pour scooter"
            ],
            [
                "id" => "1L0051435",
                "label" => "Support Smartphone"
            ],
            [
                "id" => "1L0064160",
                "label" => "Pare-brise haute protection"
            ],
        ];

        $idsToFind = explode(",", $ids);

        $labels = [];

        foreach ($dataAccessory as $item) {
            if (in_array($item['id'], $idsToFind)) {
                $labels[] = $item['label'];
            }
        }

        return implode("/ ", $labels);
    }
}