<?php

namespace App\Services;

use App\Entity\CustomersApplication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EmailService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EncryptionService
     */
    private $encryptionService;

    private $parameterBag;

    public function __construct(EntityManagerInterface $entityManager, EncryptionService $encryptionService, ParameterBagInterface $parameterBag)
    {
        $this->entityManager = $entityManager;
        $this->encryptionService = $encryptionService;
        $this->parameterBag = $parameterBag;
    }

    public function sendMail(string $to, array $customerApplicationName, string $callBackUrl, string $colorName, string $subject = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('mj-url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->parameterBag->get('mj-apikey-public') . ':' . $this->parameterBag->get('mj-apikey-private'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $data = array(
            "Messages" => array(
                array(
                    "From" => array(
                        "Email" => $this->parameterBag->get('mj-sender-address-email'),
                        "Name" => $this->parameterBag->get('mj-sender-address-name')
                    ),
                    "To" => array(
                        array(
                            "Email" => $to,
                            "Name" => $customerApplicationName['firstName'].' '.$customerApplicationName['lastname']
                        )
                    ),
                    "TemplateID" => 5620719,
                    "TemplateLanguage" => true,
                    "Subject" => $subject,
                    "CustomCampaign" => "SEAT Financement - Transactionnel",
                    "Variables" => array(
                        "firstname" => $customerApplicationName['firstName'],
                        "lastname" => $customerApplicationName['lastname'],
                        "url" => $callBackUrl,
                        "label_name" => $colorName
                    )
                )
            )
        );

        $jsonData = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur cURL : ' . curl_error($ch);
        }

        curl_close($ch);
    }

    public function sendMailBankPush(string $to, array $customerApplication = [], string $excelFile = '', array $files = [], string $subject = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('mj-url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->parameterBag->get('mj-apikey-public') . ':' . $this->parameterBag->get('mj-apikey-private'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $attachments = [
            [
                "Filename" => "escooter-tarifeur.xlsx",
                "ContentType" => "application/vnd.ms-excel",
                "Base64Content" => $excelFile
            ]
        ];

        foreach ($files as $file) {
            $attachments[] = $file;
        }

        $data = array(
            "Messages" => array(
                array(
                    "From" => array(
                        "Email" => $this->parameterBag->get('mj-sender-address-email'),
                        "Name" => $this->parameterBag->get('mj-sender-address-name')
                    ),
                    "To" => array(
                        array(
                            "Email" => "julien.darchy@tribal.paris",
                            "Name" => "Julien"
                        )
                    ),
                    "TemplateID" => 5679775,
                    "TemplateLanguage" => true,
                    "CustomCampaign" => "SEAT Financement - Transactionnel",
                    "Subject" => $subject,
                    "Variables" => array(
                        "firstname" => $customerApplication['firstName'],
                        "lastname" => $customerApplication['lastname'],
                        "civility" => $customerApplication['civility'],
                        "model_name" => $customerApplication['modelName'],
                    ),
                    "Attachments" => $attachments
                )
            )
        );

        $jsonData = json_encode($data);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            //echo 'Erreur cURL : ' . curl_error($ch);
        }

        curl_close($ch);
    }

    public function getEmailAndEncryptionId(int $customerApplicationId): array
    {
        $encryptionApplicationId = $this->encryptionService->encryption($customerApplicationId);
        $customerApplication     = $this->entityManager->getRepository(CustomersApplication::class)->find($customerApplicationId);

        return [
            'email' => $customerApplication->getEmail(),
            'encryptionId' => $encryptionApplicationId
        ];
    }

    public function getNameByCustomersApplicationId(int $customersApplicationId): array
    {
        /** @var CustomersApplication $customerApplication */
        $customerApplication = $this->entityManager->getRepository(CustomersApplication::class)->find($customersApplicationId);

        return [
            'firstName' => $customerApplication->getName(),
            'lastname' => $customerApplication->getSurname(),
            'fullName' => $customerApplication->getName() . ' ' . $customerApplication->getSurname()
        ];
    }

    public function sendMailFEBKO(string $to, array $customerApplicationName, string $callBackUrl, string $colorName, string $subject = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->parameterBag->get('mj-url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->parameterBag->get('mj-apikey-public') . ':' . $this->parameterBag->get('mj-apikey-private'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $data = array(
            "Messages" => array(
                array(
                    "From" => array(
                        "Email" => $this->parameterBag->get('mj-sender-address-email'),
                        "Name" => $this->parameterBag->get('mj-sender-address-name')
                    ),
                    "To" => array(
                        array(
                            "Email" => $to,
                            "Name" => $customerApplicationName['firstName'].' '.$customerApplicationName['lastname']
                        )
                    ),
                    "TemplateID" => 6030340,
                    "TemplateLanguage" => true,
                    "Subject" => $subject,
                    "CustomCampaign" => "SEAT Financement - Transactionnel",
                    "Variables" => array(
                        "firstname" => $customerApplicationName['firstName'],
                        "model_name"=> "Seat MO 125",
                        "url" => '',
                    )
                )
            )
        );

        $jsonData = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur cURL : ' . curl_error($ch);
        }

        curl_close($ch);
    }
}
