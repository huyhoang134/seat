<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AlgoanService
{
    private $algoanToken;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    private function generateAlgoanToken()
    {
        $algoanClientId     = $this->parameterBag->get('algoan-client-id');
        $algoanClientSecret = $this->parameterBag->get('algoan-client-secret');
        $algoanUrl          = $this->parameterBag->get('algoan-url');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $algoanUrl . '/v1/oauth/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=$algoanClientId&client_secret=$algoanClientSecret");

        $response = curl_exec($ch);
        $err      = curl_error($ch);

        curl_close($ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $ret               = json_decode($response);
            $this->algoanToken = $ret->access_token;
        }
    }

    /**
     * Used to search for customers by user ID
     * 
     * @param string $customerId
     *
     * @return mixed|void
     */
    public function getCustomerInfo(string $customerId)
    {
        try {
            $userId = json_encode(['userId' => $customerId]);
            $url    = $this->parameterBag->get('algoan-url') . '/v2/customers?filter=' . $userId;

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $this->algoanToken"]);

            $response = curl_exec($ch);

            if (curl_error($ch)) {
                echo 'cURL Error #: ' . curl_error($ch);
            } else {
                $result = json_decode($response, true);

                return $result['resources'][0];
            }
        } catch (\Exception $e) {
        }
    }

    public function getAnalysis($id, $analysisId)
    {
        $this->generateAlgoanToken();
        
        try {
            $url = $this->parameterBag->get('algoan-url') . '/v2/customers/' . $id . '/analyses/' . $analysisId;

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $this->algoanToken"]);

            $response = curl_exec($ch);

            if (curl_error($ch)) {
                echo 'cURL Error #: ' . curl_error($ch);
            } else {
                $result = json_decode($response, true);

                return $result;
            }
        } catch (\Exception $e) {
        }
    }
}
