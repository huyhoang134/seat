<?php

namespace App\Services;

class EncryptionService
{
    private $encryption_iv = '243b736051eff250';
    private $ciphering     = "AES-128-CTR";

    public function encryption(int $customerApplicationId)
    {
        $ciphering = $this->ciphering;
        $options = 0;

        $encryption_iv = $this->encryption_iv;

        $simple_string = 'customerApplicationId-' . $customerApplicationId;

        $encryption_key = openssl_digest(php_uname(), 'MD5', TRUE);
        $encryption = openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);

        return $encryption;
    }

    public function decryption(string $encryption)
    {
        $decryption_key = openssl_digest(php_uname(), 'MD5', TRUE);
        $ciphering = $this->ciphering;
        $options = 0;
        $encryption_iv = $this->encryption_iv;
        $decryption = openssl_decrypt($encryption, $ciphering, $decryption_key, $options, $encryption_iv);

        if (strpos($decryption, "-")) {
            $decryptionApplicationId = explode('-', $decryption)[1];
        } else {
            $decryptionApplicationId = $decryption;
        }

        return $decryptionApplicationId;
    }
}