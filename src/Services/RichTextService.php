<?php 
namespace App\Services;

use PhpOffice\PhpSpreadsheet\IOFactory;
#use PhpOffice\PhpSpreadsheet\RichText;
use PhpOffice\PhpSpreadsheet\RichText\Run;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use Pelago\Emogrifier\HtmlProcessor\CssToAttributeConverter;

class RichTextService {

    /**
     * Takes array where of CSS properties / values and converts to CSS stri$
     *
     * @param array
     * @return string
     */
    private function assembleCSS($pValue = array())
    {
        $pairs = array();
        foreach ($pValue as $property => $value) {
            $pairs[] = $property . ':' . $value;
        }
        $string = implode('; ', $pairs);
        return $string;
    }

    /**
     * Create CSS style (Font)
     *
     * @param    Font        $pStyle            Font
     * @return    array
     */
    private function createCSSStyleFont(Font $pStyle)
    {
        // Construct CSS
        $css = array();
        // Create CSS
        if ($pStyle->getBold()) {
           // $css['font-weight'] = 'bold';
        }
        if ($pStyle->getUnderline() != Font::UNDERLINE_NONE && $pStyle->getStrikethrough()) {
            
            //$css['text-decoration'] = 'underline line-through';
        } elseif ($pStyle->getUnderline() != Font::UNDERLINE_NONE) {
            //$css['text-decoration'] = 'underline';
        } elseif ($pStyle->getStrikethrough()) {
            //$css['text-decoration'] = 'line-through';
        }
        if ($pStyle->getItalic()) {
            $css['font-style'] = 'italic';
        }
        $css['color']       = '#' . $pStyle->getColor()->getRGB();
        //$css['font-family'] = '\'' . $pStyle->getName() . '\'';
        //$css['font-size']   = $pStyle->getSize() . 'pt';
        return $css;
    }

    /**
     *
     * @param RichText|string $value
     * @return string
     */
    public function getHTML($value,$style) {
        
        if(is_string($value)) {
            return $value;
        }

        $cellData = '';
        if ($value instanceof RichText) {
            // Loop through rich text elements
            $elements = $value->getRichTextElements();
            foreach ($elements as $element) {
               
                // Rich text start?
                /*if ($element instanceof Run) {
                    
                    $cellData=$this->getElStyle($el);
                    
                    
                    
                    if($element->getFont()->getUnderline()=="single"){
                        $cellData .= '<ul>';
                    }
                    
                    if ($element->getFont()->getBold()) {
                        $cellData .= '<strong>';
                    }
                    if ($element->getFont()->getSuperScript()) {
                        $cellData .= '<sup>';
                    } elseif ($element->getFont()->getSubScript()) {
                        $cellData .= '<sub>';
                    }
                    $cellData .= '<span style="' . $this->assembleCSS($this->createCSSStyleFont($element->getFont())) . '">';
                }*/
                // Convert UTF8 data to PCDATA
                //$cellText = $element->getText();
                //$cellData .= htmlspecialchars($cellText);
                if ($element instanceof Run) {
                    $cellData.=$this->getElStyle($element,$style);
                   
                }else{
                    $data= htmlspecialchars($element->getText());
                    $cellData.=$this->getSumpleStyle($data,$style);
                }
                
                /*if ($element instanceof Run) {
                    $cellData .= '</span>';
                    if ($element->getFont()->getSuperScript()) {
                        $cellData .= '</sup>';
                    } elseif ($element->getFont()->getSubScript()) {
                        $cellData .= '</sub>';
                    }
                    if ($element->getFont()->getBold()) {
                        $cellData .= '</strong>';
                    }
                    if($element->getFont()->getUnderline()=="single"){
                        $cellData .= '</ul>';
                    }
                }*/
            }
        }

        return str_replace("\n", "<br>\n", $cellData);
        
        
    }
    
    public function getStyleHTML($cell,$style) {
        
        $value =  $cell->getValue();
        $css = "";
        if(is_string($value)) {
            if($style->getFont()->getColor()->getRGB()!="000000"){
                $css.="color: #".$style->getFont()->getColor()->getRGB().";";
            }
            
            if(!empty($css)){
                $value="<span style='".$css."'>".$value."</span>";
            }
            
            if($style->getFont()->getBold())
            {
                $value="<strong>".$value."</strong>";
            
            }
            
            if($style->getFont()->getUnderline()=="single" && $style->getFont()->getStrikethrough()){
                $value="<u>".$value."</u>";
                //$css.="text-decoration: underline overline;";
            }elseif($style->getFont()->getUnderline()=="single" ){
                // $css.="text-decoration: underline;";
                $value="<u>".$value."</u>";
            }elseif($style->getFont()->getStrikethrough()){
                //$css.="text-decoration: line-through;";
            }
            
            if($style->getFont()->getItalic()){
                //$css.="font-style: italic;";
                $value="<em>".$value."</em>";
            }
            
            
            return $value;

        }
        elseif($value instanceof RichText){
            return $this->getHTML($value,$style);
        }
        
        return $value;
    }
    

    public function getElStyle($element, $style)
    {
    
        $value="";
        $css="";
    
        if($element->getFont()->getColor()->getRGB()!="000000"){
            $css.="color: #".$element->getFont()->getColor()->getRGB().";";
        }
    
        
        $value="<span style='".$css."'>".htmlspecialchars($element->getText())."</span>";
        
        if($element->getFont()->getBold())
        {
            $value="<strong>".$value."</strong>";
        
        }elseif($style->getFont()->getBold())
        {
            $value="<strong>".$value."</strong>";
        }
        
        if($element->getFont()->getUnderline()=="single" && $element->getFont()->getStrikethrough()){
            $value="<u>".$value."</u>";
        }elseif($element->getFont()->getUnderline()=="single" ){
            $value="<u>".$value."</u>";
        }elseif($element->getFont()->getStrikethrough()){
        }
        
        if($element->getFont()->getItalic()){
            //$css.="font-style: italic;";
            $value="<em>".$value."</em>";
        }elseif($style->getFont()->getItalic())
        {
            $value="<em>".$value."</em>";
        }
        

        return $value;
    }
    
    public function getSumpleStyle($value,$style)
    {
        $css="";
        if($style->getFont()->getColor()->getRGB()!="000000"){
            $css.="color: #".$style->getFont()->getColor()->getRGB().";";
        }
        
        if(!empty($css)){
            $value="<span style='".$css."'>".$value."</span>";
        }
        
        if($style->getFont()->getBold())
        {
            $value="<strong>".$value."</strong>";
        
        }
        
        if($style->getFont()->getUnderline()=="single" && $style->getFont()->getStrikethrough()){
            $value="<u>".$value."</u>";
            //$css.="text-decoration: underline overline;";
        }elseif($style->getFont()->getUnderline()=="single" ){
            // $css.="text-decoration: underline;";
            $value="<u>".$value."</u>";
        }elseif($style->getFont()->getStrikethrough()){
            //$css.="text-decoration: line-through;";
        }
        
        if($style->getFont()->getItalic()){
            //$css.="font-style: italic;";
            $value="<em>".$value."</em>";
        }
        
        
        return $value;
    }
   
}
?>