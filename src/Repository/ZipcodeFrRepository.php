<?php

namespace App\Repository;

use App\Entity\ZipcodeFr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ZipcodeFr|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZipcodeFr|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZipcodeFr[]    findAll()
 * @method ZipcodeFr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZipcodeFrRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZipcodeFr::class);
    }
}
