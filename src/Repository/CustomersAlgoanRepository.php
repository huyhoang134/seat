<?php

namespace App\Repository;

use App\Entity\CustomersAlgoan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CustomersAlgoanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomersAlgoan::class);
    }

    public function findCustomersAlgoanForCheckAnalysis()
    {
        return $this->createQueryBuilder('ca')
                    ->where('ca.isCheckedAnalysis IS NULL OR ca.isCheckedAnalysis = :isCheckedAnalysis')
                    ->andWhere('ca.customerId IS NOT NULL')
                    ->andWhere('ca.algoanExtnalId IS NOT NULL')
                    ->setParameter('isCheckedAnalysis', false)
                    ->getQuery()
                    ->getResult();
    }

    public function findCustomersAlgoanInvalidRecords()
    {
        return $this->createQueryBuilder('ca')
                    ->where('ca.isCheckedAnalysis IS NULL OR ca.isCheckedAnalysis = :isCheckedAnalysis')
                    ->andWhere('(ca.customerId IS NULL OR ca.algoanExtnalId IS NULL)')
                    ->setParameter('isCheckedAnalysis', false)
                    ->getQuery()
                    ->getResult();
    }
}