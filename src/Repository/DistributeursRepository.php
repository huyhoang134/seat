<?php

namespace App\Repository;

use Psr\Container\ContainerInterface;
use App\Entity\Accessoires;
use App\Services\RichTextService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;

use PhpOffice\PhpSpreadsheet\IOFactory;

use App\Entity\Distributeurs;
use App\Entity\ImportLogs;
use App\Entity\Parametres;

/**
 * @method ModelesFinitions|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModelesFinitions|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModelesFinitions[]    findAll()
 * @method ModelesFinitions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistributeursRepository extends ServiceEntityRepository
{
    
    
    public function __construct(ManagerRegistry $registry, ContainerInterface $container)
    {
        $this->registry=$registry;
        $this->connection=$this->registry->getManager()->getConnection();
        $this->container = $container;
        parent::__construct($registry, Distributeurs::class);
    }

    public function findOneById($value)
    {
        return $this->createQueryBuilder('d')
        ->andWhere('d.id = :val')
        ->setParameter('val', $value)
        ->getQuery()
        ->getOneOrNullResult()
        ;
    }

    public function findDidtribByZipcodeFr($zipcode)
    {
        $lat           = $zipcode->getLatitude();
        $long          = $zipcode->getLongitude();
        $radius        = "1000";
        $entityManager = $this->container->get('doctrine')->getManager();
        $rsm           = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('kvps', 'kvps');
        $rsm->addScalarResult('raison_sociale', 'raison_sociale');
        $rsm->addScalarResult('adresse', 'adresse');
        $rsm->addScalarResult('code_postal', 'code_postal');
        $rsm->addScalarResult('ville', 'ville');
        $rsm->addScalarResult('telephone', 'telephone');
        $rsm->addScalarResult('fax', 'fax');
        $rsm->addScalarResult('email', 'email');
        $rsm->addScalarResult('website', 'website');
        $rsm->addScalarResult('occasion', 'occasion');
        $rsm->addScalarResult('service_center', 'service_center');
        $rsm->addScalarResult('distance_in_km', 'distance_in_km');
        $rsm->addScalarResult('test_drive', 'test_drive');
        $rsm->addScalarResult('latitude', 'latitude');
        $rsm->addScalarResult('longitude', 'longitude');

        $sql = "
            SELECT * ,
            ROUND( (6371 * acos( 
                cos( radians(" . $lat . ") ) 
              * cos( radians( latitude ) ) 
              * cos( radians( longitude ) - radians(" . $long . ") ) 
              + sin( radians(" . $lat . ") ) 
              * sin( radians( latitude ) )
                ) ), 1) as distance_in_km
                FROM distributeurs where code_postal > 0 AND is_reservation_available=1
                order by distance_in_km ASC
                LIMIT 4
                ";

        $query   = $entityManager->createNativeQuery($sql, $rsm);
        $results = $query->getResult();

        if ( ! empty($results)) {
            $data = [];
            foreach ($results as $d) {
                $data[] = [
                    'id'                  => $d['id'],
                    'kvps'                => $d['kvps'],
                    'name'                => 'SEAT ' . $d['ville'],
                    'label_info'          => ! empty($d['test_drive']) ? 'Essai disponible' : null,
                    'address'             => $d['adresse'] . ', ' . $d['code_postal'] . ', ' . $d['ville'],
                    'telephone'           => $d['telephone'],
                    'distrib_brand_name'  => 'SEAT',
                    'distrib_brand_color' => '#d96632',
                    'distance'            => ($d['distance_in_km'] + 0) . 'km',
                    'lat'                 => $d['latitude'],
                    'lng'                 => $d['longitude'],
                    'raison_sociale'      => $d['raison_sociale'],
                    'code_postal'         => $d['code_postal'],
                    'ville'               => $d['ville'],
                ];
            }

            return $data;

        }

        return [];
    }
    
    public function findDidtribByLongLat($long, $lat )
    {
    
        /*$lat=$zipcode->getLatitude();
        $long=$zipcode->getLongitude();
        $radius = "1000";*/
        $entityManager = $this->container->get('doctrine')->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('kvps', 'kvps');
        $rsm->addScalarResult('raison_sociale', 'raison_sociale');
        $rsm->addScalarResult('adresse', 'adresse');
        $rsm->addScalarResult('code_postal', 'code_postal');
        $rsm->addScalarResult('ville', 'ville');
        $rsm->addScalarResult('telephone', 'telephone');
        $rsm->addScalarResult('fax', 'fax');
        $rsm->addScalarResult('email', 'email');
        $rsm->addScalarResult('website', 'website');
        $rsm->addScalarResult('occasion', 'occasion');
        $rsm->addScalarResult('service_center', 'service_center');
        $rsm->addScalarResult('distance_in_km', 'distance_in_km');
        $rsm->addScalarResult('test_drive', 'test_drive');
    
        $sql="
            SELECT * ,
            ROUND( (6371 * acos(
                cos( radians(".$lat.") )
              * cos( radians( latitude ) )
              * cos( radians( longitude ) - radians(".$long.") )
              + sin( radians(".$lat.") )
              * sin( radians( latitude ) )
                ) ), 1) as distance_in_km
                FROM distributeurs where escooter=1 AND code_postal > 0
                order by distance_in_km ASC
                LIMIT 4
                ";
        $query = $entityManager->createNativeQuery($sql, $rsm);
        $results = $query->getResult();
        if(!empty($results)){
            $data=[];
            foreach($results as $d){
                $data[]=[
                    'id'=>$d['kvps'],
                    'name'=>'SEAT '.$d['ville'],
                    'label_info'=>!empty($d['test_drive'])?'Essais disponible':null,
                    'address'=>$d['adresse'].', '.$d['code_postal'].', '.$d['ville'],
                    'telephone'=>$d['telephone'],
                    'distrib_brand_name'=>'SEAT',
                    'distrib_brand_color'=>'#d96632',
                    'distance'=>($d['distance_in_km'] + 0 ).'km',
                ];
            }
            return $data;
             
        }
        return [];
    
    
    }
    public function importDistributeurs($data)
    {
        $log=[];
        
        foreach($data as $i=>$dist){
            $latitude = preg_replace('/[^\d\.-]/', '', $dist->latitude);
            $longitude = preg_replace('/[^\d\.-]/', '', $dist->longitude);

            if(empty($latitude)){
                $latitude = 0;
                $longitude = 0;
            }
            $distributeur=$this->findOneById($dist->id);
            if(!empty($distributeur)){
                $sql="
                    UPDATE `distributeurs` SET 
                        `kvps`=:kvps,
                        `kvps_de`=:kvps_de,
                        `code_ce`=:code_ce,
                        `district`=:district,
                        `occasion`=:occasion,
                        `distrib_type`=:distrib_type,
                        `service_center`=:service_center,
                        `raison_sociale`=:raison_sociale,
                        `adresse`=:adresse,
                        `code_postal`=:code_postal,
                        `ville`=:ville,
                        `telephone`=:telephone,
                        `fax`=:fax,
                        `telephone_abbr`=:telephone_abbr,
                        `fax_abbr`=:fax_abbr,
                        `latitude`=:latitude,
                        `longitude`=:longitude,
                        `email`=:email,
                        `website`=:website,
                        `website_seat`=:website_seat,
                        `seat_access`=:seat_access,
                        `cupra_access`=:cupra_access,
                        `created`=:created,
                        `modified`=:modified
                    WHERE `distributeurs`.`id` = :id;";
                $stmt = $this->connection->prepare($sql);
                $stmt->execute(array(
                    'id'=>$dist->id,
                    'kvps'=>$dist->kvps,
                    'kvps_de'=>$dist->kvps_de,
                    'code_ce'=>$dist->code_ce,
                    'district'=>$dist->district,
                    'occasion'=>empty($dist->occasion)?0:$dist->occasion,
                    'distrib_type'=>empty($dist->distrib_type)?0:$dist->distrib_type,
                    'service_center'=>$dist->service_center,
                    'raison_sociale'=>$dist->raison_sociale,
                    'adresse'=>$dist->adresse,
                    'code_postal'=>$dist->code_postal,
                    'ville'=>$dist->ville,
                    'telephone'=>$dist->telephone,
                    'fax'=>$dist->fax,
                    'telephone_abbr'=>$dist->telephone_abbr,
                    'fax_abbr'=>$dist->fax_abbr,
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                    'email'=>$dist->email,
                    'website'=>$dist->website,
                    'website_seat'=>$dist->website_seat,
                    'seat_access'=>$dist->seat_access,
                    'cupra_access'=>$dist->cupra_access,
                    'created'=>date("Y-m-d H:i:s"),
                    'modified'=>date("Y-m-d H:i:s"),
                ));
                
                $logs[]="Distributeur ".$dist->id." : ".$dist->raison_sociale." <span style='color: green;'>mis à jour</span>.";
            }else{
                $sql="INSERT INTO `distributeurs` (
                                `id`,
                                `kvps`,
                                `kvps_de`,
                                `code_ce`,
                                `district`,
                                `occasion`,
                                `distrib_type`,
                                `service_center`,
                                `raison_sociale`,
                                `adresse`,
                                `code_postal`,
                                `ville`,
                                `telephone`,
                                `fax`,
                                `telephone_abbr`,
                                `fax_abbr`,
                                `latitude`,
                                `longitude`,
                                `email`,
                                `website`,
                                `website_seat`,
                                `seat_access`,
                                `cupra_access`,
                                `created`,
                                `modified`
                                ) VALUES (
                                :id,
                                :kvps,
                                :kvps_de,
                                :code_ce,
                                :district,
                                :occasion,
                                :distrib_type,
                                :service_center,
                                :raison_sociale,
                                :adresse,
                                :code_postal,
                                :ville,
                                :telephone,
                                :fax,
                                :telephone_abbr,
                                :fax_abbr,
                                :latitude,
                                :longitude,
                                :email,
                                :website,
                                :website_seat,
                                :seat_access,
                                :cupra_access,
                                :created,
                                :modified
                                );";
                $stmt = $this->connection->prepare($sql);

                $stmt->execute(array(
                    'id'=>$dist->id,
                    'kvps'=>$dist->kvps,
                    'kvps_de'=>$dist->kvps_de,
                    'code_ce'=>$dist->code_ce,
                    'district'=>$dist->district,
                    'occasion'=>empty($dist->occasion)?0:$dist->occasion,
                    'distrib_type'=>empty($dist->distrib_type)?0:$dist->distrib_type,
                    'service_center'=>$dist->service_center,
                    'raison_sociale'=>$dist->raison_sociale,
                    'adresse'=>$dist->adresse,
                    'code_postal'=>$dist->code_postal,
                    'ville'=>$dist->ville,
                    'telephone'=>$dist->telephone,
                    'fax'=>$dist->fax,
                    'telephone_abbr'=>$dist->telephone_abbr,
                    'fax_abbr'=>$dist->fax_abbr,
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                    'email'=>$dist->email,
                    'website'=>$dist->website,
                    'website_seat'=>$dist->website_seat,
                    'seat_access'=>$dist->seat_access,
                    'cupra_access'=>$dist->cupra_access,
                    'created'=>date("Y-m-d H:i:s"),
                    'modified'=>date("Y-m-d H:i:s"),
                ));

                $current_id=$this->connection->lastInsertId();
                $logs[]="Distributeur ".$dist->id." : ".$dist->raison_sociale." <span style='color: red;'>créé</span>.";
            }
          

        
        }
        
        return $logs;
    }
    
    
    public function import_zipcode()
    {
        //https://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/export/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5
    
        $zipcodes=json_decode(file_get_contents('./import/laposte_hexasmal.json'));
        foreach($zipcodes as $z){
            $sql="INSERT INTO `zipcode_fr` (`id`, `code_commune_insee`, `nom_de_la_commune`, `code_postal`, `longitude`, `latitude`)
                VALUES
                (NULL, :code_commune_insee, :nom_de_la_commune, :code_postal, :longitude, :latitude);";
    
            $stmt = $this->connection->prepare($sql);
            if(!empty($z->fields->coordonnees_gps)){
                $stmt->execute(array(
                    'code_commune_insee'=>$z->fields->code_commune_insee,
                    'nom_de_la_commune'=>$z->fields->nom_de_la_commune,
                    'code_postal'=>$z->fields->code_postal,
                    'longitude'=>$z->fields->coordonnees_gps[1],
                    'latitude'=>$z->fields->coordonnees_gps[0],
                
                ));
            }

            
        }
        die();
    }

    public function findDidtribByKvps(array $kvps)
    {
        return $this->createQueryBuilder('d')
            ->where('d.kvps IN (:kvps)')
            ->setParameter('kvps', $kvps)
            ->orderBy('d.raisonSociale', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findDidtribByKvpsDe(array $kvpsDe)
    {
        return $this->createQueryBuilder('d')
            ->where('d.kvpsDe IN (:kvpsDe)')
            ->setParameter('kvpsDe', $kvpsDe)
            ->orderBy('d.raisonSociale', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
