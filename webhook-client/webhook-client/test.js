const fs = require('fs');
const https = require('https');
const express = require('express');
const axios = require('axios');

const app = express();
app.use(express.json());

// Charger le certificat de serveur (.cer, .key)
// Générer la clé privée du serveur: openssl genpkey -algorithm RSA -out server-key.pem
// Générer le certificat auto-signé du serveur: openssl req -new -x509 -key server-key.pem -out server-cert.cer -days 365
// Générer la clé privée du client: openssl genpkey -algorithm RSA -out client-key.pem
// Générer une demande de signature de certificat (CSR) pour le client: openssl req -new -key client-key.pem -out client.csr
// Générez le certificat client en utilisant la CSR et la clé privée du serveur: openssl x509 -req -in client.csr -CA server-cert.cer -CAkey server-key.pem -CAcreateserial -out client-cert.cer -days 365
// ensuite faire une requete avec curl :  curl -v -k --cert client-cert.cer --key client-key.pem -X POST https://localhost:4433/webhook -d "{\"id\":\"notificationId\",\"timestamp\":\"timestamp\",\"applicantId\":{\"organizationId\":\"organizationId\",\"domain\":\"applicant\",\"type\":\"applicant\",\"id\":\"applicant.applicantId\"},\"organizationId\":\"organizationId\"}" -H "Content-Type: application/json"
// vérifier sur le site : admin / 5%9NT9p@z
// https://e-scooter.ddb.clients.prod1.partitech.com/admin/app/webhooklogs/list

const serverOptions = {
    key: fs.readFileSync('server-key.pem'),
    cert: fs.readFileSync('server-cert.cer'),
    // Activer la vérification du certificat client
    requestCert: true,
    rejectUnauthorized: false,
    // Charger les certificats CA
    ca: [fs.readFileSync('client-cert.cer')],
};

// Middleware pour vérifier le certificat client
app.use((req, res, next) => {
    const cert = req.connection.getPeerCertificate();
    if (req.client.authorized) {
        return next();
    } else {
        if(serverOptions.rejectUnauthorized){
            res.status(401).send({error: 'Certificat client invalide'});
        }else{
            console.log('Certificat client invalide (auto signé ?)');
            console.log('serverOptions.rejectUnauthorized est désactivé. Le process continu.');
            return next();
        }

    }
});

// Route pour le webhook
app.post('/webhook', async (req, res) => {
    console.log('Headers:', req.headers);
    console.log('Body:', req.body);
    try {
        const targetServer = 'https://clients:partITech@e-scooter.ddb.clients.prod1.partitech.com/api/webhook'; // Le serveur cible (modifiable)
        const response = await axios.post(targetServer, req.body);
        res.status(response.status).send(response.data);
        console.log(response.data);
    } catch (error) {
        res.status(500).send({error: 'Erreur lors de la redirection vers le serveur cible'});
    }
});

// Créer le serveur HTTPS
const server = https.createServer(serverOptions, app);
server.listen(4433, () => {
    console.log('Server running at https://localhost:4433/');
});
