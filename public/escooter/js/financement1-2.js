$(document).ready(function () {
    let zipcode = ($("#zipcode").val().length === 0) ? $("#zipcode").attr('placeholder') : $("#zipcode").val();
    let dataAccessory = null;
    
    // getData for step 1-2
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var get_configurator_data = urlParams.get('data');
    if (get_configurator_data != null) {
        var configuratorData = JSON.parse(get_configurator_data);
    }

    var comment = 'Personne intéressé par un SEAT MÓ eScooter 125; ';
    let model = 'S0AAA3';
    let totalPrice = 0;

    if (typeof configuratorData != 'undefined' && configuratorData != null) {
        // show color
        $('.item__img').find('img').addClass('d-none');
        $('.color-circle').find('img').addClass('d-none');
        $('.color-circle').find('.text-description').addClass('d-none');
        $('.' + configuratorData.colorValue).removeClass('d-none');

        // model
        model = configuratorData.colorValue;

        // color price
        let cashPrice = Number(configuratorData.colorCashPrice);
        let rentPrice = Number(configuratorData.colorRentPrice);

        // show accessoires
        dataAccessory = configuratorData.dataAccessory;

        if (!$.isEmptyObject(dataAccessory)) {
            comment += 'Accessoires : ';

            $('#accessoires').removeClass('d-none');
            var accessoriesItem = $('#accessoires').find('.wrapper-accessoires');

            $.each(dataAccessory, function (key, data) {
                if (data.isFree == false) {
                    accessoriesItem.append('<div class=\'accessoires__item col-md-6\'>\n' +
                        '                                        <img src="' + data.configuratorImage + '" alt=\'item_images\'>\n' +
                        '                                        <div class=\'accessoires__item--info\'>\n' +
                        '                                            <p class=\'text-header\'>' + data.label + '</p>\n' +
                        '                                            <p class=\'text-price\'>' + data.price + '€</p>\n' +
                        '                                            <a href=\'#\' id=' + data.id + ' data-price=' + data.price + ' class=\'text-link\'>Supprimer</a>\n' +
                        '                                        </div>\n' +
                        '                                    </div>');
                    totalPrice += Number(data.price);

                    comment += '' + data.label + '/ ';
                }
            });

            $('#totalPrice').html(totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + "€");
        }

        if (configuratorData.financement === 'cash') {
            comment += 'Souhaite payer comptant; ';
            comment += 'À partir de ' + cashPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + '€ TTC*; ';
        }

        if (configuratorData.assuranceOui === 'oui') {
            comment += 'Souhaite une assurance; ';
        }

        if (configuratorData.financement === 'rent') {
            comment += 'Souhaite un loyer à ' + rentPrice + '€/mois sur 37 mois avec apport de <span class="var_loyer_majore">xxxx</span>€ et 5 000km annuel; ';
        }

        if (!$.isEmptyObject(dataAccessory)) {
            comment += 'Accessoires : ';

            $.each(dataAccessory, function (key, data) {
                if (data.isFree === true) {
                    comment += '' + data.label + '/ ';
                }
            });

            $.each(dataAccessory, function (key, data) {
                if (data.isFree == false) {
                    comment += '' + data.label + '/ ';
                }
            });
        }
    }

    $('.accessoires__item--info a').click(function (e) {
        
        const selectedItemId = $(this).attr('id');

        Object.entries(dataAccessory).forEach(([key, val]) => {
            if (selectedItemId === key) {
                delete dataAccessory[key]
            }
        });
        
        e.preventDefault();

        var item = $(this);
        var price = item.data('price');

        item.closest('.accessoires__item').remove();

        var numItems = $('.accessoires__item').length
        if (numItems === 1) {
            $('.accessoires__item').removeClass('col-md-6').addClass('col-md-12')
        }

        // setPrice
        totalPrice -= Number(price);
        $('#totalPrice').html(totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + "€");
    });

    // submit form
    $('#contactFormLead').submit(function (event) {
        var apiUrl = window.iFrameResizer

        var now = new Date();
        var dateStringWithTime = moment(now).format('YYYY/MM/DD HH:mm:ss');

        if (typeof dealer != 'undefined' && dealer != null) {
            selected_dealer = dealer;
        } else {
            selected_dealer = "";
        }

        /* model id : 
         * Rouge Audacieux S0AAA3
			Blanc Oxygène S0AAA1
			Gris Aluminium S0AAA5
         * */
        var formData = {
            action: 'lead',
            campaignid: 100515,
            model: model,
            civility: $("input[name='civilite']:checked").val(),
            last_name: $("#nom").val(),
            first_name: $("#prenom").val(),
            email: $("#email").val(),
            zip_code: zipcode,
            phone: $("#phone").val().replace(/\s/g, ''),
            privacy_email: $("input[name='privacy_email']:checked").val(),
            dealer: selected_dealer,
            comment: comment,
            created_at: dateStringWithTime,
        };

        var div = document.createElement("div");
        div.innerHTML = "<a style='position:absolute;left:0;transition: width 2s;top:" + 150 + "px' href='#'></a>";
        div = div.firstChild;
        document.body.appendChild(div);
        div.focus();
        div.parentNode.removeChild(div);

        if (typeof dealer == "undefined" || dealer == 'undefined' || dealer === null || (typeof dealer != "undefined" && dealer.length < 3)) {
            dealer = '';
            let url = apiUrl.targetApiPartiTech + '/get-distributors-by-adress/' + zipcode;

            $.ajax({
                crossOrigin: true,
                type: 'GET',
                url: url,
                success: function (response) {
                    var dealer = response.id;
                    formData.dealer = dealer;
                    $.ajax({
                        type: "GET",
                        url: apiUrl.targetApiLeadConnect,
                        data: formData,
                        success: function (response) {
                            Cookies.remove("cookie_configuratorData");
                            $('.item').hide(500);
                            $('.block__conditions-financement').hide(500);
                            $('#contactFormLead').hide(500);
                            $('#responseMessage').removeClass('d-none');
                        },
                        error: function (error) {
                        }
                    });
                },
                error: function (error) {
                }
            });
        } else {
            $.ajax({
                type: "GET",
                url: apiUrl.targetApiLeadConnect,
                data: formData,
                success: function (response) {
                    Cookies.remove("cookie_configuratorData");
                    $('.item').hide(500);
                    $('.block__conditions-financement').hide(500);
                    $('#contactFormLead').hide(500);
                    $('#responseMessage').removeClass('d-none');
                },
                error: function (error) {
                }
            });
        }

        event.preventDefault();
    });

    $('#gotoStep1-3').on('click', async function (e) {
        e.preventDefault();
        $('#contactForm')[0].reportValidity();

        const contactForm = $('#contactForm')[0];
        const isValid = contactForm.checkValidity();

        if (isValid) {
            var contactData = {
                model: model,
                civility: $("input[name='civilite']:checked").val(),
                last_name: $("#nom").val(),
                first_name: $("#prenom").val(),
                email: $("#email").val(),
                zip_code: zipcode,
                address: $("#address").val(),
                phone: $("#phone").val().replace(/\s/g, ''),
                privacy_email: $("input[name='privacy_email']:checked").val(),
                permit: $("input[name='permit']:checked").val(),
                dataAccessory: dataAccessory,
                totalPriceOption: totalPrice,
                gcm: configuratorData.gcm,
                vdr: configuratorData.vdr,
            };

            var apiUrl = window.iFrameResizer
            var customer_application = '';

            //ajax insert db
            await $.ajax({
                type: "POST",
                url: apiUrl.targetApiPartiTech + '/process-configurator-contact',
                data: contactData,
                success: function (response) {
                    customer_application = JSON.parse(response).customer_application
                },
                error: function (error) {
                }
            });

            window.location.href = $(this).attr('rel')+'?formData='+encodeURIComponent(JSON.stringify(contactData)) + '&customerApplication=' + customer_application;

            if ('parentIFrame' in window) {
                parentIFrame.scrollTo(0,0);
            }
        }
    });
});