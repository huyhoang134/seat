var dataColorPrice = [
    {
        "id": "S0AAA3",
        "label": "Rouge Audacieux",
        "cashPrice": window.iFrameResizer.S0AAA3.prix_catalogue,
        "rentPrice": window.iFrameResizer.S0AAA3.prix_a_partir_de,
    },
    {
        "id": "S0AAA1",
        "label": "Blanc Oxygène",
        "cashPrice": window.iFrameResizer.S0AAA1.prix_catalogue,
        "rentPrice": window.iFrameResizer.S0AAA1.prix_a_partir_de,
    },
    {
        "id": "S0AAA5",
        "label": "Gris Aluminium",
        "cashPrice": window.iFrameResizer.S0AAA5.prix_catalogue,
        "rentPrice": window.iFrameResizer.S0AAA5.prix_a_partir_de,
    },
    {
        "id": "S0AAA9",
        "label": "Bleu Tarifa",
        "cashPrice": window.iFrameResizer.S0AAA9.prix_catalogue,
        "rentPrice": window.iFrameResizer.S0AAA9.prix_a_partir_de,
    }

];


var dataOffersImage = [
			'images/SEAT_MO_040.png',
	        'images/SEAT_MO_091.png',
	        'images/SEAT_MO_068.png'
        ]

var dataAccessory = [
    {
        "id": "1L0050320E",
        "label": "Casque JET G262 HEBO",
        "description": "",
        "desktopImage": "images/jet helmet.jpg",
        "mobileImage": "images/jet helmet_mobile.jpg",
        "configuratorImage": "images/jet helmet_mobile.jpg",
        "price": "110",
        "isFree": false
    },
    {
        "id": "1L0052100",
        "label": "Smart light",
        "description": "",
        "desktopImage": "images/lumiere_arriere.png",
        "mobileImage": "images/lumiere_arriere.png",
        "configuratorImage": "images/detail/casque.png",
        "price": "90",
        "isFree": false
    },
    {
        "id": "1L0050320A",
        "label": "Casque Intégral G361 HEBO",
        "description": "",
        "desktopImage": "images/casque_integral.png",
        "mobileImage": "images/casque_integral.png",
        "configuratorImage": "images/detail/helmet.png",
        "price": "140",
        "isFree": false
    },
    {
        "id": "1L0071101",
        "label": "Support de malle",
        "description": "",
        "desktopImage": "images/trunksupport.jpg",
        "mobileImage": "images/trunksupport_mobile.jpg",
        "configuratorImage": "images/trunksupport_mobile.jpg",
        "price": "40",
        "isFree": false
    },
    {
        "id": "1L0061121",
        "label": "Malle 39 litres",
        "description": "",
        "desktopImage": "images/coffre_arriere.png",
        "mobileImage": "images/coffre_arriere.png",
        "configuratorImage": "images/detail/coffre.png",
        "price": "150",
        "isFree": false
    },
    {
        "id": "1L0084342E",
        "label": "Gants HEBO City Eté",
        "description": "",
        "desktopImage": "images/gants_d-ete.png",
        "mobileImage": "images/gants_d-ete_small.png",
        "configuratorImage": "images/detail/hebo.png",
        "price": "35",
        "isFree": false
    },
    {
        "id": "1L0071267A",
        "label": "Antivol pour scooter",
        "description": "",
        "desktopImage": "images/antivol.png",
        "mobileImage": "images/antivol.png",
        "configuratorImage": "images/detail/antivo.png",
        "price": "30",
        "isFree": false
    },
    {
        "id": "1L0051435",
        "label": "Support Smartphone",
        "description": "",
        "desktopImage": "images/support_pour_smartphone.png",
        "mobileImage": "images/support_pour_smartphone_small.png",
        "configuratorImage": "images/detail/support_smartphone.png",
        "price": "60",
        "isFree": false
    },
    /*{
      "id": "1L0084503",
      "label": "Tablier",
      "description": "",
      "desktopImage": "images/blanket.jpg",
      "mobileImage": "images/blanket_mobile.jpg",
      "configuratorImage": "images/blanket_mobile.jpg",
      "price": "130",
      "isFree": false
    },*/
    {
        "id": "1L0064160",
        "label": "Pare-brise haute protection",
        "description": "",
        "desktopImage": "images/bulle.jpg",
        "mobileImage": "images/bulle_mobile.jpg",
        "configuratorImage": "images/bulle_mobile.jpg",
        "price": "210",
        "isFree": false
    },
]

        //Cookies.set("cookie_dataAccessory", JSON.stringify(dataAccessory));
        //CookieSet("cookie_dataAccessory", JSON.stringify(dataAccessory), 365);
        


