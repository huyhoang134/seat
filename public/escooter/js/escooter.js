
function handleMessage(evt)
{
	try{
		  parentIFrame.getPageInfo(
				    (pageInfo) => sendPageInfo(pageInfo)
				  );
	}catch(e){}
}
function sendPageInfo(pageInfo){
	try{

		//_letScroll(pageInfo);
		_scrollZoom(pageInfo);
		_letScrollStickyHeader(pageInfo);
		_openModal(pageInfo);
	}catch(e){}
}
if (window.addEventListener) {
	window.addEventListener("message", handleMessage, false);
}else {
	window.attachEvent("onmessage", handleMessage);
}

function _changePage(){
    $('.gotoPage').click(function (e){
        //Cookies.set("cookie_dealer_radio_value", $("input[name='dealer_radio']:checked").val());
        //CookieSet("cookie_dealer_radio_value", $("input[name='dealer_radio']:checked").val(), 365)
    	var dealer = $("input[name='dealer_radio']:checked").val();
    	if (typeof(dealer)=='undefined') {
    		dealer="";
    	}

        let zipcodeValue = $('#zipcode').val();
        if (!zipcodeValue) {
            zipcodeValue = $('#zipcode').attr('placeholder');
        }

        e.preventDefault();
    	window.location.href=$(this).attr('rel')+'?zipcode='+zipcodeValue+'&dealer='+dealer;
    	if ('parentIFrame' in window) {
  		  parentIFrame.scrollTo(0,0);
  		}
    })
}




_changePage();

function _openModal(event) {
    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    }
}

$('.btn-show').click(function () {
    var item = $(this);

    if (item.length > 0) {
        var content = item.closest('.description');

        if (content.length > 0 && content.hasClass('active')) {
            content.removeClass('active');
            item.text("Lire les mentions légales");
        } else {
            content.addClass('active');
            item.text("Masquer les mentions légales");
        }
    }
})

function _letScroll(event) {

    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    //Setting chat button
    $('#btn-chat').css({
        position: "absolute",
        top: docViewBottom - 180,
    })
}

function _scrollZoom(event) {
    var descriptionBlock = $('.block-description .header');
    _animation(descriptionBlock, event);

    var galleryItemSecond = $('.block-description .gallery-item-second');
    //_animation(galleryItemSecond, event);

    var aluminumBlack = $('.aluminum-black-banner .aluminum-black-scooter');
    _animation(aluminumBlack, event);

    var imageBannerRights = $('.banner-right .image-banner');
    _animation(imageBannerRights, event);

    var bannerLeft = $('.banner-left .banner__image');
    _animation(bannerLeft, event);
}

function _animation(element, event) {
    var screenHeight = $('header').height();
    const viewportHeight = element.height();
    const parent = element[0];

    var scrollY = event.scrollTop;
    const elPosY = parent.getBoundingClientRect().top;


    if (elPosY  + viewportHeight < scrollY + 100) {
        element.removeClass('aos-animate');
    } else if ( elPosY > scrollY + screenHeight - screenHeight/ 3) {
        element.removeClass('aos-animate');
    } else {
        element.addClass('aos-animate');
    }
}


function _letScrollStickyHeader(event) {
    //var parentWindow = event.currentTarget;

    var docViewTop = event.scrollTop ;
    var docViewBottom = docViewTop + event.clientHeight;

    //Setting sticky header

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.section-sticky-header').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    } else {
        $('.section-sticky-header').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    }

    $('.section-sticky-header').css('z-index', 999);
}

function letScroll(event) {
    var parentWindow = event.currentTarget;
    var docViewTop = $(parentWindow).scrollTop();
    var docViewBottom = docViewTop + $(parentWindow).height();

    //Setting chat button
    $('#btn-chat').css({
        position: "absolute",
        top: docViewBottom - 180,
    })
}

function scrollZoom(event) {
    var descriptionBlock = $('.block-description .header');
    animation(descriptionBlock, event);

    var galleryItemSecond = $('.block-description .gallery-item-second');
    animation(galleryItemSecond, event);

    var aluminumBlack = $('.aluminum-black-banner .aluminum-black-scooter');
    animation(aluminumBlack, event);

    var imageBannerRights = $('.banner-right .image-banner');
    animation(imageBannerRights, event);

    var bannerLeft = $('.banner-left .banner__image');
    animation(bannerLeft, event);
}

function animation(element, event) {
	return;
    var screenHeight = $('header').height();
    const viewportHeight = element.height();
    const parent = element[0];
    var parentWindow = event.currentTarget;
    var scrollY = $(parentWindow).scrollTop();
    const elPosY = parent.getBoundingClientRect().top;

    if (elPosY  + viewportHeight < scrollY + 100) {
        element.removeClass('aos-animate');
    } else if ( elPosY > scrollY + screenHeight - screenHeight/ 3) {
        element.removeClass('aos-animate');
    } else {
        element.addClass('aos-animate');
    }
}

// function letScrollStickyHeader(event) {
//     var parentWindow = event.currentTarget;

//     var docViewTop = $(parentWindow).scrollTop();
//     var docViewBottom = docViewTop + $(parentWindow).height();

//     //Setting sticky header

//     if (window.matchMedia('(min-width: 767px)').matches) {
//         $('.section-sticky-header').css({
//             position: "absolute",
//             top: docViewTop - 75,
//         });
//     } else {
//         $('.section-sticky-header').css({
//             position: "absolute",
//             top: docViewTop - 5,
//         });
//     }

//     $('.section-sticky-header').css('z-index', 999);
// }

function changePage(){
    $('.gotoPage').click(function (e){
        //Cookies.set("cookie_dealer_radio_value", $("input[name='dealer_radio']:checked").val());
        //CookieSet("cookie_dealer_radio_value", $("input[name='dealer_radio']:checked").val(), 365)

        e.preventDefault();
        parent.window.changeIframeSrc($(this).attr('rel')+'?dealer='+$("input[name='dealer_radio']:checked").val());
    })
}

//changePage();

function onMouseAccessory() {
    let isDown = false;
    let startX;
    let scrollLeft;
    $('.accessory-content').on('mousedown', function (e) {
        isDown = true;
        startX = e.pageX - this.offsetLeft;
        scrollLeft = this.scrollLeft;

    });
    $('.accessory-content').on('mouseleave', function () {
        isDown = false;
    });
    $('.accessory-content').on('mouseup', function () {
        isDown = false;
    });
    $('.accessory-content').on('mousemove', function (e, delta) {
        if (!isDown) {
            return;
        }
        const x = e.pageX;
        const walk = (x - startX);
        this.scrollLeft = scrollLeft - walk + 144;
        e.preventDefault();
    });
}

onMouseAccessory();

function openModal(event) {

    var parentWindow = event.currentTarget;

    var docViewTop = $(parentWindow).scrollTop();
    var docViewBottom = docViewTop + $(parentWindow).height();

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });
    }
}

let currentPageInfo = null;
$(document).ready(function () {


    // function loadData() {
    //
	//	
	// 	var dataAccessoryFirst = dataAccessory.slice(0, Number((dataAccessory.length /2).toFixed(0)))
	// 	var dataAccessorySecond = dataAccessory.slice(Number((dataAccessory.length /2).toFixed(0)), dataAccessory.length)
	//	
	// 	var accessoryFirst = $('.accessory-first');
	// 	var accessorySecond = $('.accessory-second');
	// 	var accessoryMobile = $('.accessory-mobile');
	// 	dataAccessoryFirst.forEach(function (data) {
	// 	    accessoryFirst.append('<div class="mb-4 pe-4 accessory-item">\n' +
	// 	        '                            <img class="" src="' + data.desktopImage + '" alt="'+ data.label+'"/>\n' +
	// 	        '                            <div class="accessory-text py-4 px-4">\n' +
	// 	        '                                <div class="title">' + data.label + '</div>\n' +
	// 	        '                                <div class="sub-title">' + data.description + '</div>\n' +
	// 	        '                            </div>\n' +
	// 	        '                        </div>');
	// 	})
	//	
	// 	dataAccessorySecond.forEach(function (data) {
	// 	    accessorySecond.append('<div class="mb-4 pe-4 accessory-item">\n' +
	// 	        '                            <img class="" src="' + data.desktopImage + '" alt="'+ data.label+'"/>\n' +
	// 	        '                            <div class="accessory-text py-4 px-4">\n' +
	// 	        '                                <div class="title">' + data.label + '</div>\n' +
	// 	        '                                <div class="sub-title">' + data.description + '</div>\n' +
	// 	        '                            </div>\n' +
	// 	        '                        </div>');
	// 	});
	//	
	// 	dataAccessory.forEach(function (data) {
	// 	    accessoryMobile.append('<div class="mb-4 pe-4 accessory-item">\n' +
	// 	        '                            <img class="" src="' + data.mobileImage + '" alt="'+ data.label+'"/>\n' +
	// 	        '                            <div class="accessory-text py-4 px-4">\n' +
	// 	        '                                <div class="title">' + data.label + '</div>\n' +
	// 	        '                                <div class="sub-title">' + data.description + '</div>\n' +
	// 	        '                            </div>\n' +
	// 	        '                        </div>');
	// 	})
    //
    //     $(dataOffersImage).each(function(index, url){
    //      		if (index > 0){
    //                  $('.gallery-item-second').append('<div class="gallery-item mb-3" style="background-image: url('+url+')">\n' +
    //                      '   <img src="'+url+'" style="opacity: 0;">\n' +
    //                      '</div>')
    //              }else{
    //              	var  itemFirst = $('.gallery-item-first');
    //                  itemFirst.css('background-image', 'url("' + url +'")');
    //                  itemFirst.append('<img src="'+url+'" style="opacity: 0;">');
    //
    //              }
    //      	});
    //
    //
    //
    //
    // }
    //
    // loadData();

    function getDistributors() {
        $(' #zipcode').focus(function () {
            $(this).data('placeholder', $(this).attr('placeholder'))
                .attr('placeholder', '');
        }).blur(function () {
            $(this).attr('placeholder', $(this).data('placeholder'));
        });
        $('#zipcode').keyup(function () {
            var blockContent = $('.alert-error').addClass('d-none');
            var apiUrl = window.iFrameResizer

            if (typeof Timer !== 'undefined'){
                clearTimeout(Timer);
            }
            Timer = setTimeout(()=>{
                let zipcode = $(this).val();
                let url = apiUrl.targetApiPartiTech + '/get-distributors/' + zipcode;

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        var blockContent = $('.block-location-content').removeClass('d-none');
                        let list =  $('.block-location-list-item')
                        list.empty();

                        response.forEach((item) => {
                            list.append('<div class="px-3 py-3 content-item my-2 mx-lg-2 d-lg-flex" type="button">\n' +
                                '                        <div class="d-none d-lg-flex col-lg-1 mt-2">\n' +
                                '                            <input class="form-check-input" name="dealer_radio" id="dealer-'+ item.id +'" value="'+ item.id +'" type="radio" >\n' +
                                '                        </div>\n' +
                                '                        <label class="d-block col-lg-11" for="dealer-'+ item.id +'">\n' +
                                '                            <div class="d-flex justify-content-between">\n' +
                                '                                <div class="d-lg-flex ">\n' +
                                '                                    <div class="title">'+ item.name +'</div>\n' +
                                '                                    <div class="cl-red ps-lg-2">'+ (item.label_info ? item.label_info: '') +'</div>\n' +
                                '                                </div>\n' +
                                '                                <div class="d-flex align-items-start">\n' +
                                '                                    <div class="text-white px-2 box-seat" style="background:'+item.distrib_brand_color +'">'+ item.distrib_brand_name +'</div>\n' +
                                '                                    <div class="bg-dark text-white ms-2 px-2 d-flex box-location">\n' +
                                '                                        <img class="me-1" src="images/Icons/location-icon.svg"/>\n' +
                                '                                        '+ item.distance +'\n' +
                                '                                    </div>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                            <div class="pt-3 pt-lg-1 location">\n' +
                                '                                <div>' + item.address + '</div>\n' +
                                '                                <div class="d-flex pt-lg-1">\n' +
                                '                                    <img class="me-2" src="images/Icons/private-call.svg"/>\n' +
                                '                                    <div>' + item.telephone.toString().replace(/\B(?=(\d{2})+(?!\d))/g, " ") + '</div>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </label>\n' +
                                '\n' +
                                '                    </div>')
                        });
                    },
                    error: function (error) {
                        var blockContent = $('.alert-error').removeClass('d-none');
                    }
                });
            }, 300);
        });

    }

    getDistributors();

    // keep checking for parentIFrame until it's available
    function checkParentIFrame() {
        if (window.parentIFrame == undefined) {
            setTimeout(checkParentIFrame, 50);
        } else {
            window.parentIFrame.getPageInfo(updateLayoutWithPageInfo);
        }

    }
    setTimeout(checkParentIFrame, 50);

    // update content with width/height from parent page
    function updateLayoutWithPageInfo(pageInfo) {
        if (currentPageInfo == null || pageInfo.clientHeight != currentPageInfo.clientHeight) {
            $('header').css('min-height', pageInfo.clientHeight);
        }
        currentPageInfo = pageInfo;
    }


    // faq
    $('.faq-header-arrow').click(function () {
        var item = $(this);

        if (item.length > 0) {
            var content = item.find('.content');
            if (content.length > 0 && content.hasClass('d-none')) {
                content[0].classList.remove('d-none');
                $(this).find('.faq-icon').css('transform','rotate( -180deg )');
            } else {
                content[0].classList.add('d-none');
                $(this).find('.faq-icon').css('transform','rotate( -0deg )');
            }
        }
    })
});

const video = document.getElementById('video');
const videoControls = document.getElementById('video-controls');
const playButton = document.getElementById('play');
const playStartButton = document.getElementById('btn-start');
const playbackIcons = document.querySelectorAll('.playback-icons use');
const timeElapsed = document.getElementById('time-elapsed');
const duration = document.getElementById('duration');

const progressBar = document.getElementById('progress-bar');
const seek = document.getElementById('seek');


const volumeButton = document.getElementById('volume-button');
const volumeIcons = document.querySelectorAll('.volume-button use');
const volumeMute = document.querySelector('use[href="#volume-mute"]');
const volumeLow = document.querySelector('use[href="#volume-low"]');
const volumeHigh = document.querySelector('use[href="#volume-high"]');
const volume = document.getElementById('volume');

const videoWorks = !!document.createElement('video').canPlayType;
if (videoWorks) {
    video.controls = false;
}

function togglePlay() {

    if (video.paused || video.ended) {
        video.play();
    } else {
        video.pause();
    }
}

function togglePlayStart() {
    videoControls.classList.remove('hidden');
    videoControls.classList.add('d-flex');
    playStartButton.classList.add('hidden');
    playStartButton.classList.remove('d-flex');
    if (video.paused || video.ended) {
        video.play();
    } else {
        video.pause();
    }
}

function updatePlayButton() {
    playbackIcons.forEach(icon => icon.classList.toggle('hidden'));

    if (video.paused) {
        playButton.setAttribute('data-title', 'Play (k)')
    } else {
        playButton.setAttribute('data-title', 'Pause (k)')
    }
}

function formatTime(timeInSeconds) {
    const result = new Date(timeInSeconds * 1000).toISOString().substr(11, 8);

    return {
        minutes: result.substr(3, 2),
        seconds: result.substr(6, 2),
    };
};

function initializeVideo() {
    const videoDuration = Math.round(video.duration);
    seek.setAttribute('max', videoDuration);
    progressBar.setAttribute('max', videoDuration);
}

function updateTimeElapsed() {
    const time = formatTime(Math.round(video.currentTime));
    timeElapsed.innerText = `${time.minutes}:${time.seconds}`;
    timeElapsed.setAttribute('datetime', `${time.minutes}m ${time.seconds}s`)
}

function updateProgress() {
    seek.value = Math.floor(video.currentTime);
    progressBar.value = Math.floor(video.currentTime);
}


function skipAhead(event) {
    const skipTo = event.target.dataset.seek ? event.target.dataset.seek : event.target.value;
    video.currentTime = skipTo;
    progressBar.value = skipTo;
    seek.value = skipTo;
}
function updateVolume() {
    if (video.muted) {
        video.muted = false;
    }

    video.volume = volume.value;
}
function updateVolumeIcon() {
    volumeIcons.forEach(icon => {
        icon.classList.add('hidden');
    });

    volumeButton.setAttribute('data-title', 'Mute (m)')

    if (video.muted || video.volume === 0) {
        volumeMute.classList.remove('hidden');
        volumeButton.setAttribute('data-title', 'Unmute (m)')
    } else if (video.volume > 0 && video.volume <= 0.5) {
        volumeLow.classList.remove('hidden');
    } else {
        volumeHigh.classList.remove('hidden');
    }
}
function toggleMute() {
    video.muted = !video.muted;
}

playButton.addEventListener('click', togglePlay);
playStartButton.addEventListener('click', togglePlayStart);

video.addEventListener('play', updatePlayButton);
video.addEventListener('pause', updatePlayButton);
video.addEventListener('loadedmetadata', initializeVideo);
video.addEventListener('timeupdate', updateTimeElapsed);
video.addEventListener('timeupdate', updateProgress);
seek.addEventListener('input', skipAhead);
// volume.addEventListener('input', updateVolume);
video.addEventListener('volumechange', updateVolumeIcon);
volumeButton.addEventListener('click', toggleMute);
video.addEventListener('click', togglePlay);


