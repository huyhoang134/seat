$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var formDataFromUrl = urlParams.get('formData');
    var customerApplication = urlParams.get('customerApplication')

    if(formDataFromUrl!=null){
        var formData = JSON.parse(formDataFromUrl);
        console.log(formData)
        $("input[name=civilite][value=" + formData.civility + "]").prop('checked', true);
        $('#nom').val(formData.last_name);
        $('#prenom').val(formData.first_name);
        $('#address').val(formData.address);
        $('#country').val('France');
        $('#email').val(formData.email);
        $('#code-postal').val(formData.code_postal);
        $('#phone').val(formData.phone);
        $('#maiden-name').val(formData.maiden_name);
        $('#date-of-birth').val(formData.date_of_birth);
        $('#city-of-birth').val(formData.city_of_birth);
        $('#postal-code').val(formData.code_postal_naissance);
        $('#additional-address').val(formData.additional_address);
        $('#city').val(formData.city);
        $('#nationality').val(formData.nationality);
        $('#principal-residence').val(formData.principal_residence);
        $('#family-status').val(formData.family_status);
        $('#number-of-dependent-children').val(formData.number_of_dependent_children);
        $('#occupation').val(formData.occupation);
        $('#hiring-date').val(formData.hiring_date);
        $('#type-of-contract').val(formData.type_of_contract);
    }

    // Modifier Form
    const formGroup = [
        '.form-group-information',
        '.form-group-to-know',
        '.form-group-situation-familiale',
        '.form-group-professional-situation',
        '.form-group-contact',
    ];

    formGroup.map((item) => {
        $(item).find('.text-modifier').on('click', function () {
            $(item).find('input').removeAttr('disabled');
            $(item).find('input').first().focus();
            if (item === '.form-group-information') {
                $(item).find('input#nom').focus();
            }
        });
    });

    $('#btn-confirm').on('click', function (e) {
        e.preventDefault();

        $('#formConfirm')[0].reportValidity();

        const formConfirm = $('#formConfirm')[0];
        const isValid = formConfirm.checkValidity();

        if (isValid) {
            var informationData = {
                id: customerApplication,
                civility: $("input[name='civilite']:checked").val(),
                last_name: $("#nom").val(),
                first_name: $("#prenom").val(),
                maiden_name: $("#maiden-name").val(),
                date_of_birth: $("#date-of-birth").val(),
                city_of_birth: $("#city-of-birth").val(),
                code_postal_naissance: $("#postal-code").val(),
                address: $("#address").val(),
                additional_address: $("#additional-address").val(),
                code_postal: $("#code-postal").val(),
                city: $("#city").val(),
                country: $("#country").val(),
                nationality: $("#nationality").val(),
                principal_residence: $( "#principal-residence option:selected" ).text(),
                family_status: $("#family-status").val(),
                number_of_dependent_children: $("#number-of-dependent-children").val(),
                occupation: $("#occupation").val(),
                type_of_contract: $( "#type-of-contract option:selected" ).text(),
                hiring_date: $("#hiring-date").val(),
                email: $("#email").val(),
                phone: $("#phone").val().replace(/\s/g, ''),
            };

            var apiUrl = window.iFrameResizer

            $.ajax({
                type: "POST",
                url: apiUrl.targetApiPartiTech + '/confirm-information',
                data: informationData,
                success: function (response) {
                },
                error: function (error) {
                }
            })
        }
    });
});