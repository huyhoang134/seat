let textDate = '<span class="text-black"> au </span>';

window.iFrameResizer = {
    //targetOrigin: 'https://www.e-scooter-configurateur.test',
    targetApiPartiTech: '/api',
    targetApiLeadConnect: 'https://dev-leadconnect.seat.fr/webservices/leads',
    S0AAA1 : {
		loa_duree: 39,
		loa_loyers: 38,
    	prix_catalogue: 6775,
    	prix_a_partir_de: 989,
        montant_total_du: '7.,89',
        option_achat: '3.,93',
        loyer_majore: 908,
        date_offre: 'xx/xx/2020' + textDate + 'xx/xx/2023',
        mention_legale: "<p>ZZZZZ SEAT MÓ eScooter 125 avec peinture Rouge Audacieux à 98 € / mois (1)Véhicule de remplacement inclus (2)LOA sur 37 mois et 15.000 Km.1er loyer majoré de 900€ ramené à 0€ après déduction du bonus écologique de 900€*, suivi de 36 loyers de 98€.Montant total dû : 7807,89€. Offre valable du 01/04/2022 au 30/06/2022.	Un crédit vous engage et doit être remboursé. Vérifiez vos capacités de remboursement avant de vous engager.</p><p>(1) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 6775€. Option d’achat finale : 3439,93 € ou reprise du scooter sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2022 pour toute commande d’une SEAT MÓ eScooter 125 passée avant le 31/05/2022 et livrée avant le 31/10/2022.<br>* Offre valable sous réserve de bénéficier du Bonus écologique (conditions sur servicepublic.fr)Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours.</p><p>Assurance facultative Décès Incapacité Perte d’Emploi : 9,82€ / mois en sus de la mensualité. Contrat souscrit auprès de Cardif Assurance Vie SA au capital de 688 507 760€, N°732 028 154 RCS Paris et Cardif Assurances Risques Divers SA au capital de 14 784 000€, N°308 896 547 RCS Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré.</p><p>SEAT MÓ eScooter 125: Autonomie électrique WLTP: 133 km. Consommation mixte WLTP (min-max kWh/100km) : 0,07kWh/100km. Émissions de CO2 WLTP (min – max g/km) : 0 (en phase de roulage)<p>Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée."

    },
    S0AAA3 : {
		loa_duree: 40,
		loa_loyers: 39,
    	prix_catalogue: 6777,
    	prix_a_partir_de: 96,
        montant_total_du: '7.888,89',
        option_achat: '3.439,93',
        loyer_majore: 908,
        date_offre: 'xx/xx/xxxx' + textDate + 'xx/xx/xxxx',
        mention_legale: "<p>SEAT MÓ eScooter 125 avec peinture Rouge Audacieux à 98 € / mois (1)Véhicule de remplacement inclus (2)LOA sur 37 mois et 15.000 Km.1er loyer majoré de 900€ ramené à 0€ après déduction du bonus écologique de 900€*, suivi de 36 loyers de 98€.Montant total dû : 7807,89€. Offre valable du 01/04/2022 au 30/06/2022.	Un crédit vous engage et doit être remboursé. Vérifiez vos capacités de remboursement avant de vous engager.</p><p>(1) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 6775€. Option d’achat finale : 3439,93 € ou reprise du scooter sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2022 pour toute commande d’une SEAT MÓ eScooter 125 passée avant le 31/05/2022 et livrée avant le 31/10/2022.<br>* Offre valable sous réserve de bénéficier du Bonus écologique (conditions sur servicepublic.fr)Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours.</p><p>Assurance facultative Décès Incapacité Perte d’Emploi : 9,82€ / mois en sus de la mensualité. Contrat souscrit auprès de Cardif Assurance Vie SA au capital de 688 507 760€, N°732 028 154 RCS Paris et Cardif Assurances Risques Divers SA au capital de 14 784 000€, N°308 896 547 RCS Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré.</p><p>SEAT MÓ eScooter 125: Autonomie électrique WLTP: 133 km. Consommation mixte WLTP (min-max kWh/100km) : 0,07kWh/100km. Émissions de CO2 WLTP (min – max g/km) : 0 (en phase de roulage)<p>Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée."
    },
    S0AAA5 : {
		loa_duree: 42,
		loa_loyers: 41,
    	prix_catalogue: 678,
    	prix_a_partir_de: 92,
        montant_total_du: '7.999,89',
        option_achat: '3.439,93',
        loyer_majore: 909,
        date_offre: 'xx/xx/xxxx' + textDate + 'xx/xx/xxxx',
        mention_legale: "<p>SEAT MÓ eScooter 125 avec peinture Rouge Audacieux à 98 € / mois (1)Véhicule de remplacement inclus (2)LOA sur 37 mois et 15.000 Km.1er loyer majoré de 900€ ramené à 0€ après déduction du bonus écologique de 900€*, suivi de 36 loyers de 98€.Montant total dû : 7807,89€. Offre valable du 01/04/2022 au 30/06/2022.	Un crédit vous engage et doit être remboursé. Vérifiez vos capacités de remboursement avant de vous engager.</p><p>(1) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 6775€. Option d’achat finale : 3439,93 € ou reprise du scooter sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2022 pour toute commande d’une SEAT MÓ eScooter 125 passée avant le 31/05/2022 et livrée avant le 31/10/2022.<br>* Offre valable sous réserve de bénéficier du Bonus écologique (conditions sur servicepublic.fr)Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours.</p><p>Assurance facultative Décès Incapacité Perte d’Emploi : 9,82€ / mois en sus de la mensualité. Contrat souscrit auprès de Cardif Assurance Vie SA au capital de 688 507 760€, N°732 028 154 RCS Paris et Cardif Assurances Risques Divers SA au capital de 14 784 000€, N°308 896 547 RCS Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré.</p><p>SEAT MÓ eScooter 125: Autonomie électrique WLTP: 133 km. Consommation mixte WLTP (min-max kWh/100km) : 0,07kWh/100km. Émissions de CO2 WLTP (min – max g/km) : 0 (en phase de roulage)<p>Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée."

    },
    

    model_defaut: 'S0AAA1'
}

var pricing=false;

document.addEventListener("DOMContentLoaded", function(event) {

	//load default color in configurator page
	 if($('.product-detail .color li.active').length >0 &&  $('.product-detail .color li.active').attr('value').length){
		 var current_model = $('.product-detail .color li.active').attr('value')
		 
		 //Reload prince on change color in configurator page.
		 $('.product-detail .color li').click(function() {
			 
			 var current_model = $('.product-detail .color li.active').attr('value');
			 eval('pricing=window.iFrameResizer.'+current_model);
			 console.log('pricing=window.iFrameResizer.'+current_model);
			 load_vars();
		 });
		 
	  }else if($('.item__profile  .item__img img:visible').length >0 &&  $('.item__profile  .item__img img:visible').attr('value').length){	 
		  
		  var current_model = $('.item__profile  .item__img img:visible').attr('value');
		 
	 }else{
		 //load default color on index page.
		 var current_model = window.iFrameResizer.model_defaut
	 }
	 
	 eval('pricing=window.iFrameResizer.'+current_model);
	 
	 load_vars();
});

function load_vars(){
	$('.var_prix_catalogue').html(pricing.prix_catalogue);
	$('.var_prix_a_partir_de').html(pricing.prix_a_partir_de);
	$('.var_montant_total_du').html(pricing.montant_total_du);
	$('.var_option_achat').html(pricing.option_achat);
	$('.var_loyer_majore').html(pricing.loyer_majore);
	$('.var_date_offre').html(pricing.date_offre);
	$('.var_mention_legale').html(pricing.mention_legale);
	$('.var_loa_duree').html(pricing.loa_duree);
	$('.var_loa_loyers').html(pricing.loa_loyers);

}

function refresh_index_pricing()
{
	setTimeout(() => {
		var current_model = $('.owl-item.active.center .produit').data('value');
		eval('pricing=window.iFrameResizer.'+current_model);
		load_vars();
	}, "500")



}



