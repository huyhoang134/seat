$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var formDataFromUrl = urlParams.get('formData');
    var customerApplication = urlParams.get('customerApplication')

    if(formDataFromUrl!=null){
        var formData = JSON.parse(formDataFromUrl);
        console.log(formData)
        $("input[name=civilite][value=" + formData.civility + "]").prop('checked', true);
        $('#nom').val(formData.last_name);
        $('#prenom').val(formData.first_name);
        $('#address').val(formData.address);
        $('#country').val('France');
        $('#email').val(formData.email);
        $('#code-postal').val(formData.zip_code);
        $('#phone').val(formData.phone);
    }

    $('#gotoStep1-4').on('click', function (e) {
        e.preventDefault();
        $('#informationForm')[0].reportValidity();

        const formFinancement = $('#informationForm')[0];
        const isValid = formFinancement.checkValidity();

        if (isValid) {
            var informationData = {
                civility: $("input[name='civilite']:checked").val(),
                last_name: $("#nom").val(),
                first_name: $("#prenom").val(),
                maiden_name: $("#maiden-name").val(),
                date_of_birth: $("#date-of-birth").val(),
                city_of_birth: $("#city-of-birth").val(),
                code_postal_naissance: $("#code-postal-naissance").val(),
                address: $("#address").val(),
                additional_address: $("#additional-address").val(),
                code_postal: $("#code-postal").val(),
                city: $("#city").val(),
                country: $("#country").val(),
                nationality: $("#nationality").val(),
                principal_residence: $( "#principal-residence option:selected" ).val(),
                family_status: $("#family-status").val(),
                number_of_dependent_children: $("#number-of-dependent-children").val(),
                occupation: $("#occupation").val(),
                type_of_contract: $( "#type-of-contract option:selected" ).val(),
                hiring_date: $("#hiring-date").val(),
                email: $("#email").val(),
                phone: $("#phone").val().replace(/\s/g, ''),
            };

            window.location.href = $(this).attr('rel')+'?formData='+encodeURIComponent(JSON.stringify(informationData)) + '&customerApplication=' + customerApplication;

            if ('parentIFrame' in window) {
                parentIFrame.scrollTo(0,0);
            }
        }
    });
});