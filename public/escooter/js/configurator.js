function handleMessage(evt)
{
	try{
		  parentIFrame.getPageInfo(
				    (pageInfo) => sendPageInfo(pageInfo)
				  );
	}catch(e){}
}
function sendPageInfo(pageInfo){
	try{
        _letScrollPicSidebar(pageInfo);
        _letScrollStickyHeader(pageInfo);
        _openModal(pageInfo);
	}catch(e){}
}

if (window.addEventListener) {
	window.addEventListener("message", handleMessage, false);
}else {
	window.attachEvent("onmessage", handleMessage);
}


const collapseBtnElement = document.querySelectorAll("a.icon--small"), colorListElement = document.querySelectorAll(".color ul li"),
    colorCheckboxListElement = document.querySelectorAll(".color__checkbox"), collapseDivHeaderElement = document.querySelectorAll("h5.header");

function removeClassNameActive(e, c = "active") {
    e.forEach(e => {
        e.className.includes(c) && e.classList.remove(c)
    })
}

function removeInnerHTML(e) {
    e.forEach(e => {
        e.innerHTML = ""
    })
}

var colorValue;
let colorCashPrice;
let colorRentPrice;



function getColorValue() {
    colorValue = $('#color li.active').attr('value');

    setColorPrice(colorValue);
}
getColorValue()

function setColorPrice(colorValue) {
    dataColorPrice.forEach(function (data) {
        if (colorValue === data.id) {

            // set color price
            colorCashPrice = data.cashPrice;
            colorRentPrice = data.rentPrice;

            // display color price
            $('#financement .cash label.header p.d-flex').html('À partir de '+ data.cashPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") +'€ TTC*');
            $('#financement .rent label.header p.d-flex').html('<span class="d-md-block d-none">À partir de&nbsp;</span> '+ data.rentPrice +'€/mois');
            
            // Calculation GCM
            var gcmPrice = parseFloat(colorCashPrice) * 0.0011; //0.11%
            $('#financement .rent div.gcm p.text--price').html(+ gcmPrice.toFixed(2) +'€/mois');
        }
    });
}

collapseDivHeaderElement.forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus"
    })
});

document.querySelectorAll('.collapse-text').forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus";
    })
});

colorListElement.forEach(e => {
    e.addEventListener("click", c => {
        colorValue = $(e).attr('value');
        setColorPrice(colorValue);
        c.preventDefault(), removeClassNameActive(colorListElement), removeInnerHTML(colorCheckboxListElement), e.classList.add("active"), e.querySelector('a').classList.add('color-circle-checkbox')
    })
});

function getSelectedFinancement() {
    var selectedValue = $("input[name='financement']:checked");
    selectedValue.closest('.shadow').addClass('border-active');
}
getSelectedFinancement()

$('input[type=radio][name=financement]').change(function() {
    $(this).closest('#financement').find('.shadow').removeClass('border-active')
    var shadow = $(this).closest('.shadow');
    shadow.addClass('border-active');
});

let cookieAccessory = Cookies.get("cookie_dataAccessory");

function getData() {
	
	
    let cookie = Cookies.get("cookie_configuratorData");

    if(typeof cookie != 'undefined' && cookie != 'undefined') {
        var configuratorData = JSON.parse(cookie);
        var colorSelector = document.querySelectorAll('.color ul li[value='+ configuratorData.colorValue+ ']')[0];
        if(typeof colorSelector != 'undefined' && colorSelector != 'undefined') {
            removeClassNameActive(colorListElement), removeInnerHTML(colorCheckboxListElement)
            colorSelector.querySelector(".color__checkbox").innerHTML = '<label class="check-task custom-control custom-checkbox">\n    <input type="checkbox" class="custom-control-input" checked>\n    <span class="custom-control-label"></span>\n  </label>';
            $(colorSelector).addClass('active');
            var slides =document.querySelectorAll('#carouselProduct .carousel-inner .carousel-item') ;
            removeClassNameActive(slides);

            setColorPrice(configuratorData.colorValue);

            slides.forEach(e => {
                var itemData = $(e).data('value');
                if (itemData == configuratorData.colorValue){
                    $(e).addClass('active');
                }
            })
        }
    }


        // dataAccessory.sort(function(x, y) {
        //     return x.isFree-y.isFree;
        // });

        dataAccessory.forEach(function (data) {
            if (data.isFree == true) {
                $('#accessoires ul').append('<li class="accessoires--active">\n' +
                    '                            <label class="row" for="'+ data.id +'">\n' +
                    '                              <div class="col-10">\n' +
                    '                                <div class="accessoires__item">\n' +
                    '                                  <img src="'+ data.configuratorImage +'" alt="item_images">\n' +
                    '                                  <div class="accessoires__item--info">\n' +
                    '                                    <p class="text--header">'+ data.label +'</p>\n' +
                    '                                    <p class="text--price">'+ data.price +'€</p>\n' +
                    '                                    <p class="text--offer">Offert avec l\'offre de lancement</p>\n' +
                    '                                  </div>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                              <div class="col-2">\n' +
                    '                                <div class="accessoires__checkbox">\n' +
                    '                                  <label class="custom-control custom-checkbox checkbox-square custom-card">\n' +
                    '                                    <input type="checkbox" class="custom-control-input" id="'+ data.id +'"\n' +
                    '                                      checked>\n' +
                    '                                    <span class="custom-control-label"></span>\n' +
                    '                                  </label>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                            </label>\n' +
                    '                          </li>');
            } else {
                $('#accessoires ul').append('<li>\n' +
                    '                            <label class="row" for="'+ data.id +'">\n' +
                    '                              <div class="col-10">\n' +
                    '                                <div class="accessoires__item">\n' +
                    '                                  <img src="'+ data.configuratorImage +'" alt="item_images">\n' +
                    '                                  <div class="accessoires__item--info">\n' +
                    '                                    <p class="text--header">'+ data.label +'</p>\n' +
                    '                                    <p class="text--price">'+ data.price +'€</p>\n' +
                    '                                  </div>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                              <div class="col-2">\n' +
                    '                                <div class="accessoires__checkbox">\n' +
                    '                                  <label class="custom-control custom-checkbox checkbox-square custom-card">\n' +
                    '                                    <input type="checkbox" class="custom-control-input" id="'+ data.id +'">\n' +
                    '                                    <span class="custom-control-label hand-cursor"></span>\n' +
                    '                                  </label>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                            </label>\n' +
                    '                          </li>');
            }
        });
}

getData();

function addLineForAssurance() {
    $('#accessoires ul li:eq(2)').after('<div class="line-2"></div>')
    $('#accessoires ul li:eq(4)').after('<div class="line-2"></div>')
}
addLineForAssurance()

function _changePage(){
    $('.gotoPage').click(function (e){
    	
    	var queryString = window.location.search;
    	var urlParams = new URLSearchParams(queryString);
    	var zipcode = urlParams.get('zipcode');
    	
        var configuratorData = {
            colorValue: colorValue,
            colorCashPrice: colorCashPrice,
            colorRentPrice: colorRentPrice,
            financement: $("input[name='financement']:checked").val(),
            assuranceOui: $("input[name='assurance_oui']:checked").val(),
            gcm: $("input[name='gcm']:checked").length,
            vdr: $("input[name='vdr']:checked").length,
            dataAccessory: {},
        };

            dataAccessory.forEach(function (data) {
                let id = data.id;
                if ($('#'+ id +'').is(":checked")) {

                    configuratorData.dataAccessory[id] = {
                        id: id,
                        label: data.label,
                        price: data.price,
                        isFree: data.isFree,
                        configuratorImage: data.configuratorImage,
                    }
                }
            });
        window.location.href=$(this).attr('rel')+'?zipcode='+zipcode+'&data='+encodeURIComponent(JSON.stringify(configuratorData));
        
        if ('parentIFrame' in window) {
            parentIFrame.scrollTo(0,0);
        }
    })
}

_changePage();

function checkZipcodeForEligible() {
    var apiUrl = window.iFrameResizer
    let zipcodeValue = $('#zipcode').val();
    if (!zipcodeValue) {
        zipcodeValue = $('#zipcode').attr('placeholder');
    }
    let url = apiUrl.targetApiPartiTech + '/check-zipcode-eligible/' + zipcodeValue;

    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            if($('#particulier').is(':checked') ) {
                $('.alert-eligible').removeClass('d-none');
                $('.alert-not-eligible').addClass('d-none');
                $('.btn-recontact').addClass('d-none');
                $('.btn-ligne').removeClass('d-none');
                $('.btn-concession').removeClass('d-none');
            } else {
                $('.alert-not-eligible').removeClass('d-none');
                $('.alert-eligible').addClass('d-none');
                $('.btn-recontact').removeClass('d-none');
                $('.btn-ligne').addClass('d-none');
                $('.btn-concession').addClass('d-none');
            }
        },
        error: function (error) {
            $('.alert-not-eligible').removeClass('d-none');
            $('.alert-eligible').addClass('d-none');
            $('.btn-recontact').removeClass('d-none');
            $('.btn-ligne').addClass('d-none');
            $('.btn-concession').addClass('d-none');
        }
    });
}

function checkEligible() {
    $(' #zipcode').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
            .attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
    $('#zipcode').keyup(function () {
        if (typeof Timer !== 'undefined'){
            clearTimeout(Timer);
        }

        Timer = setTimeout(()=>{
            checkZipcodeForEligible();
        }, 300);
    });

    $('input[type=radio][name=declare]').change(function() {
        if (this.value == 'un-particulier') {
            checkZipcodeForEligible();
        }else if (this.value == 'professionnel') {
            checkZipcodeForEligible();
        }
    });
}

checkEligible();

function _letScrollPicSidebar(event) {
    var docViewTop = event.scrollTop ;

    if (window.matchMedia('(max-width: 767px)').matches) {
        // remove sticky on mobile

        // $('#sticky-sidebar').css({
        //     position: "relative",
        //     top: docViewTop - event.offsetTop,
        // });
    } else {
        $('#sticky-sidebar').css({
            position: "absolute",
            top: (docViewTop<1555?docViewTop:1555 ) + event.clientHeight / 4,
        });
    }

    $('#sticky-sidebar').css('z-index', 998);
}

function _letScrollStickyHeader(event) {
    var docViewTop = event.scrollTop ;
    var top = docViewTop - event.offsetTop;

    //Setting sticky header
    if (top > 0) {
        $('#stickyHeaderOffre').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    } else {
        $('#stickyHeaderOffre').css({
            position: "relative",
            top: "unset",
        });
    }

    $('#stickyHeaderOffre').css('z-index', 999);
}

function _openModal(event) {
    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    }
}

function collapseText() {
    var txt = $('#changedText').text();

    if (txt == "Réduire le détail" ) {
        $("#changedText").text("Voir le détail");
    }else {
        $("#changedText").text("Réduire le détail");
    }
}

$('.btn-show').click(function () {
    var item = $(this);

    if (item.length > 0) {
        var content = item.closest('.description');

        if (content.length > 0 && content.hasClass('active')) {
            content.removeClass('active');
            item.text("Lire les mentions légales");
        } else {
            content.addClass('active');
            item.text("Masquer les mentions légales");
        }
    }
})
const fieldSelectCheckbox = [
    '.declare',
    '.form-civilite',
    '.form-permit',
    '.form-privacy_email'
];
$(document).ready(function () {
    $('.go-back').click(() => {
        window.history.back();
    })

    fieldSelectCheckbox.map((item) => {
        $(item).find('input[type=radio]').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).next().addClass('checked');
            }
            $(item).find('input[type=radio]').not($(this)).next().removeClass('checked');
        });
    });

    $('input').on('blur', function () {
        $(this).val() === "" ? $(this).removeClass("filled") : $(this).addClass("filled")
    });
});