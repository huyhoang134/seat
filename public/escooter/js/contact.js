const fieldSelectCheckbox = [
    '.form-civilite',
    '.form-permit',
    '.form-privacy_email'
];

$(document).ready(function () {

	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	
	var zipcode = urlParams.get('zipcode');
	
	var get_dealer=urlParams.get('data');
	if(get_dealer!=null){
		var configuratorData = JSON.parse(get_dealer);
	}
	
	
	var dealer = urlParams.get('dealer');
	
    $('.go-back').click(() => {
        window.history.back();
    })

    if(typeof zipcode != 'undefined' && zipcode != null){
    	$('#zipcode').val(zipcode);
    }

    var comment = 'Personne intéressé par un SEAT MÓ eScooter 125; ';
    var model = 'S0AAA3';

	//$('#civilite-m').focus();
    $('#contactFormLead').submit(function (event) {

    	var apiUrl = window.iFrameResizer

        var now = new Date();
        var dateStringWithTime = moment(now).format('YYYY/MM/DD HH:mm:ss');

        var isPermis = $("#permisSelect option:selected").val() == 'OUI' ? 'OUI' : 'NON';
        comment += ' Permis 125 : ' + isPermis;

        if(typeof dealer != 'undefined' && dealer != null) {
        	selected_dealer=dealer;
        }else{
        	selected_dealer="";
        }


        /* model id : 
         * Rouge Audacieux S0AAA3
			Blanc Oxygène S0AAA1
			Gris Aluminium S0AAA5
         * */
        var formData = {
            action: 'lead',
            campaignid: 100515,
            model: model,
            civility: $("input[name='civilite']:checked").val(),
            last_name: $("#nom").val(),
            first_name: $("#prenom").val(),
            email: $("#email").val(),
            zip_code: $("#zipcode").val(),
            phone: $("#phone").val().replace(/\s/g, ''),
            privacy_email: $("input[name='privacy_email']:checked").val(),
            dealer: selected_dealer,
            comment: comment,
            created_at: dateStringWithTime,
        };

        console.log(formData);
        var div = document.createElement("div");
        div.innerHTML = "<a style='position:absolute;left:0;transition: width 2s;top:" + 150 + "px' href='#'></a>";
        div = div.firstChild;
        document.body.appendChild(div);
        div.focus();
        div.parentNode.removeChild(div);

        if (typeof dealer == "undefined" ||  dealer == 'undefined'|| dealer === null   || ( typeof dealer != "undefined" && dealer.length <3) ) {
            dealer = '';

            let zipcode = $("#zipcode").val();
            let url = apiUrl.targetApiPartiTech + '/get-distributors-by-adress/' + zipcode;

            $.ajax({
                crossOrigin: true,
                type: 'GET',
                url: url,
                success: function (response) {
                    var dealer = response.id;
                    formData.dealer = dealer;
                    $.ajax({
                        type: "GET",
                        url: apiUrl.targetApiLeadConnect,
                        data: formData,
                        success: function (response) {
                            Cookies.remove("cookie_configuratorData");
                            $('.common-info').removeClass('d-flex');
                            $('.common-info').hide(500);
                            $('#contact-header').hide(500);
                            $('#contactFormLead').hide(500);
                            $('#responseMessage').removeClass('d-none');
                        },
                        error: function (error) {
                        }
                    });
                },
                error: function (error) {
                }
            });
        } else {
            $.ajax({
                type: "GET",
                url: apiUrl.targetApiLeadConnect,
                data: formData,
                success: function (response) {
                    Cookies.remove("cookie_configuratorData");
                    $('.common-info').removeClass('d-flex');
                    $('.common-info').hide(500);
                    $('#contact-header').hide(500);
                    $('#contactFormLead').hide(500);
                    $('#responseMessage').removeClass('d-none');
                },
                error: function (error) {
                }
            });
        }

        event.preventDefault();
    });

    // $('.contact .form-fill__input input[type=radio]').on('click', function () {
    //     if ($(this).is(':checked')) {
    //         $(this).next().addClass('checked');
    //     }
    //
    //     $('.contact .form-fill__input input[type=radio]').not($(this)).next().removeClass('checked');
    //
    // });

    fieldSelectCheckbox.map((item) => {
        $(item).find('input[type=radio]').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).next().addClass('checked');
            }
            $(item).find('input[type=radio]').not($(this)).next().removeClass('checked');
        });
    });
});

document.querySelectorAll('p.icon-collapse').forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus"
    })
});

function changeTextInteresse() {
    var txt1 = $('#changeTextInteresse').text();

    if (txt1 == "Voir tous les services inclus" ) {
        $("#changeTextInteresse").text("Réduire tous les services inclus");
    }else {
        $("#changeTextInteresse").text("Voir tous les services inclus");
    }
}

function changeTextAccessoires() {
    var txt2 = $('#changeTextAccessoires').text();

    if (txt2 == "Voir le détail" ) {
        $("#changeTextAccessoires").text("Réduire le détail");
    }else {
        $("#changeTextAccessoires").text("Voir le détail");
    }
}

function handleMessage(evt)
{
    try{
        parentIFrame.getPageInfo(
            (pageInfo) => sendPageInfo(pageInfo)
        );
    }catch(e){}
}
function sendPageInfo(pageInfo){
    try{
        _letScrollStickyHeader(pageInfo);
        _openModal(pageInfo);
    }catch(e){}
}

if (window.addEventListener) {
    window.addEventListener("message", handleMessage, false);
}else {
    window.attachEvent("onmessage", handleMessage);
}

function _letScrollStickyHeader(event) {
    var docViewTop = event.scrollTop ;
    var top = docViewTop - event.offsetTop;

    //Setting sticky header
    if (top > 0) {
        $('#stickyHeaderOffre').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    } else {
        $('#stickyHeaderOffre').css({
            position: "relative",
            top: "unset",
        });
    }

    $('#stickyHeaderOffre').css('z-index', 999);
}

function _openModal(event) {
    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    }
}

$('.btn-show').click(function () {
    var item = $(this);

    if (item.length > 0) {
        var content = item.closest('.description');

        if (content.length > 0 && content.hasClass('active')) {
            content.removeClass('active');
            item.text("Lire les mentions légales");
        } else {
            content.addClass('active');
            item.text("Masquer les mentions légales");
        }
    }
})
