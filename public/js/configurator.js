function handleMessage(evt)
{
	try{
		  parentIFrame.getPageInfo(
				    (pageInfo) => sendPageInfo(pageInfo)
				  );
	}catch(e){}
}
function sendPageInfo(pageInfo){
	try{
        _letScrollPicSidebar(pageInfo);
        _letScrollStickyHeader(pageInfo);
        _openModal(pageInfo);
	}catch(e){}
}

if (window.addEventListener) {
	window.addEventListener("message", handleMessage, false);
}else {
	window.attachEvent("onmessage", handleMessage);
}


const collapseBtnElement = document.querySelectorAll("a.icon--small"), colorListElement = document.querySelectorAll(".color ul li"),
    colorCheckboxListElement = document.querySelectorAll(".color__checkbox"), collapseDivHeaderElement = document.querySelectorAll("h5.header");

function removeClassNameActive(e, c = "active") {
    e.forEach(e => {
        e.className.includes(c) && e.classList.remove(c)
    })
}

function removeInnerHTML(e) {
    e.forEach(e => {
        e.innerHTML = ""
    })
}

var colorValue;
let colorCashPrice;
let colorRentPrice;
let colorName;

getData();

function getColorValue() {
    colorValue = $('#color li.active').attr('value');

    setColorPrice(colorValue);
}
getColorValue()

function setColorPrice(colorValue) {
    if (typeof dataColorPrice !== 'undefined') {
        dataColorPrice.forEach(function (data) {
            if (colorValue === data.id) {
                var month = $('.item-duration').find('input[name=financement]:checked').val();
                // set color price
                var lastKeyMonth = Object.keys(data.rentPrice)[1];
                month = month ? month : lastKeyMonth;
                colorCashPrice = data.cashPrice;
                colorRentPrice = data.rentPrice[month];
                var loaDuree = data.loaDuree[month]
                var loaLoyers = data.loaLoyers[month]
                var montantTotalDu = data.montantTotalDu[month]
                var optionAchat = data.optionAchat[month]
                var loyerMajore = data.loyerMajore[month]
                var dateOffre = data.dateOffre[month]
                var mentionLegale = data.mentionLegale[month].left_content + data.mentionLegale[month].right_content;
                var loyerAuTitreDeLocation = data.loyerAuTitreDeLocation[month]
                var kilometer = data.kilometer[month]
                var gcmPrice;
                var cashPrice;

                if (typeof colorCashPrice === 'string') {
                    gcmPrice  = parseFloat(colorCashPrice.replace(/\s/g, '').replace(',', '.')) * 0.0011; //0.11%
                    cashPrice = data.cashPrice;
                } else {
                    gcmPrice  = parseFloat(colorCashPrice) * 0.0011;
                    cashPrice = data.cashPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                }

                // display color price
                $('#financement .cash label.header p.d-flex').html('À partir de '+ cashPrice +'€ TTC*');

                const gcm = $('input[name=gcm]').is(':checked');
                const vdr = $('input[name=vdr]').is(':checked');
                if (gcm && vdr) {
                    $('#financement .rent .card-title p.d-flex').html('À partir de&nbsp; ' + (data.rentPrice[month] + 5 + Math.round(gcmPrice)) + '€/mois');
                    $('.price-per-month').text(data.rentPrice[month] + 5 + Math.round(gcmPrice));
                    localStorage.setItem('prixAPartirDe', data.rentPrice[month] + 5 + Math.round(gcmPrice))
                } else if (gcm) {
                    $('#financement .rent .card-title p.d-flex').html('À partir de&nbsp; ' + (data.rentPrice[month] + Math.round(gcmPrice)) + '€/mois');
                    $('.price-per-month').text(data.rentPrice[month] + Math.round(gcmPrice));
                    localStorage.setItem('prixAPartirDe', data.rentPrice[month] + Math.round(gcmPrice))
                } else if (vdr) {
                    $('#financement .rent .card-title p.d-flex').html('À partir de&nbsp; ' + (data.rentPrice[month] + 5) + '€/mois');
                    $('.price-per-month').text(data.rentPrice[month] + 5);
                    localStorage.setItem('prixAPartirDe', data.rentPrice[month] + 5)
                } else {
                    $('#financement .rent .card-title p.d-flex').html('À partir de&nbsp; ' + data.rentPrice[month] + '€/mois');
                    $('.price-per-month').text(data.rentPrice[month]);
                    localStorage.setItem('prixAPartirDe', data.rentPrice[month])
                }

                // Calculation GCM
                $('#financement .rent div.gcm p.text--price').html(+ gcmPrice.toFixed(2) + '€ / mois<span class="text-c-orange">*</span>');

                $('.var_loa_duree').html(loaDuree)
                $('.var_loa_loyers').html(loaLoyers)
                $('.var_montant_total_du').html(montantTotalDu)
                $('.var_option_achat').html(optionAchat)
                $('.var_loyer_majore').html(loyerMajore)
                $('.var_date_offre').html(dateOffre)
                $('.var_mention_legale').html(mentionLegale)
                $('.var_loyer_au_titre_de_location').html(loyerAuTitreDeLocation);

                localStorage.setItem('loaDuree', loaDuree)
                localStorage.setItem('loaLoyers', loaLoyers)
                localStorage.setItem('montantTotalDu', montantTotalDu)
                localStorage.setItem('optionAchat', optionAchat)
                localStorage.setItem('loyerMajore', loyerMajore)
                localStorage.setItem('dateOffre', dateOffre)
                localStorage.setItem('mentionLegale', mentionLegale)
                localStorage.setItem('loyerAuTitreDeLocation', loyerAuTitreDeLocation)
                localStorage.setItem('prixCatalogue', colorCashPrice)
                localStorage.setItem('kilometer', kilometer)
            }
        });
    }
}

$('.benefit').click(function () {
    colorValue = $('#color li.active').attr('value');
    setColorPrice(colorValue);
})

$('#color li').click(function () {
    colorValue = $('#color li.active').attr('value');
    setColorPrice(colorValue);
});

$('.item-duration input[name=financement]').change(function () {
    colorValue = $('#color li.active').attr('value');
    setColorPrice(colorValue);
})

collapseDivHeaderElement.forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus"
    })
});

document.querySelectorAll('.collapse-text').forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus";
    })
});

colorListElement.forEach(e => {
    e.addEventListener("click", c => {
        colorValue = $(e).attr('value');
        setColorPrice(colorValue);
        c.preventDefault(), removeClassNameActive(colorListElement), removeInnerHTML(colorCheckboxListElement), e.classList.add("active"), e.querySelector('a').classList.add('color-circle-checkbox')
    })
});

function getSelectedFinancement() {
    var selectedValue = $("input[name='financement']:checked");
    selectedValue.closest('.shadow').addClass('border-active');
}
getSelectedFinancement()

$('input[type=radio][name=financement]').change(function() {

    $(this).closest('#financement').find('.shadow').removeClass('border-active')
    var shadow = $(this).closest('.shadow');
    shadow.addClass('border-active');
    if($(this).val() === 'cash') {
        $('#modalLegal').hide()
    } else {
        $('#modalLegal').show()
    }
    if($('input[name=gcm]').is(':checked')){
        $('input[type=checkbox][name=gcm]').trigger('click')
    }
    if( $('input[name=vdr]').is(':checked')){
        $('input[type=checkbox][name=vdr]').trigger('click')
    }

});

let cookieAccessory = Cookies.get("cookie_dataAccessory");

function getData() {
    let cookie = Cookies.get("cookie_configuratorData");

    if(typeof cookie != 'undefined' && cookie != 'undefined') {
        var configuratorData = JSON.parse(cookie);
        var colorSelector = document.querySelectorAll('.color ul li[value='+ configuratorData.colorValue+ ']')[0];
        if(typeof colorSelector != 'undefined' && colorSelector != 'undefined') {
            removeClassNameActive(colorListElement), removeInnerHTML(colorCheckboxListElement)
            colorSelector.querySelector(".color__checkbox").innerHTML = '<label class="check-task custom-control custom-checkbox">\n    <input type="checkbox" class="custom-control-input" checked>\n    <span class="custom-control-label"></span>\n  </label>';
            $(colorSelector).addClass('active');
            var slides =document.querySelectorAll('#carouselProduct .carousel-inner .carousel-item') ;
            removeClassNameActive(slides);

            setColorPrice(configuratorData.colorValue);

            slides.forEach(e => {
                var itemData = $(e).data('value');
                if (itemData == configuratorData.colorValue){
                    $(e).addClass('active');
                }
            })
        }
    }

        if (typeof dataAccessory !== 'undefined') {
             dataAccessory.forEach(function (data) {
                if (data.isFree == true) {
                $('#accessoires ul').append('<li class="accessoires--active">\n' +
                    '                            <label class="row" for="'+ data.id +'">\n' +
                    '                              <div class="col-10">\n' +
                    '                                <div class="accessoires__item">\n' +
                    '                                  <img src="'+ data.configuratorImage +'" alt="item_images">\n' +
                    '                                  <div class="accessoires__item--info">\n' +
                    '                                    <p class="text--header mb-0">'+ data.label +'</p>\n' +
                    '                                    <p class="text--price">'+ data.price +'€</p>\n' +
                    '                                    <p class="text--offer">Offert avec l\'offre de lancement</p>\n' +
                    '                                  </div>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                              <div class="col-2">\n' +
                    '                                <div class="accessoires__checkbox">\n' +
                    '                                  <label class="custom-control custom-checkbox checkbox-square custom-card">\n' +
                    '                                    <input type="checkbox" class="custom-control-input accessoiry-item" id="'+ data.id +'"\n' +
                    '                                      checked>\n' +
                    '                                    <span class="custom-control-label"></span>\n' +
                    '                                  </label>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                            </label>\n' +
                    '                          </li>');
            } else {
                $('#accessoires ul').append('<li>\n' +
                    '                            <label class="row" for="'+ data.id +'">\n' +
                    '                              <div class="col-10">\n' +
                    '                                <div class="accessoires__item">\n' +
                    '                                  <img src="'+ data.configuratorImage +'" alt="item_images">\n' +
                    '                                  <div class="accessoires__item--info">\n' +
                    '                                    <p class="text--header mb-0">'+ data.label +'</p>\n' +
                    '                                    <p class="text--price">'+ data.price +'€</p>\n' +
                    '                                  </div>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                              <div class="col-2">\n' +
                    '                                <div class="accessoires__checkbox">\n' +
                    '                                  <label class="custom-control custom-checkbox checkbox-square custom-card">\n' +
                    '                                    <input type="checkbox" class="custom-control-input accessoiry-item" id="'+ data.id +'">\n' +
                    '                                    <span class="custom-control-label hand-cursor"></span>\n' +
                    '                                  </label>\n' +
                    '                                </div>\n' +
                    '                              </div>\n' +
                    '                            </label>\n' +
                    '                          </li>');
            }
        });
        }

        if (typeof dataColorPrice !== 'undefined') {
            dataColorPrice.forEach(function(data, index) {
               var isActive = index === 0 ? 'active' : '';
               $('.color.product-detail__block').find('ul').append('<li class="'+ isActive +'" value="' + data.id + '" data-bs-target="#carouselProduct" data-bs-slide-to="' + index +'" aria-label="Slide '+ index +'">' +
                                                                      '<a href="#" class="color-circle-checkbox">' +
                                                                      '<img src="images/' + data.colorImage + '">' +
                                                                      '</a>' +
                                                                      '<p>' + data.label + '</p>' +
                                                                      '</li>');

               $('#sticky-sidebar').append('<div class="carousel-item ' + isActive + '" data-value="' + data.id +'">' +
                                               '<img src="images/' + data.modelImage +'" class="w-75 slider__images">' +
                                               '<img src="images/configurator/energy-label.png" class="energy-label" alt="energy">' +
                                               '</div>')
            });
        }

        $('.color.product-detail__block ul').find('li').on('click', function() {
            $('.color.product-detail__block ul').find('li').removeClass('active');
            $(this).addClass('active');
        });

        if (typeof modelName !== 'undefined') {
            $('.product-detail__info .product-detail__block .text--large .text--model').text(modelName);
        }
}

function addLineForAssurance() {
    $('#accessoires ul li:eq(2)').after('<div class="line-2"></div>')
    $('#accessoires ul li:eq(4)').after('<div class="line-2"></div>')
}
addLineForAssurance()

function _changePage(){
    $('.gotoPage').click(function (e){
        if ($(this).attr('rel') === 'contact.html') {
            trackingCTAGoToLeadForm(this);
        }
        colorValue = $('#color li.active').attr('value');
        colorName = $('#color li.active').find('p').text();
        setColorPrice(colorValue);
    	
    	var queryString = window.location.search;
    	var urlParams = new URLSearchParams(queryString);
    	var zipcode = urlParams.get('zipcode');
        var dealer  = urlParams.get('dealer');
        var gamme   = urlParams.get('gamme') ?? '';

        var configuratorData = {
            colorValue: colorValue,
            colorName: colorName,
            colorCashPrice: colorCashPrice,
            colorRentPrice: colorRentPrice,
            financement: $("input[name='financement']:checked").val(),
            assuranceOui: $("input[name='assurance_oui']:checked").val(),
            gcm: $("input[name='gcm']:checked").length,
            vdr: $("input[name='vdr']:checked").length,
            dataAccessory: {},
        };

            dataAccessory.forEach(function (data) {
                let id = data.id;
                if ($('#'+ id +'').is(":checked")) {

                    configuratorData.dataAccessory[id] = {
                        id: id,
                        label: data.label,
                        price: data.price,
                        isFree: data.isFree,
                        configuratorImage: data.configuratorImage,
                    }
                }
            });
        window.location.href=$(this).attr('rel')+'?zipcode='+zipcode+'&dealer='+dealer+'&gamme='+ gamme +'&data='+encodeURIComponent(JSON.stringify(configuratorData));

        if ('parentIFrame' in window) {
            parentIFrame.scrollTo(0,0);
        }
    })
}

_changePage();

function checkZipcodeForEligible() {
    var apiUrl = window.iFrameResizerSeat125Mo
    let zipcodeValue = $('#zipcode').val();
    if (!zipcodeValue) {
        zipcodeValue = $('#zipcode').attr('placeholder');
    }
    let url = apiUrl.targetApiPartiTech + '/get-distributors/' + zipcodeValue;
    let urlParams = new URLSearchParams(window.location.search);

    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            if($('#particulier').is(':checked') ) {
                $('.alert-eligible').removeClass('d-none');
                $('.alert-not-eligible').addClass('d-none');
                $('.btn-recontact').addClass('d-none');
                $('.btn-ligne').removeClass('d-none');
                $('.btn-concession').removeClass('d-none');
                $('#contactFormLead').removeClass('d-none');
                $('.card-conditions .item-condition .text-declare').text('Je déclare être un particulier :');
                $('#contactFormLead .btn-en-concession').attr('href', '/contact.html?zipcode=' + zipcodeValue);
            } else {
                $('.alert-not-eligible').removeClass('d-none');
                $('.alert-eligible').addClass('d-none');
                $('.btn-recontact').removeClass('d-none');
                $('.btn-ligne').addClass('d-none');
                $('.btn-concession').addClass('d-none');
                $('#contactFormLead').addClass('d-none');
                $('.card-conditions .item-condition .text-declare').text('Je déclare être :');
            }
        },
        error: function (error) {
            $('.alert-not-eligible').removeClass('d-none');
            $('.alert-eligible').addClass('d-none');
            $('.btn-recontact').removeClass('d-none');
            $('.btn-ligne').addClass('d-none');
            $('.btn-concession').addClass('d-none');
            $('#contactFormLead').addClass('d-none');
            $('.card-conditions .item-condition .text-declare').text('Je déclare être :');
        }
    });
}

function checkEligible() {
    $(' #zipcode').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
            .attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
    $('#zipcode').keyup(function () {
        if (typeof Timer !== 'undefined'){
            clearTimeout(Timer);
        }

        Timer = setTimeout(()=>{
            checkZipcodeForEligible();
        }, 300);
    });

    $('input[type=radio][name=declare]').change(function() {
        if (this.value == 'un-particulier') {
            checkZipcodeForEligible();
        }else if (this.value == 'professionnel') {
            checkZipcodeForEligible();
        }
    });
}

checkEligible();

function _letScrollPicSidebar(event) {
    var docViewTop = event.scrollTop ;

    if (window.matchMedia('(max-width: 767px)').matches) {
        // remove sticky on mobile

        // $('#sticky-sidebar').css({
        //     position: "relative",
        //     top: docViewTop - event.offsetTop,
        // });
    } else {
        $('#sticky-sidebar').css({
            position: "absolute",
            top: (docViewTop<1555?docViewTop:1555 ) + event.clientHeight / 4,
        });
    }

    $('#sticky-sidebar').css('z-index', 998);
}

function _letScrollStickyHeader(event) {
    var docViewTop = event.scrollTop ;
    var top = docViewTop - event.offsetTop;

    //Setting sticky header
    if (top > 0) {
        $('#stickyHeaderOffre').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    } else {
        $('#stickyHeaderOffre').css({
            position: "relative",
            top: "unset",
        });
    }

    $('#stickyHeaderOffre').css('z-index', 999);
}

function _openModal(event) {
    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    }
}

function collapseText() {
    var txt = $('#changedText').text();

    if (txt == "Réduire le détail" ) {
        $("#changedText").text("Voir le détail");
    }else {
        $("#changedText").text("Réduire le détail");
    }
}

$('.btn-show').click(function () {
    var item = $(this);

    if (item.length > 0) {
        var content = item.closest('.description');

        if (content.length > 0 && content.hasClass('active')) {
            content.removeClass('active');
            item.text("Lire les mentions légales");
        } else {
            content.addClass('active');
            item.text("Masquer les mentions légales");
        }
    }
})
const fieldSelectCheckbox = [
    '.declare',
    '.form-civilite',
    '.form-permit',
    '.form-privacy_email',
    '.form-personne-majeure'
];
$(document).ready(function () {
    $('.go-back').click(() => {
        window.history.back();
    })

    $('.go-back-iframe').click(() => {
        window.history.back();
        window.history.back();
    })

    // Sticky Sidebar
    $('#sticky-sidebar').css({
        position: "absolute",
        top: 160
    });
    $(window).scroll(function() {
        if (window.matchMedia('(min-width: 767px)').matches) {
            $('#sticky-sidebar').css({
                top: ($(window).scrollTop()<1900?$(window).scrollTop() + 160:1900)
            });
        }
    })

    fieldSelectCheckbox.map((item) => {
        $(item).find('input[type=radio]').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).next().addClass('checked');
            }
            $(item).find('input[type=radio]').not($(this)).next().removeClass('checked');
        });
    });

    $('input').on('blur', function () {
        $(this).val() === "" ? $(this).removeClass("filled") : $(this).addClass("filled")
    });

    // Sticky Header Offre
    $(window).scroll(function() {
        let stickyHeaderOffer = $('#stickyHeaderOffre');

        if ($(window).scrollTop() >= stickyHeaderOffer.offset().top) {
            $('#stickyHeaderOffre').addClass('sticky-active');
        }

        if ($(window).scrollTop() < 134 && stickyHeaderOffer.hasClass('sticky-active')) {
            $('#stickyHeaderOffre').removeClass('sticky-active');
        }
    });


    // issue-205: (SMFEL-11) back button must link to home
    $('.go-back-homepage').click(() => {
        window.location.href = '/';
    })

    $('input[name="financement"]').on('change', function() {
        if ($(this).val() === 'cash') {
            $('button.gotoPage').attr('rel', 'contact.html');
        } else {
            $('button.gotoPage').attr('rel', 'financement-pre-commande.html');
        }
    });
});