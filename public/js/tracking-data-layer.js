function trackingDataLayerExternal(element, isModel = false, isHeader = false) {
    var CTALabel = '';
    var carName  = '';
    var carCode  = '';

    if (isHeader) {
        CTALabel = $(element).find('h5').text().trim();
    } else {
        if ($(element).text().trim()) {
            CTALabel = $(element).text().trim();
        } else {
            CTALabel = $(element).data('socialname') ? $(element).data('socialname') : '';
        }
    }

    if ($(element).parent().hasClass('item-panel')) {
        CTALabel = $(element).find('span').first().text().trim();
    }

    if (isModel) {
        carCode = CTALabel;
        carName = CTALabel;
    }

    var dataLayerExternal = {
        'eventName': isHeader ? 'card-click' : 'CTA-click',
        'moduleComponent': $(element).attr('class') ? $(element).attr('class') : '',
        'elementName': $(element)[0].nodeName.toLowerCase(),
        'carName': carName,
        'carCode': carCode,
        'CTALabel': CTALabel,
        'CTAType': 'anchor',
        'linkURL': $(element).attr('href')
    }

    dynamic_dataLayer.call("externalLink", dataLayerExternal);
    console.log(dataLayerExternal);
}

function trackingDataLayerInternal(element, isFormSubmit = false) {

    var elementName = $(element)[0].nodeName.toLowerCase();
    var linkURL     = '';
    var CTALabel    = '';

    var moduleComponent = '';

    if (isFormSubmit) {
        linkURL         = $(element)[0].action;
        CTALabel        = $(element).find('button').first().text().trim();
        moduleComponent = $(element).find('button').first().attr('class');
    } else {
        CTALabel        =  $(element).text().trim();
        moduleComponent = $(element).attr('class') ? $(element).attr('class') : ''

        if (elementName === 'a') {
            linkURL = $(element).attr('href');
        } else {
            linkURL = $(element).data('url') ? $(element).data('url') : ($(element).attr('rel') ? $(element).attr('rel') : $(element).attr('formaction'));
        }
    }

    var dataLayerInternal = {
        'eventName': 'CTA-click',
        'moduleComponent': moduleComponent,
        'elementName': elementName,
        'carName': 'SEAT MÓ eScooter 125',
        'carCode': 'S0AAA3',
        'CTALabel': CTALabel,
        'CTAType': 'anchor',
        'linkURL': linkURL
    };
    dynamic_dataLayer.call("internalLink", dataLayerInternal)
    console.log(dataLayerInternal);
}

function trackingDataLayerPopup(element) {
    var popUpDescription = $($(element).data('bs-target')).find('h5').text().trim();

    var dataLayerPopup = {
        'eventName': 'popUpView',
        'moduleComponent': $(element).attr('class') ? $(element).attr('class') : '',
        'carName': '',
        'carCode': '',
        'popUpType': $(element).data('type'),
        'popUpTitle': 'side-modal',
        'popUpDescription': popUpDescription,
    }
    dynamic_dataLayer.call('popUpView', dataLayerPopup)
    console.log(dataLayerPopup);
}

function trackingCTAGoToLeadForm(element) {
    var linkURL = '';
    if ($(element)[0].nodeName.toLowerCase() === 'a') {
        linkURL = $(element).attr('href');
    } else {
        linkURL = $(element).attr('rel');
    }

    var dataStepCTA = {
        digitalElement: 'SEAT MO',
        formID: 'Pre - Financial - Request',
        eventName: 'app-step-CTA',
        applicationType: 'form',
        applicationName: 'Finance Request',
        applicationStep: 'appStepCTA',
        CTALabel: $(element).text().trim(),
        CTAType: $(element).attr('class') ? $(element).attr('class') : '',
        linkURL: linkURL,
    }

    dynamic_dataLayer.call("appStepCTA", dataStepCTA);
    console.log(dataStepCTA);
}

function trackingLeadFormSend(element) {
    const queryString = window.location.search;
    const urlParams   = new URLSearchParams(queryString);

    var dealerName = $(element).find('input[name=dealer]').data('dealer-name');
    var dealerCode = $(element).find('input[name=dealer]').data('dealer-code');

    var dataLeadFormSend = {
        channel: 'Pre Financing',
        virtualPageName: 'SEAT – Mo Pre Financing – form request confirm',
        digitalElement: 'SEAT MO',
        formID: 'Pre-Financial-Request',
        eventName: 'app-step-final',
        applicationType: 'form',
        applicationName: 'Finance Request',
        applicationStep: '2-registration-complete',
        dealerName: dealerName,
        dealerCode: dealerCode,
        carName: 'SEAT MÓ eScooter 125',
        carCode: urlParams.get('data') ? $.parseJSON(urlParams.get('data')).colorValue: ''
    };

    dynamic_dataLayer.push("appStepFinal", dataLeadFormSend);
    console.log(dataLeadFormSend);
}
