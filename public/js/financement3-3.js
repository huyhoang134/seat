$(document).ready(function () {
    let accessoriesItem = $('.wrapper-accessoires');
    let accessory       = accessoriesItem.attr('data-accessoires');

    $.each(dataAccessory, function (key, data) {
        if (accessory.indexOf(data.id) > -1) {
            accessoriesItem.append('<div class=\'accessoires__item col-md-6\'>\n' +
                '                                        <img src="' + data.configuratorImage + '" alt=\'item_images\'>\n' +
                '                                        <div class=\'accessoires__item--info\'>\n' +
                '                                            <p class=\'text-header\'>' + data.label + '</p>\n' +
                '                                            <p class=\'text-price\'>' + data.price + '€</p>\n' +
                '                                        </div>\n' +
                '                                    </div>');
        }
    });

    $('.selection__item--info .text-header').text(modelName);

    var vehicleModel = $('.selection__item--info .selection__item--description').data('vehicle-model');
    var dataVehicleModel;
    dataColorPrice.forEach(function (data) {
        if (data.id === vehicleModel) {
           dataVehicleModel = data;
        }
    });

    $('.selection__item .selection__item--image').append('<img src="images/' + dataVehicleModel.colorImage +'" alt="item_images" class="' + dataVehicleModel.id +'">');

    var optionMonth = $('.loa-duree-value').text();
    $('.date_offre').html(dataVehicleModel.dateOffre[optionMonth]);
    $('.loyer_au_titre_de_location').text(dataVehicleModel.loyerAuTitreDeLocation[optionMonth]);
    $('.mention_legale').html(dataVehicleModel.mentionLegale[optionMonth].left_content + dataVehicleModel.mentionLegale[optionMonth].right_content);

    var prixCatalogue = dataVehicleModel.cashPrice;
    var gcmPrice;
    if (typeof prixCatalogue === 'string') {
        gcmPrice  = parseFloat(prixCatalogue.replace(/\s/g, '').replace(',', '.')) * 0.0011; //0.11%
    } else {
        gcmPrice  = parseFloat(prixCatalogue) * 0.0011;
    }

    $('.gcm-value').html(+ gcmPrice.toFixed(2) + '€ / mois<span class="text-c-orange">*</span>');

});