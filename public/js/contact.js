const fieldSelectCheckbox = [
    '.form-civilite',
    '.form-permit',
    '.form-privacy_email'
];

$(document).ready(function () {

	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	
	var zipcode = urlParams.get('zipcode');
	
	var get_dealer=urlParams.get('data');
	if(get_dealer!=null){
		var configuratorData = JSON.parse(get_dealer);
	}
    var dealer = urlParams.get('dealer');
    var comment = 'Personne intéressé par un SEAT MÓ eScooter 125; ';

    if (configuratorData && configuratorData.dataAccessory) {
        var dataAccessoryArray = [];

        $.each(configuratorData.dataAccessory, function (index, data) {
            dataAccessoryArray.push(data.label);
        });

        comment = 'Option: ' + dataAccessoryArray.join('/ ') + ', Mensualités (Monthly payments): ';
    }

    $('.go-back').click(() => {
        window.history.back();
    })

    $('.go-back-homepage').click(() => {
        window.location.href = '/';
    })

    if(typeof zipcode != 'undefined' && zipcode != null){
    	$('#zipcode').val(zipcode);
    }

    var model = 'S0AAA3';

    var dataLeadForm = {
        channel: 'Pre Financing',
        virtualPageName: 'SEAT – Mo Pre Financing – form request ',
        digitalElement: 'SEAT MO',
        formID: 'Pre-Financial-Request',
        eventName: 'app-step-1',
        applicationType: 'form',
        applicationName: 'Finance Request',
        applicationStep: '1-contact-data',
        dealerName: $('input[name=dealer]').data('dealer-name'),
        dealerCode: $('input[name=dealer]').data('dealer-code'),
        carName: 'SEAT MÓ eScooter 125',
        carCode: urlParams.get('data') ? $.parseJSON(urlParams.get('data')).colorValue: '',
    }

    dynamic_dataLayer.push("appStep1", dataLeadForm);
    console.log(dataLeadForm);

    // Format phone number
    $('input#phone').on('input keyup', function() {
        var phoneNumber     = $(this).val().replace(/\D/g,'');
        var formattedNumber = formatPhoneNumber(phoneNumber);

        $(this).val(formattedNumber);
    });

    function formatPhoneNumber(phoneNumber) {
        return phoneNumber.replace(/(\d{2})(?=\d)/g, '$1 ');
    }
    //

    $('.btn-contact').on('click', function (e) {
        $("input[name='comment']").val(comment)
        $("input[name='dealer']").val(dealer)
    });

    // $('.contact .form-fill__input input[type=radio]').on('click', function () {
    //     if ($(this).is(':checked')) {
    //         $(this).next().addClass('checked');
    //     }
    //
    //     $('.contact .form-fill__input input[type=radio]').not($(this)).next().removeClass('checked');
    //
    // });

    fieldSelectCheckbox.map((item) => {
        $(item).find('input[type=radio]').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).next().addClass('checked');
            }
            $(item).find('input[type=radio]').not($(this)).next().removeClass('checked');
        });
    });

    // Sticky Header Offre

    $(window).scroll(function() {
        let stickyHeaderOffer = $('#stickyHeaderOffre');
        if ($(window).scrollTop() >= stickyHeaderOffer.offset().top) {
            $('#stickyHeaderOffre').addClass('sticky-active');
        }

        if ($(window).scrollTop() < 134 && stickyHeaderOffer.hasClass('sticky-active')) {
            $('#stickyHeaderOffre').removeClass('sticky-active');
        }
    });

    $('#textConfigHeader .model-name').text(modelName);
});

document.querySelectorAll('p.icon-collapse').forEach(e => {
    e.addEventListener("click", c => {
        c.preventDefault();
        const l = e.querySelector("i");
        l.className.includes("plus") ? l.className = "fas fa-minus" : l.className = "fas fa-plus"
    })
});

function changeTextInteresse() {
    var txt1 = $('#changeTextInteresse').text();

    if (txt1 == "Voir tous les services inclus" ) {
        $("#changeTextInteresse").text("Réduire tous les services inclus");
    }else {
        $("#changeTextInteresse").text("Voir tous les services inclus");
    }
}

function changeTextAccessoires() {
    var txt2 = $('#changeTextAccessoires').text();

    if (txt2 == "Voir le détail" ) {
        $("#changeTextAccessoires").text("Réduire le détail");
    }else {
        $("#changeTextAccessoires").text("Voir le détail");
    }
}

function handleMessage(evt)
{
    try{
        parentIFrame.getPageInfo(
            (pageInfo) => sendPageInfo(pageInfo)
        );
    }catch(e){}
}
function sendPageInfo(pageInfo){
    try{
        _letScrollStickyHeader(pageInfo);
        _openModal(pageInfo);
    }catch(e){}
}

if (window.addEventListener) {
    window.addEventListener("message", handleMessage, false);
}else {
    window.attachEvent("onmessage", handleMessage);
}

function _letScrollStickyHeader(event) {
    var docViewTop = event.scrollTop ;
    var top = docViewTop - event.offsetTop;

    //Setting sticky header
    if (top > 0) {
        $('#stickyHeaderOffre').css({
            position: "absolute",
            top: docViewTop - event.offsetTop,
        });
    } else {
        $('#stickyHeaderOffre').css({
            position: "relative",
            top: "unset",
        });
    }

    $('#stickyHeaderOffre').css('z-index', 999);
}

function _openModal(event) {
    var docViewTop = event.scrollTop;
    var docViewBottom = docViewTop + event.clientHeight;

    if (window.matchMedia('(min-width: 767px)').matches) {
        $('.modal').css({
            top: docViewTop,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    } else {
        $('.modal').css({
            top: docViewTop + (docViewBottom-docViewTop) / 5,
        });

        $('#legalMentionModal').css({
            height: window.screen.height / 1.3,
        });
    }
}

$('.btn-show').click(function () {
    var item = $(this);

    if (item.length > 0) {
        var content = item.closest('.description');

        if (content.length > 0 && content.hasClass('active')) {
            content.removeClass('active');
            item.text("Lire les mentions légales");
        } else {
            content.addClass('active');
            item.text("Masquer les mentions légales");
        }
    }
})
