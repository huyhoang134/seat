$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const focusInput = localStorage.getItem('firstInput')
    if (focusInput) {
        $('html, body').animate({
            scrollTop: $('#' + focusInput).offset().top - $(window).height() / 2
        }, 1000);
        if (focusInput === 'civilite-m') {
            const value = $('#nom').val();
            $('#nom').focus().val('').val(value)
            $('#nom').focus()
        } else {
            if (window.innerWidth <= 768) {
                if (focusInput === 'address') {
                    $('#civilite-block').css('display', 'none')
                    $('#mieux-vous-connaitre').css('display', 'block')
                    $('#situation-familliale').css('display', 'none')
                    $('.next').removeClass('disabled')
                    $('.prev').removeClass('disabled')
                }

                if (focusInput === 'family-status' || focusInput === 'occupation' || focusInput === 'email') {
                    $('#civilite-block').css('display', 'none')
                    $('#mieux-vous-connaitre').css('display', 'none')
                    $('#situation-familliale').css('display', 'block')
                    $('.next').addClass('disabled')
                    $('.prev').removeClass('disabled')
                }

                $('html, body').animate({
                    scrollTop: $('#' + focusInput).offset().top - $(window).height() / 2
                }, 1000);
            }
            const value = $('#' + focusInput).val();
            $('#' + focusInput).focus().val('').val(value)
        }
    }
    localStorage.removeItem('firstInput')

    var formDataFromUrl = urlParams.get('formData');
    var customerApplication = urlParams.get('customerApplication')

    if(formDataFromUrl!=null){
        var formData = JSON.parse(formDataFromUrl);
        $("input[name=civilite][value=" + urlParams.get('civility') + "]").prop('checked', true);
        $('#nom').val(urlParams.get('last_name'));
        $('#prenom').val(urlParams.get('first_name'));
        $('#address').val(urlParams.get('address'));
        $('#email').val(urlParams.get('email'));
        $('#email_confirmation').val(urlParams.get('email'));
        $('#code-postal').val(formData.zip_code);
        if (urlParams.get('phone')) {
            $('#phone').val(urlParams.get('phone'))
        }
        $("#nationality option[value='" + $('#nationality').data('nationality') +"']").attr("selected","selected");
        $("#country option[value='" + $('#country').data('country') +"']").attr("selected","selected");
        $("#occupation option[value='" + $('#occupation').data('occupation') +"']").attr("selected","selected");
        $("#family-status option[value='" + $('#family-status').data('family-status') +"']").attr("selected","selected");
        $('#employeur').val(urlParams.get('employeur'));
        $("#type-of-contract option[value='" + $('#type-of-contract').data('type-of-contract') +"']").attr("selected","selected");
        $("#principal-residence option[value='" + $('#principal-residence').data('principal-residence') +"']").attr("selected","selected");
    }

    $('#gotoStep1-4').on('click', function (e) {
        e.preventDefault();
        $('#informationForm')[0].reportValidity();
        const formFinancement = $('#informationForm')[0];
        var isValid = checkValidate();

        if (formFinancement.checkValidity() && isValid) {
            var informationData = {
                civility: $("input[name='civilite']:checked").val(),
                last_name: $("#nom").val(),
                first_name: $("#prenom").val(),
                maiden_name: $("#maiden-name").val(),
                date_of_birth: $("#date-of-birth").val(),
                city_of_birth: $("#city-of-birth").val(),
                code_postal_naissance: $("#code-postal-naissance").val(),
                address: $("#address").val(),
                additional_address: $("#additional-address").val(),
                code_postal: $("#code-postal").val(),
                city: $("#city").val(),
                country: $("#country option:selected").val(),
                nationality: $("#nationality option:selected").val(),
                principal_residence: $( "#principal-residence option:selected" ).val(),
                family_status: $("#family-status option:selected").val(),
                number_of_dependent_children: $("#number-of-dependent-children").val(),
                occupation: $("#occupation option:selected").val(),
                type_of_contract: $("#type-of-contract option:selected").val(),
                hiring_date: $("#hiring-date").val(),
                employeur: $("#employeur").val(),
                email: $("#email").val(),
                phone: $("#phone").val().replace(/\s/g, ''),
                revenu_net_mensuel: $("#revenu-net-mensuel").val(),
                autres_revenus: $("#autres-revenus").val(),
                charges_immobilieres: $("#charges-immobilieres").val(),
                autres_remboursements_mensuels: $("#autres-remboursements-mensuels").val()
            };

            localStorage.setItem('maiden-name', $("#maiden-name").val())
            localStorage.setItem('date-of-birth', $("#date-of-birth").val())
            localStorage.setItem('code-postal-naissance', $("#code-postal-naissance").val())
            localStorage.setItem('city-of-birth', $("#city-of-birth").val())
            localStorage.setItem('additional-address', $("#additional-address").val())
            localStorage.setItem('city', $("#city").val())
            localStorage.setItem('nationality', $("#nationality").val())
            localStorage.setItem('family-status', $("#family-status option:selected").val())
            localStorage.setItem('number-of-dependent-children', $("#number-of-dependent-children").val())
            localStorage.setItem('occupation', $("#occupation option:selected").val())
            localStorage.setItem('hiring-date', $("#hiring-date").val())
            localStorage.setItem('employeur', $("#employeur").val())
            localStorage.setItem('revenu_net_mensuel', $("#revenu-net-mensuel").val())
            localStorage.setItem('autres_revenus', $("#autres-revenus").val())
            localStorage.setItem('charges_immobilieres', $("#charges-immobilieres").val())
            localStorage.setItem('autres_remboursements_mensuels', $("#autres-remboursements-mensuels").val())
            localStorage.setItem('type-of-contract', $( "#type-of-contract option:selected").val())
            localStorage.setItem('country', $( "#country option:selected").val())
            localStorage.setItem('principal-residence', $( "#principal-residence option:selected").val())

            window.location.href = $(this).attr('rel')+'?formData='+encodeURIComponent(JSON.stringify(informationData)) + '&customerApplication=' + customerApplication;

            if ('parentIFrame' in window) {
                parentIFrame.scrollTo(0,0);
            }
        }
    });

    // Format phone number
    $('input#phone').on('input keyup', function() {
        var phoneNumber     = $(this).val().replace(/\D/g,'');
        var formattedNumber = formatPhoneNumber(phoneNumber);

        $(this).val(formattedNumber);
    });

    var formattedPhoneNumber = formatPhoneNumber($('input#phone').val());
    $('input#phone').val(formattedPhoneNumber);
    //

    var isValidCodePostal = true;
    var isAppendMessage   = false;
    $('#code-postal').on('input', function() {
        if ($(this).val().length > 5) {
            isValidCodePostal = false;
            $(this).val($(this).val().substr(0, 5));
        }

        if (!isValidCodePostal && !isAppendMessage) {
            $(this).parent().append('<div class="popin-message-code-postal">Veuillez renseigner un code postal valide (5 caractères, ex : 75000).</div>');
            isAppendMessage = true;
        }
    }).on('blur', function() {
        if (isAppendMessage) {
            $(this).parent().find('.popin-message-code-postal').remove();
            isAppendMessage = false;
        }
    })
});

function trackingAppStep1(element) {
    if ($('#nom').val() !== '' && $('#prenom').val() !== '' && $('#maiden-name').val() !== '' && $(element).val() !== '' &&
        $('#date-of-birth').val() !== '' && $('#city-of-birth').val() !== '' && $('#code-postal-naissance').val() !== '') {

        var dataAppStep1 = {
            channel: 'Pre Financing',
            virtualPageName: 'SEAT – Mo Pre Financing – form request',
            digitalElement: 'SEAT MO',
            formID: 'Pre-Financial-Request',
            eventName: "app-step-1",
            applicationType: "form",
            applicationName: 'Finance Request',
            applicationStep: '1-civility'
        }

        dynamic_dataLayer.push('appStep1', dataAppStep1);
        console.log(dataAppStep1);
    }
}

function trackingAppStep2(element) {
    if ($('#address').val() !== '' && $('#additional-address').val() !== '' && $('#code-postal').val() !== '' && $(element).val() !== '' &&
        $('#city').val() !== '') {

        var dataAppStep2 = {
            channel: 'Pre Financing',
            virtualPageName: 'SEAT – Mo Pre Financing – form request',
            digitalElement: 'SEAT MO',
            formID: 'Pre-Financial-Request',
            eventName: 'app-step-2',
            applicationType: "form",
            applicationName: 'Finance Request',
            applicationStep: '2-know-you-better'
        }

        dynamic_dataLayer.push('appStep2', dataAppStep2);
        console.log(dataAppStep2);
    }
}

function trackingAppStep3(element) {
    if ($('#number-of-dependent-children').val() !== '' && $(element).val() !== '') {

        var dataAppStep3 = {
            channel: 'Pre Financing',
            virtualPageName: 'SEAT – Mo Pre Financing – form request',
            digitalElement: 'SEAT MO',
            formID: 'Pre-Financial-Request',
            eventName: "app-step-3",
            applicationType: 'form',
            applicationName: 'Finance Request',
            applicationStep: '3-family-situation'
        };

        dynamic_dataLayer.push('appStep3', dataAppStep3);
        console.log(dataAppStep3);
    }
}

let inputsRequired = $('input[required]');

function checkValidate() {
    var popinMessage = '<div class="popin-message">Veuillez compléter ce champ.</div>';
    var isValid      = true;

    inputsRequired.each(function(index, input) {
        if ($(input).attr('id') !== 'email' || $(input).attr('id') !== 'phone' || $(input).attr('id') !== 'email_confirmation' || $(input).attr('name') !== 'civilite') {
            isValid = checkCivilite(isValid);

            if ($(input).val() === '' && isValid) {
                var formField = $(input).parent();

                if (!formField.find('.popin-message').length) {
                    formField.append(popinMessage);
                }

                if (window.innerWidth < 768) {
                    var indexItem = $(input).closest('.form-item').data('index');
                    if (indexItem < 3) {
                        showSlides(indexItem, true);
                    }
                }

                scrollToFieldError(formField);
                isValid = false;
                return isValid;
            }
        }

        if ($(input).attr('id') === 'code-postal' && isValid) {
            isValid = checkLengthPostalCode($(input), isValid)
        }

        if ($(input).attr('id') === 'email' && isValid) {
           isValid = checkEmail($(input), isValid);
        }

        if ($(input).attr('id') === 'email_confirmation' && isValid) {
            isValid = checkEmailConfirmation($(input), isValid);
        }

        if ($(input).attr('id') === 'phone' && isValid) {
            isValid = checkPhoneNumber($(input), isValid);
        }
    });

    return isValid;
}

inputsRequired.each(function(index, input) {
    $(input).on('click', function() {
        $(this).parent().find('.popin-message').remove();
    });
})

function scrollToFieldError(formField) {
    $('html, body').animate({
        scrollTop: formField.offset().top - (formField.parent().height() + 40)
    }, 500);
}

function checkEmail(input, isValid) {
    var formField     = input.parent();
    var popinMessage  = '<div class="popin-message">Veuillez respecter le format requis.</div>';

    var pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (!pattern.test(input.val())) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessage);
        }

        scrollToFieldError(formField);
        isValid = false;
    }

    return isValid;
}

function checkEmailConfirmation(input, isValid) {
    var emailField             = $('#email');
    var pattern                = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    var formField              = input.parent();

    var popinMessage              = '<div class="popin-message">Veuillez respecter le format requis.</div>';
    var popinMessageCorrespondent = '<div class="popin-message">Les deux emails ne correspondent pas.</div>';

    if (!pattern.test(input.val())) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessage);
        }
        scrollToFieldError(formField);

        isValid = false;
    }

    if (input.val() !== emailField.val()) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessageCorrespondent);
        }

        scrollToFieldError(formField);

        isValid = false;
    }

    return isValid;
}

function checkPhoneNumber(input, isValid) {
    var popinMessage  = '<div class="popin-message">Veuillez saisir un numéro de téléphone à 10 chiffres.</div>';
    var formField     = input.parent();
    var inputValue    = input.val();

    inputValue = inputValue.replace(/\s/g, '');

    if (inputValue.length !== 10) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessage);
        }

        scrollToFieldError(formField);

        isValid = false;
    }

    return isValid;
}

function checkCivilite(isValid) {
    var formField     = $('input#civilite-m').parent();
    var popinMessage  = '<div class="popin-message" style="color: #0F0F0F">Vous devez choisir cette 1 option.</div>';

    if (!$('input[name="civilite"]').is(':checked')) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessage);
        }
        scrollToFieldError(formField);

        if (window.innerWidth < 768) {
            var indexItem = formField.closest('.form-item').data('index');
            if (indexItem < 3) {
                showSlides(indexItem, true);
            }
        }

        isValid = false;
    }

    $('input[name="civilite"]').on('change', function() {
        $(this).parent().parent().find('.popin-message').remove();
    })

    return isValid;
}

function checkLengthPostalCode(input, isValid) {
    var formField     = input.parent();
    var popinMessage  = '<div class="popin-message">Veuillez renseigner un code postal valide (5 caractères, ex : 75000).</div>';

    if (input.val().length < 5) {
        if (!formField.find('.popin-message').length) {
            formField.append(popinMessage);
        }

        scrollToFieldError(formField);
        isValid = false;
    }

    return isValid;
}

function onInvalid(e) {
    e.preventDefault();
}

function validateNumberInput(input) {
    input.value = input.value.replace(/[^0-9]/g, '');
}

function trackingAppStep4(element) {
    if ($('#number-of-dependent-children').val() !== '' && $(element).val() !== '') {

        var dataAppStep4 = {
            channel: 'Pre Financing',
            virtualPageName: 'SEAT – Mo Pre Financing – form request',
            digitalElement: 'SEAT MO',
            formID: 'Pre-Financial-Request',
            eventName: "app-step-4",
            applicationType: 'form',
            applicationName: 'Finance Request',
            applicationStep: '4-professional-situation'
        };

        dynamic_dataLayer.push('appStep4', dataAppStep4);
        console.log(dataAppStep4);
    }
}

function formatPhoneNumber(phoneNumber) {
    return phoneNumber.replace(/(\d{2})(?=\d)/g, '$1 ');
}
