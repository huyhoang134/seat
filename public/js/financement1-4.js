$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var formDataFromUrl = urlParams.get('formData');
    var customerApplication = urlParams.get('customerApplication')

    if(formDataFromUrl!=null){
        var formData = JSON.parse(formDataFromUrl);
        var inputCivility = $("input[name=civilite][value=" + formData.civility + "]");

        if(inputCivility.attr('id')=='civilite-m'){
            $('#civilite-f').hide();
            $("label[for=civilite-f]").hide();
        }else{
            $('#civilite-m').hide();
            $("label[for=civilite-m]").hide();
        }
        inputCivility.prop('checked', true);
        $("label[for=" + inputCivility.attr('id') + "]").addClass('checked');
        $('#nom').val(formData.last_name);
        $('#prenom').val(formData.first_name);
        $('#address').val(formData.address);
        $('#country').val(formData.country);
        $('#email').val(formData.email);
        $('#code-postal').val(formData.code_postal);
        $('#phone').val(formData.phone);
        $('#maiden-name').val(formData.maiden_name);
        $('#date-of-birth').val(formData.date_of_birth);
        $('#city-of-birth').val(formData.city_of_birth);
        $('#postal-code').val(formData.code_postal_naissance);
        $('#additional-address').val(formData.additional_address);
        $('#city').val(formData.city);
        $('#nationality').val(formData.nationality);
        $('#principal-residence').val(formData.principal_residence);
        $('#family-status').val(formData.family_status);
        $('#number-of-dependent-children').val(formData.number_of_dependent_children);
        $('#occupation').val(formData.occupation);
        $('#hiring-date').val(formData.hiring_date);
        $('#type-of-contract').val(formData.type_of_contract);
        $('#employeur').val(formData.employeur);
        $('#revenu-net-mensuel').val(formData.revenu_net_mensuel);
        $('#autres-revenus').val(formData.autres_revenus);
        $('#charges-immobilieres').val(formData.charges_immobilieres);
        $('#autres-remboursements-mensuels').val(formData.autres_remboursements_mensuels);
        console.log(formData)
    }

    // Modifier Form
    const formGroup = [
        '.form-group-information',
        '.form-group-to-know',
        '.form-group-situation-familiale',
        '.form-group-professional-situation',
        '.form-group-contact',
    ];

    formGroup.map((item) => {
        $(item).find('.text-modifier').on('click', function () {
            const civility = $("input[name='civilite']:checked").val()
            const last_name = formData.last_name
            const first_name = formData.first_name
            const address = formData.address
            const email = formData.email
            const phone = formData.phone
            const employeur = formData.employeur
            const maidenName = localStorage.getItem('maiden-name')
            const dateOfBirth = localStorage.getItem('date-of-birth')
            const codePostalNaissance = localStorage.getItem('code-postal-naissance')
            const cityOfBirth = localStorage.getItem('city-of-birth')
            const additionalAddress = localStorage.getItem('additional-address')
            const city = localStorage.getItem('city')
            const nationality = localStorage.getItem('nationality')
            const familyStatus = localStorage.getItem('family-status')
            const numberOfDependentChildren = localStorage.getItem('number-of-dependent-children')
            const occupation = localStorage.getItem('occupation')
            const hiringDate = localStorage.getItem('hiring-date')
            const country = localStorage.getItem('country')
            const typeOfContract = localStorage.getItem('type-of-contract')
            const principalResidence = localStorage.getItem('principal-residence')
            const firstInput = $(item).find('input').first().attr('id');
            const revenuNetMensuel = localStorage.getItem('revenu_net_mensuel')
            const autresRevenus    = localStorage.getItem('autres_revenus')
            const chargesImmobilieres = localStorage.getItem('charges_immobilieres')
            const autresRemboursementsMensuels = localStorage.getItem('autres_remboursements_mensuels');
            localStorage.setItem('firstInput', firstInput)
            window.location = document.referrer + '&date-of-birth=' + dateOfBirth +
                '&civility=' + civility +
                '&last_name=' + last_name +
                '&first_name=' + first_name +
                '&address=' + address +
                '&email=' + email +
                '&phone=' + phone +
                '&maiden-name=' + maidenName +
                '&code-postal-naissance=' + codePostalNaissance +
                '&city-of-birth=' + cityOfBirth +
                '&additional-address=' + additionalAddress +
                '&city=' + city +
                '&nationality=' + nationality +
                '&family-status=' + familyStatus +
                '&number-of-dependent-children=' + numberOfDependentChildren +
                '&occupation=' + occupation +
                '&country=' + country +
                '&employeur=' + employeur +
                '&hiring-date=' + hiringDate +
                '&type-of-contract=' + typeOfContract +
                '&principal-residence=' + principalResidence +
                '&revenu-net-mensuel=' + revenuNetMensuel +
                '&autres-revenus=' + autresRevenus +
                '&charges-immobilieres=' + chargesImmobilieres +
                '&autres-remboursements-mensuels=' + autresRemboursementsMensuels;
        });
    });

    // Format Phone number
    var formattedPhoneNumber = formatPhoneNumber($('input#phone').val());
    $('input#phone').val(formattedPhoneNumber);
    //
});

function formatPhoneNumber(phoneNumber) {
    return phoneNumber.replace(/(\d{2})(?=\d)/g, '$1 ');
}
