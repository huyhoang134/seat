let textDate = '<span class="text-black"> au </span>';

window.iFrameResizerSeat125Mo = {
	//targetOrigin: 'https://www.e-scooter-configurateur.test',
	targetApiPartiTech: '/api',
	targetApiLeadConnect: 'https://dev-leadconnect.seat.fr/webservices/leads',
	S0AAA1 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 7275,
		prix_a_partir_de: {24: 76,
			37: 77},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '6 925,80',
			37: '7 306,30'},
		option_achat: {24: '4 284,98',
			37: '3 659,33'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 4 284,98€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire </p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 3 659,33€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire</p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			}
		}
	},
	S0AAA3 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 7275,
		prix_a_partir_de: {24: 76,
			37: 77},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '6 925,80',
			37: '7 306,30'},
		option_achat: {24: '4 284,98',
			37: '3 659,33'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 4 284,98€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire </p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 3 659,33€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire</p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			}
		}
	},
	S0AAA7 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 7275,
		prix_a_partir_de: {24: 76,
			37: 77},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '6 925,80',
			37: '7 306,30'},
		option_achat: {24: '4 284,98',
			37: '3 659,33'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 4 284,98€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire </p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option d’Achat pour une SEAT MÓ eScooter 125 vendu au prix de 7 275 €. Option d’achat finale : 3 659,33€ ou reprise du véhicule sous conditions du distributeur. Montants exprimés TTC. Offre réservée aux particuliers chez tous les distributeurs SEAT MÓ (France métropolitaine) présentant ce financement et valable jusqu’au 31/05/2024 pour toute commande d’une SEAT MÓ 125 eScooter passée avant le 31/05/2024 et livrée avant le 31/05/2024. Offre sous réserve d’acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : € 318 279 200 - Siège social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - Intermédiaire</p>",
				right_content: "<p>d’assurance européen : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : Bâtiment Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-Cotterêts Cedex. Délai de rétractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative Décès Incapacité Perte d’Emploi : 8,81€ / mois en sus de la mensualité. Contrat souscrit  auprès de Cardif Assurance Vie S.A. au capital de 719 167 488 €, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 € - n° 308 896 547 R.C.S. Paris, Siège social : 1 Boulevard Haussmann - 75009 Paris. Le coût de l’assurance peut varier en fonction de l’âge de l'assuré. SEAT MÓ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Émissions de CO2  WLTP (min – max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les véhicules légers neufs sont réceptionnés en Europe sur la base de la procédure d’essai harmonisée pour les véhicules légers (WLTP), procédure d’essai permettant de mesurer la consommation de carburant et les émissions de CO2, plus réaliste que la procédure NEDC précédemment utilisée.</p>"
			}
		}
	},
	S0AAA9 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 7275,
		prix_a_partir_de: {24: 76,
			37: 77},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '6 925,80',
			37: '7 306,30'},
		option_achat: {24: '4 284,98',
			37: '3 659,33'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 vendu au prix de 7 275 â‚¬. Option dâ€™achat finale : 4 284,98â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire </p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 8,81â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 vendu au prix de 7 275 â‚¬. Option dâ€™achat finale : 3 659,33â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 8,81â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			}
		}
	},

	model_defaut: 'S0AAA1',
	model_name: 'SEAT MÃ“ 125'
}

window.iFrameResizerSeat125Perf = {
	//targetOrigin: 'https://www.e-scooter-configurateur.test',
	targetApiPartiTech: '/api',
	targetApiLeadConnect: 'https://dev-leadconnect.seat.fr/webservices/leads',
	S01AAA7 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 8900,
		prix_a_partir_de: {24: 115,
			37: 109},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '8 784,21',
			37: '9 274,25'},
		option_achat: {24: '5 242,10',
			37: '4 476,70'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 Performance vendu au prix de 8 900 â‚¬. Option dâ€™achat finale : 5 242,10â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter Performance passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 11,17â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125 Performance: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 Performance vendu au prix de 8 900 â‚¬. Option dâ€™achat finale : 4 476,70â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter Performance passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 11,17â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125 Performance: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage)Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			}
		}
	},
	S01AAA9 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 8900,
		prix_a_partir_de: {24: 115,
			37: 109},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '8 784,21',
			37: '9 274,25'},
		option_achat: {24: '5 242,10',
			37: '4 476,70'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 Performance vendu au prix de 8 900 â‚¬. Option dâ€™achat finale : 5 242,10â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter Performance passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 11,17â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125 Performance: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 125 Performance vendu au prix de 8 900 â‚¬. Option dâ€™achat finale : 4 476,70â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 125 eScooter Performance passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 11,17â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 125 Performance: consommation mixte WLTP (min-max kWh/100km) : 4,2kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage)Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			}
		}
	},

	model_defaut: 'S01AAA7',
	model_name: 'SEAT MÃ“ 125 PERFORMANCE'
}

window.iFrameResizerSeat50 = {
	//targetOrigin: 'https://www.e-scooter-configurateur.test',
	targetApiPartiTech: '/api',
	targetApiLeadConnect: 'https://dev-leadconnect.seat.fr/webservices/leads',
	S02AA7 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 5900,
		prix_a_partir_de: {24: 67,
			37: 66},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '5 899,53',
			37: '6 213,51'},
		option_achat: {24: '3 475,10',
			37: '2 967,70'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 50 vendu au prix de 5 900 â‚¬. Option dâ€™achat finale : 3 475,10â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 50 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 7,54â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 50 : consommation mixte WLTP (min-max kWh/100km) : 3,26kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 50 vendu au prix de 5 900 â‚¬. Option dâ€™achat finale : 2 967,70â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 50 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 7,54â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 50 : consommation mixte WLTP (min-max kWh/100km) : 3,26kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			}
		}
	},
	S02AA9 : {
		loa_duree: {24: 24,
			37: 37},
		loa_loyers: {24: 23,
			37: 36},
		prix_catalogue: 5900,
		prix_a_partir_de: {24: 67,
			37: 66},
		kilometer: {24: '10000',
			37: '15000'},
		montant_total_du: {24: '5 899,53',
			37: '6 213,51'},
		option_achat: {24: '3 475,10',
			37: '2 967,70'},
		loyer_majore: {24: 900,
			37: 900},
		loyer_au_titre_de_location: {24: 23,
			37: 36},
		date_offre: {24: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>',
			37: '<span class="text-date">01/04/2024</span>' + textDate + '<span class="text-date">31/05/2024</span>'},
		mention_legale: {
			24: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 50 vendu au prix de 5 900 â‚¬. Option dâ€™achat finale : 3 475,10â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 50 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 7,54â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 50 : consommation mixte WLTP (min-max kWh/100km) : 3,26kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			},
			37: {
				left_content: "<p>(2) Location avec Option dâ€™Achat pour une SEAT MÃ“ eScooter 50 vendu au prix de 5 900 â‚¬. Option dâ€™achat finale : 2 967,70â‚¬ ou reprise du vÃ©hicule sous conditions du distributeur. Montants exprimÃ©s TTC. Offre rÃ©servÃ©e aux particuliers chez tous les distributeurs SEAT MÃ“ (France mÃ©tropolitaine) prÃ©sentant ce financement et valable jusquâ€™au 31/05/2024 pour toute commande dâ€™une SEAT MÃ“ 50 eScooter passÃ©e avant le 31/05/2024 et livrÃ©e avant le 31/05/2024. Offre sous rÃ©serve dâ€™acceptation du dossier par VOLKSWAGEN BANK GMBH - SARL de droit allemand - Capital social : â‚¬ 318 279 200 - SiÃ¨ge social : Braunschweig (Allemagne) - RC/HRB Braunschweig : 1819 - IntermÃ©diaire</p>",
				right_content: "<p>dâ€™assurance europÃ©en : D-HNQM-UQ9MO-22 (www.orias.fr) - Succursale France : BÃ¢timent Ellipse, 15 avenue de la Demi-Lune - 95700 Roissy-en-France - RCS Pontoise : 451 618 904 - Administration et adresse postale : 11, avenue de Boursonne - B.P. 61 - 02601 Villers-CotterÃªts Cedex. DÃ©lai de rÃ©tractation de 14 jours. * Conditions sur servicepublic.fr. Assurance facultative DÃ©cÃ¨s IncapacitÃ© Perte dâ€™Emploi : 7,54â‚¬ / mois en sus de la mensualitÃ©. Contrat souscrit  auprÃ¨s de Cardif Assurance Vie S.A. au capital de 719 167 488 â‚¬, 732 028 154 R.C.S. Paris et Cardif Assurances Risques Divers S.A. au capital de 21 602 240 â‚¬ - nÂ° 308 896 547 R.C.S. Paris, SiÃ¨ge social : 1 Boulevard Haussmann - 75009 Paris. Le coÃ»t de lâ€™assurance peut varier en fonction de lâ€™Ã¢ge de l'assurÃ©. SEAT MÃ“ eScooter 50 : consommation mixte WLTP (min-max kWh/100km) : 3,26kWh/100km. Ã‰missions de CO2  WLTP (min â€“ max g/km) : 0 (en phase de roulage) Depuis le 1er septembre 2018, les vÃ©hicules lÃ©gers neufs sont rÃ©ceptionnÃ©s en Europe sur la base de la procÃ©dure dâ€™essai harmonisÃ©e pour les vÃ©hicules lÃ©gers (WLTP), procÃ©dure dâ€™essai permettant de mesurer la consommation de carburant et les Ã©missions de CO2, plus rÃ©aliste que la procÃ©dure NEDC prÃ©cÃ©demment utilisÃ©e.</p>"
			}
		}
	},

	model_defaut: 'S02AA7',
	model_name: 'SEAT MÃ“ 50'
}

var pricing = false;


$(document).ready(function() {
	var queryString = window.location.search;
	var urlParams   = new URLSearchParams(queryString);
	var gamme       = urlParams.get('gamme');
	var dataItem    = '';

	if (gamme === '125perf') {
		dataItem = window.iFrameResizerSeat125Perf
	} else if (gamme === '50') {
		dataItem = window.iFrameResizerSeat50
	} else {
		dataItem = window.iFrameResizerSeat125Mo
	}

	$('.model-name').html(dataItem.model_name);

	contentLoaded(dataItem);
});

function contentLoaded(dataItem) {
	//load default color in configurator page
	if($('.product-detail .color li.active').length >0 &&  $('.product-detail .color li.active').attr('value').length){
		var current_model = $('.product-detail .color li.active').attr('value')

		//Reload prince on change color in configurator page.
		$('.product-detail .color li').click(function() {

			var current_model = $('.product-detail .color li.active').attr('value');
			eval('pricing=dataItem.'+current_model);
			console.log('pricing=dataItem.'+current_model);
			load_vars();
			$('.item-duration .custom-checkbox').find('input[name=financement]:checked').closest('.item-duration').trigger('click');
		});

	}else if($('.item__profile  .item__img img:visible').length >0 &&  typeof $('.item__profile  .item__img img:visible').attr('value') != 'undefined' && $('.item__profile  .item__img img:visible').attr('value').length){

		var current_model = $('.item__profile  .item__img img:visible').attr('value');

	} else {
		//load default color on index page.
		var current_model = dataItem.model_defaut
	}

	eval('pricing=dataItem.'+current_model);

	load_vars();

	$('input[name=financement]').on('click', function() {
		load_vars();
	})
}

function load_vars() {
	var optionMonth = $('.item-duration').find('input[name=financement]:checked').val()
	if (!optionMonth) {
		optionMonth = 37;
	}

	$('.var_prix_catalogue').html(pricing.prix_catalogue);
	$('.var_prix_a_partir_de').html(pricing.prix_a_partir_de[optionMonth]);
	$('.var_montant_total_du').html(pricing.montant_total_du[optionMonth]);
	$('.var_option_achat').html(pricing.option_achat[optionMonth]);
	$('.var_loyer_majore').html(pricing.loyer_majore[optionMonth]);
	$('.var_date_offre').html(pricing.date_offre[optionMonth]);
	$('.var_mention_legale').html(pricing.mention_legale[optionMonth].left_content + pricing.mention_legale[optionMonth].right_content);
	$('.var_loa_duree').html(pricing.loa_duree[optionMonth]);
	$('.var_loa_loyers').html(pricing.loa_loyers[optionMonth]);
	$('.price-per-month').html(pricing.prix_a_partir_de[optionMonth]);
	$('.var_loyer_au_titre_de_location').html(pricing.loyer_au_titre_de_location[optionMonth]);
	$('.var_kilometer').html(pricing.kilometer[optionMonth]);
	$('.var_mention_legale_left').html(pricing.mention_legale[optionMonth].left_content);
	$('.var_mention_legale_right').html(pricing.mention_legale[optionMonth].right_content);
}

function refresh_index_pricing() {
	setTimeout(() => {
		var current_model = $('.owl-item.active.center .produit').data('value');
		eval('pricing=window.iFrameResizerSeat125Mo.'+current_model);
		load_vars();
	}, "500")
}

function loadDataFooterInAllPage(currentModel, optionMonth, vehicleEngine)
{
	if (vehicleEngine === '125perf') {
		dataItem = window.iFrameResizerSeat125Perf
	} else if (vehicleEngine === '50') {
		dataItem = window.iFrameResizerSeat50
	} else {
		dataItem = window.iFrameResizerSeat125Mo
	}

	eval('pricing=dataItem.'+currentModel);
	$('.var_prix_catalogue').html(pricing.prix_catalogue);
	$('.var_prix_a_partir_de').html(pricing.prix_a_partir_de[optionMonth]);
	$('.var_montant_total_du').html(pricing.montant_total_du[optionMonth]);
	$('.var_option_achat').html(pricing.option_achat[optionMonth]);
	$('.var_loyer_majore').html(pricing.loyer_majore[optionMonth]);
	$('.var_date_offre').html(pricing.date_offre[optionMonth]);
	$('.var_mention_legale').append(pricing.mention_legale[optionMonth].left_content + pricing.mention_legale[optionMonth].right_content);
	$('.var_loa_duree').html(pricing.loa_duree[optionMonth]);
	$('.var_loa_loyers').html(pricing.loa_loyers[optionMonth]);
	$('.price-per-month').html(pricing.prix_a_partir_de[optionMonth]);
	$('.var_loyer_au_titre_de_location').html(pricing.loyer_au_titre_de_location[optionMonth]);
	$('.var_kilometer').html(pricing.kilometer[optionMonth]);
	$('.model-name').html(dataItem.model_name);
	$('.var_mention_legale_left').html(pricing.mention_legale[optionMonth].left_content);
	$('.var_mention_legale_right').html(pricing.mention_legale[optionMonth].right_content);
}

function getCarNameFormCarCode(carCode) {
	if (carCode === '125perf') {
		return window.iFrameResizerSeat125Perf.model_name;
	} else if (carCode === '125mo') {
		return window.iFrameResizerSeat125Mo.model_name;
	} else {
		return window.iFrameResizerSeat50.model_name;
	}
}