$(document).ready(function () {
    $('.carousel').slick({
        dots: true,
        responsive: [
            {
                breakpoint: 9999,
                settings: "unslick"
            },
            {
                breakpoint: 769,
                settings: {
                    slidesPerRow: 1,
                    rows: 1
                }
            }
        ]
    });
});
