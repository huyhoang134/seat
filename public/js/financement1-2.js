$(document).ready(function () {
    $('.var_loa_duree').html(localStorage.getItem('loaDuree'));
    $('.var_prix_a_partir_de').html(localStorage.getItem('prixAPartirDe'));
    $('.var_montant_total_du').html(localStorage.getItem('montantTotalDu'));
    $('.var_loyer_au_titre_de_location').html(localStorage.getItem('loyerAuTitreDeLocation'));
    $('.var_kilometer').html(localStorage.getItem('kilometer'));

    let zipcode = ($("#zipcode").val().length === 0) ? $("#zipcode").attr('placeholder') : $("#zipcode").val();
    let dataAccessory = null;
    
    // getData for step 1-2
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var get_configurator_data = urlParams.get('data');
    if (get_configurator_data != null) {
        var configuratorData = JSON.parse(get_configurator_data);
    }

    var comment = 'Personne intéressé par un SEAT MÓ eScooter 125; ';
    let model = 'S0AAA3';
    let totalPrice = 0;

    if (typeof configuratorData != 'undefined' && configuratorData != null) {
        // show color
        $('.item__img').find('img').addClass('d-none');
        $('.color-circle').find('img').addClass('d-none');
        $('.color-circle').find('img.energy').removeClass('d-none');
        $('.color-circle').find('.text-description').addClass('d-none');
        $('.' + configuratorData.colorValue).removeClass('d-none');

        // model
        model = configuratorData.colorValue;

        // color price
        let cashPrice = Number(configuratorData.colorCashPrice);
        let rentPrice = Number(configuratorData.colorRentPrice);

        // show accessoires
        dataAccessory = configuratorData.dataAccessory;

        if (!$.isEmptyObject(dataAccessory)) {
            comment += 'Accessoires : ';

            $('#accessoires').removeClass('d-none');
            var accessoriesItem = $('#accessoires').find('.wrapper-accessoires');

            $.each(dataAccessory, function (key, data) {
                if (data.isFree == false) {
                    accessoriesItem.append('<div class=\'accessoires__item col-md-6\'>\n' +
                        '                                        <img src="' + data.configuratorImage + '" alt=\'item_images\'>\n' +
                        '                                        <div class=\'accessoires__item--info d-flex d-md-block flex-column\'>\n' +
                        '                                           <div>\n' +
                        '                                               <p class=\'text-header\'>' + data.label + '</p>\n' +
                        '                                           </div>' +
                        '                                           <div class="price-supprimer d-flex d-md-block justify-content-between flex-column item-price">\n' +
                        '                                               <p class=\'text-price\'>' + data.price + '€</p>\n' +
                        '                                               <a href=\'#\' id=' + data.id + ' data-price=' + data.price + ' class=\'text-link\'>Supprimer</a>\n' +
                        '                                           </div>' +
                        '                                        </div>\n' +
                        '                                    </div>');
                    totalPrice += Number(data.price);

                    comment += '' + data.label + '/ ';
                }
            });

            $('#totalPrice').html(totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + "€");
        }

        if (configuratorData.financement === 'cash') {
            comment += 'Souhaite payer comptant; ';
            comment += 'À partir de ' + cashPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + '€ TTC*; ';
        }

        if (configuratorData.assuranceOui === 'oui') {
            comment += 'Souhaite une assurance; ';
        }

        if (configuratorData.financement === 'rent') {
            comment += 'Souhaite un loyer à ' + rentPrice + '€/mois sur 37 mois avec apport de <span class="var_loyer_majore">xxxx</span>€ et 5 000km annuel; ';
        }

        if (!$.isEmptyObject(dataAccessory)) {
            comment += 'Accessoires : ';

            $.each(dataAccessory, function (key, data) {
                if (data.isFree === true) {
                    comment += '' + data.label + '/ ';
                }
            });

            $.each(dataAccessory, function (key, data) {
                if (data.isFree == false) {
                    comment += '' + data.label + '/ ';
                }
            });
        }
    }

    $('.accessoires__item--info a').click(function (e) {
        
        const selectedItemId = $(this).attr('id');

        Object.entries(dataAccessory).forEach(([key, val]) => {
            if (selectedItemId === key) {
                delete dataAccessory[key]
            }
        });
        
        e.preventDefault();

        var item = $(this);
        var price = item.data('price');

        item.closest('.accessoires__item').remove();

        var numItems = $('.accessoires__item').length
        if (numItems === 1) {
            $('.accessoires__item').removeClass('col-md-6').addClass('col-md-12')
        }

        // setPrice
        totalPrice -= Number(price);
        $('#totalPrice').html(totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + "€");
    });

    $('#gotoStep1-3').on('click', function () {
        $("input[name='model']").val(model)
        $("input[name='dataAccessory']").val(Object.keys(dataAccessory))
        $("input[name='totalPriceOption']").val(totalPrice)
        $("input[name='gcm']").val(configuratorData.gcm)
        $("input[name='vdr']").val(configuratorData.vdr)
        $("input[name='assurance']").val(configuratorData.assuranceOui)
        $("input[name='colorName']").val(configuratorData.colorName)

        $("input[name='loaDuree']").val(localStorage.getItem('loaDuree'))
        $("input[name='loaLoyers']").val(localStorage.getItem('loaLoyers'))
        $("input[name='prixAPartirDe']").val(localStorage.getItem('prixAPartirDe'))
        $("input[name='montantTotalDu']").val(localStorage.getItem('montantTotalDu'))
        $("input[name='optionAchat']").val(localStorage.getItem('optionAchat'))
        $("input[name='loyerMajore']").val(localStorage.getItem('loyerMajore'))
        $("input[name='prixCatalogue']").val(localStorage.getItem('prixCatalogue'))
        $("input[name='kilometer']").val($('.var_kilometer').first().text().replace(/\s/g, ''))
    })

    $('.btn-leadconnect').on('click', function (e) {
        $("input[name='comment']").val(comment)
        $("input[name='dealer']").val(urlParams.get('dealer'))
    });

    $("input[name='personne-majeure']").on('change', function () {
        if ($(this).val() === 'F') {
            $('#personneMajeureTooltip').css("display", "block");
        } else {
            $('#personneMajeureTooltip').css("display", "none");
        }
    })

    $('.selection__item--info .text-header').text(modelName);

    var dataVehicleModel;
    dataColorPrice.forEach(function (data) {
        if (data.id === configuratorData.colorValue) {
           dataVehicleModel = data;
        }
    })

    $('.selection__item.color-circle .selection__item--image').append('<img src="images/' + dataVehicleModel.colorImage + '" alt="item_images" class="'+ dataVehicleModel.id+'">');
    $('.selection__item.color-circle .selection__item--info .selection__item--description').append('<p class="text-description '+ dataVehicleModel.id +'">' + dataVehicleModel.label +'</p>');
    $('.item__profile .item__img').append('<img src="images/' + dataVehicleModel.modelImage + '" alt="item_image" class="w-100 '+ dataVehicleModel.id + '">');
});


function validateZipcode()
{
    //Toto Validate zipcode or remove call in form oninput
    return false;
}
