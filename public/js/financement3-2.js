$(document).ready(function () {
    if (localStorage.getItem('isReloaded_3.2')) {
        location.reload()
        localStorage.removeItem('isReloaded_3.2')
    }

    let urlImage;
    var files         = [];
    let existingFiles = [];

    if ($('.card-file-name').length > 0) {
        $('.card-file-name .file-name').each(function () {
            existingFiles.push($(this).text());
        });
    }

    $('.btn-import-file').on('click', function (e) {
        e.preventDefault();
        $('input[type=file]').trigger('click');
    });

    $('.btn-import-other-file').on('click', function (e) {
        e.preventDefault();
        $('input[type=file]').trigger('click');
    })


    fileList();
    deleteFile();
    previewImg();

    function fileList() {
        const inputFile = $('input[type=file]');
        inputFile.on('change', function (e) {
            if (!this.files[0].type.match('.jpeg|.png|.pdf')) {
                $('.btn-next-step').prop('disabled', true)
                $(this).val('')
                $('.validation-text').text('Seuls les documents au format .JPEG, .PNG, .PDF sont autorisés.')
                $('.validation-text').removeClass('d-none')
            } else if (this.files[0].size > 100 * 1024 * 1024) {
                $('.btn-next-step').prop('disabled', true)
                $(this).val('')
                $('.validation-text').text('La taille du fichier est trop grande. Veuillez vous assurer que la taille est inférieure à 100 Mo')
                $('.validation-text').removeClass('d-none')
            } else {
                let fileName = $(this).val();
                if (fileName) {
                    generateFilePreview(this.files[0], files)
                }
                $('.btn-next-step').prop('disabled', false);
                checkDuplicates(files, this.files[0].name);
                e.target.value = null;
            }
        });
    }

    $('.btn-next-step').click(function (e) {
        e.preventDefault();
        var formData = new FormData();
        var dueDay = $('input[name=due-day]:checked', '#formFinancementEcheanceRIB').val();
        formData.append('application_id', $('input[name=application_id]').val())
        formData.append('due-day', dueDay);
        formData.append('bypass', $('input[name=bypass]').val());
        for (var i=0; i < files.length; i++) {
            formData.append('files[]', files[i]);
        }

        $.ajax({
            type: "POST",
            url: window.location.origin + '/votre-solvabilite-document.html',
            processData: false,
            contentType: false,
            data: formData,
            success: function (response) {
                if (response.isBypassBankpush) {
                    window.location.href = '/votre-solvabilite.html?customerApplication=' + response.application_id + '&message=' + response.message
                } else {
                    window.location.href = '/algoan-bank-interface.html?customerApplication=' + response.application_id
                }
            },
            error: function () {
            }
        })
    })

    function deleteFile(listFile) {
        $('.delete-file').off('click').on('click', function () {
            var fileElement = $(this).parent().parent();
            var targetElement = fileElement.find('.file-name').text();
            var mediaId = $(this).attr('data-id');
            if (!mediaId) {
                fileElement.remove()
                let removed = false;
                listFile = listFile.filter(item => {
                    if (item.name === targetElement && !removed) {
                        removed = true;
                        return false;
                    }
                    return true;
                });
                handleDuplicateWhenDelete(targetElement);

                files = listFile;
                allFilesDeleted()
            } else {
                $.ajax({
                    type: "POST",
                    url: window.location.origin + '/delete-file',
                    data: {
                        id: mediaId,
                        type: 'bank'
                    },
                    success: function (response) {
                        fileElement.remove()
                        handleDuplicateWhenDelete(targetElement);
                        allFilesDeleted()
                    },
                    error: function () {
                    }
                })
            }
        })
    }

    function previewImg() {
        $('.preview-img').on('click', function () {
            $('.image-default').addClass('d-none');
            let fileType = $(this).parent().parent().find('.file-name').text().split('.').pop()
            if (fileType === 'pdf') {
                window.open($(this).parent().parent().find('.pdf-preview').attr('src'));
            } else {
                window.open($(this).parent().parent().find('.img-preview').attr('src'));
            }
            $('input[type=file]').removeEventListener('change');
        });
    }

    function allFilesDeleted() {
        const countFile = $('.card-file-name').length;
        if (countFile === 0) {
            $('img.preview').addClass('d-none')
            $('.image-default').removeClass('d-none')
            $('.preview-pdf').addClass('d-none')
            hideNextStepButton();

            $('input[type=file]').val('')
        }
    }

    function showNextStepButton() {
        $('.btn-import-file').addClass('d-none')
        $('.btn-next-step').removeClass('d-none')
        $('.btn-import-other-file').removeClass('d-none')
        $('.validation-text').text('');
    }

    function hideNextStepButton() {
        $('.btn-import-file').removeClass('d-none')
        $('.btn-next-step').addClass('d-none')
        $('.btn-import-other-file').addClass('d-none')
        $('.validation-text').text('');
    }

    dragDropFile()
    function dragDropFile() {
        $('.import-file').on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (e.originalEvent.dataTransfer) {
                if (e.originalEvent.dataTransfer.files.length) {
                    var droppedFiles = e.originalEvent.dataTransfer.files;
                    for (var i = 0; i < droppedFiles.length; i++) {
                        if (!droppedFiles[i].type.match('.jpeg|.png|.pdf')) {
                            $('.btn-next-step').prop('disabled', true)
                            $('.validation-text').text('Seuls les documents au format .JPEG, .PNG, .PDF sont autorisés.')
                            $('.validation-text').removeClass('d-none')
                        } else if (droppedFiles[i].size > 100 * 1024 * 1024) {
                            $('.btn-next-step').prop('disabled', true)
                            $('.validation-text').text('La taille du fichier est trop grande. Veuillez vous assurer que la taille est inférieure à 100 Mo')
                            $('.validation-text').removeClass('d-none')
                        } else {
                            generateFilePreview(droppedFiles[i], files)
                            $('.btn-next-step').prop('disabled', false);
                            checkDuplicates(files, droppedFiles[i].name);
                        }
                    }
                }
            }

            return false;
        });

        $('.import-file').on('dragover', function (e) {
            e.preventDefault();
        });
    }

    function generateFilePreview(thisFile, files) {
        showNextStepButton();
        var fileNameElementOther = '';
        fileNameElementOther = $('<div class="card-file-name">\n' +
            '                                            <div class="dot-file-name d-flex align-items-center">\n' +
            '                                               <div class="dot"></div>\n' +
            '                                               <span class="file-name"></span>\n' +
            '                                            </div>\n' +
            '                                            <img class="img-preview d-none" src=""/>\n' +
            '                                            <iframe class="pdf-preview d-none" src=""></iframe>\n' +
            '                                            <div class="block-preview">\n' +
            '                                                <span class="preview-img">Voir</span>\n' +
            '                                                <a class="delete-file text-c-orange">Supprimer</a>\n' +
            '                                            </div>\n' +
            '                                        </div>');
        fileNameElementOther.removeClass('import-default');
        fileNameElementOther.find('.block-preview .delete-file').removeAttr('data-id');
        fileNameElementOther.find('.file-name').text(thisFile.name.split(/(\\|\/)/g).pop());

        urlImage = URL.createObjectURL(thisFile);
        if (thisFile.type === 'application/pdf') {
            $('.preview-pdf').removeClass('d-none').attr('src', urlImage);
            $('img.preview').addClass('d-none');
            $('.block-preview-image').removeClass('d-flex').addClass('d-none')

            fileNameElementOther.find('.pdf-preview').attr('src', urlImage)
        } else {
            $('img.preview').removeClass('d-none').attr('src', urlImage);
            $('.preview-pdf').addClass('d-none');
            $('.block-preview-image').removeClass('d-none').addClass('d-flex')

            fileNameElementOther.find('.img-preview').attr('src', urlImage);
        }
        $('.block-import').append(fileNameElementOther);
        $('.import-default').remove();
        $('.image-default').addClass('d-none');

        files.push(thisFile);

        deleteFile(files)
        previewImg()
    }

    $('.carousel').slick({
        dots: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 9999,
                settings: "unslick"
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }
        ]
    }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        switch (nextSlide) {
            case 0:
                $('.arrow-item .arrow-right').addClass('is-active');
                $('.arrow-item .arrow-left').removeClass('is-active');

                break;
            case 2:
                $('.arrow-item .arrow-right').addClass('is-active');
                $('.arrow-item .arrow-left').addClass('is-active');

                break;
            case 4:
                $('.arrow-item .arrow-right').removeClass('is-active');
                $('.arrow-item .arrow-left').addClass('is-active');

                break;

            default:
        }
    })

    $('input[name=due-day]').click(function () {
        if ($(this).val() == 1) {
            $('.dueday-number').html('1<sup>er</sup> du mois')
        } else {
            $('.dueday-number').html($(this).val() + ' du mois')
        }
    })

    function checkDuplicates(files, fileName) {
        for (var i = 0; i < files.length; i++) {
            if (existingFiles.includes(fileName)) {
                $('.btn-next-step').prop('disabled', true);
                $('.validation-text').removeClass('d-none').text('Attention, il semblerait que l’un des fichiers soit un doublon.');
            }
        }
        existingFiles.push(fileName);
    }

    function countOccurrences(existingFiles, targetElement) {
        return existingFiles.reduce((count, element) => (element === targetElement ? count + 1 : count), 0);
    }

    function hasDuplicate(existingFiles) {
        return existingFiles.some((element, index) => existingFiles.indexOf(element) !== index);
    }

    function handleDuplicateWhenDelete(targetElement) {
        if (existingFiles.indexOf(targetElement) !== -1) {
            existingFiles.splice(existingFiles.indexOf(targetElement), 1);
        }

        const hasDuplicates = hasDuplicate(existingFiles);
        const result        = countOccurrences(existingFiles, targetElement);

        if (existingFiles.includes(targetElement) && result === 1 && !hasDuplicates) {
            $('.validation-text').addClass('d-none').text();
            $('.btn-next-step').prop('disabled', false)
        }
    }
});
